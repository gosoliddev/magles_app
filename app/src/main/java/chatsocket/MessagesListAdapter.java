package chatsocket;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import custom.ImageCanvas;
import custom.PhotoView;
import es.maglesrevista.maglesapplication.Photo;
import es.maglesrevista.maglesapplication.R;
import other.Message;
import ru.truba.touchgallery.TouchView.UrlTouchImageView;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;


public class MessagesListAdapter extends BaseAdapter {

	private Context context;
	private List<Message> messagesItems;
	private Message current;
	public MessagesListAdapter(Context context, List<Message> navDrawerItems) {
		this.context = context;
		this.messagesItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return messagesItems.size();
	}

	@Override
	public Object getItem(int position) {
		return messagesItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		final Display display = wm.getDefaultDisplay();
		Message m = messagesItems.get(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if(position==0 && m.getType_message().equals("request") && !BaseHelper.getNickname(context).equals(m.getFromName()))
		{
			convertView = mInflater.inflate(R.layout.list_item_message_request, null);
			if(messagesItems.size()>1) {
				LinearLayout actions = (LinearLayout) convertView.findViewById(R.id.actions);
				LinearLayout pnl_actions=(LinearLayout) convertView.findViewById(R.id.actions);
				LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) pnl_actions.getLayoutParams();
				params.weight=0;
				params.height=0;
				pnl_actions.setVisibility(View.INVISIBLE);
				pnl_actions.removeAllViews();
				pnl_actions.setLayoutParams(params);

			}
		}
		else if(position==0){
			convertView = mInflater.inflate(R.layout.list_item_message_first, null);
		}
		else if(m.getType_message().equals("image")) {
			convertView = mInflater.inflate(R.layout.list_item_message_image, null);
		}
		else
			convertView = mInflater.inflate(R.layout.list_item_message_right, null);

		ImageView icnSee = (ImageView) convertView.findViewById(R.id.icnSee);
		ImageView icnSeeReaded=(ImageView)convertView.findViewById(R.id.icnSecN);
		TextView lblFrom = (TextView) convertView.findViewById(R.id.lblMsgFrom);
		TextView lblNickname = (TextView) convertView.findViewById(R.id.txtNickname);
		TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);
		TextView day = (TextView) convertView.findViewById(R.id.day);
		ImageView img=(ImageView)convertView.findViewById(R.id.photoMsg);
		// Identifying the message owner
		if (messagesItems.get(position).isSelf()) {
			Bitmap bmp= ImageUtils.decodeFile(ImageUtils.FileToByte(ImageUtils.getCacheFilename()), display.getWidth()/2);
			if(bmp!=null) {
				img.setImageBitmap(bmp);
			}
			else
				img.setImageResource(R.mipmap.photo_default);
			ViewGroup.LayoutParams params=img.getLayoutParams();
			params.width= ViewGroup.LayoutParams.MATCH_PARENT;
			img.setLayoutParams(params);
			img.setAdjustViewBounds(true);
		} else {
			try
			{
				getDrawable(img, m);
			}
			catch (Exception e) {}
			catch (OutOfMemoryError e) {}
		}
		img.setAdjustViewBounds(true);
		if((m.getType_message().equals("image"))) {
			ImageCanvas photo = (ImageCanvas)convertView.findViewById(R.id.send_photo);
			photo.setImageURI(Uri.parse(Request.BASE_URL+m.getMessage()));
			ViewGroup.LayoutParams params=photo.getLayoutParams();
			params.width=display.getWidth()-195;
			photo.setLayoutParams(params);
			photo.setTag(m.getMessage());
			photo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					ImageCanvas photo=(ImageCanvas)v;
					Dialog dialog = new Dialog((Activity)context,R.style.dialog);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.requestWindowFeature(Window.FEATURE_ACTION_MODE_OVERLAY);
					LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View view = li.inflate(R.layout.dialog_preview, null, false);
					UrlTouchImageView preview=(UrlTouchImageView)view.findViewById(R.id.preview);
					preview.setUrl(Request.BASE_URL+photo.getTag().toString().replace("file","fileOriginal"));
					preview.setClickable(false);
					dialog.setContentView(view);
					Window window = dialog.getWindow();
					WindowManager.LayoutParams wlp = window.getAttributes();
					wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
					wlp.width=display.getWidth();
					wlp.height=display.getHeight();
					window.setAttributes(wlp);
					dialog.show();
				}
			});
		}
		else {
			String strMesage = m.getMessage();
			lblFrom.setText(m.getFromName());
			lblNickname.setText(m.getFromName());
			day.setText(m.getDate());

			switch (m.getType_message()) {
				case "request": {
					if (BaseHelper.getNickname(context).equals(m.getFromName())) {
						strMesage = context.getString(R.string.text_sendding_invitation);
						strMesage = strMesage.replace("#NickName", m.getReceiver());
					} else {
						strMesage = context.getString(R.string.text_received_invitacion);
						strMesage = strMesage.replace("#NickName", m.getFromName());
					}
					break;
				}

				case "accepted": {
					if (BaseHelper.getNickname(context).equals(m.getFromName())) {
						strMesage = context.getString(R.string.text_send_accepted_invitacion);
						strMesage = strMesage.replace("#NickName", m.getReceiver());
					} else {
						strMesage = context.getString(R.string.text_accepted_invitacion);
						strMesage = strMesage.replace("#NickName", m.getFromName());
					}
					break;
				}
				case "message": {
					strMesage = m.getMessage();
				}
			}
			txtMsg.setText(strMesage);
		}
		if (!messagesItems.get(position).isSelf())
		{
			icnSee.setVisibility(View.GONE);
			icnSeeReaded.setVisibility(View.GONE);
		}else if(!m.getReaded()) {
			icnSeeReaded.setVisibility(View.VISIBLE);
			icnSee.setVisibility(View.GONE);
		}
		else {
			icnSee.setVisibility(View.VISIBLE);
			icnSeeReaded.setVisibility(View.GONE);
		}
		return convertView;
	}
	public  void getDrawable(final ImageView image ,  final Message current){
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		final Display display = wm.getDefaultDisplay();
		RequestParams params=new RequestParams();
		params.put("nickname",current.getFromName());
			Request.get("Photos/getPhotoByNickname", params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int i, Header[] headers, byte[] bytes) {
					try {
						String cadena=new String(bytes,"UTF-8");
						if(!cadena.equals("")) {
							BitmapFactory.Options options = new BitmapFactory.Options();
							Bitmap bitmap = ImageUtils.decodeFile(bytes, display.getWidth() / 2);//BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
							image.setImageBitmap(bitmap);
							ViewGroup.LayoutParams params = image.getLayoutParams();
							params.width = ViewGroup.LayoutParams.MATCH_PARENT;
							image.setLayoutParams(params);
							image.setAdjustViewBounds(true);
						}
						else
						{
							image.setImageResource(R.mipmap.photo_default);
						}
					} catch (Exception ex) {
						image.setImageResource(R.mipmap.photo_default);
					}
				}

				@Override
				public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
					image.setImageResource(R.mipmap.photo_default);
				}
			});
	}
	public void getDrawable(final PhotoView image, String uri) {
		final Dialog dialog=Preloader.getInstance((Activity)context).build();
		dialog.show();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		final Display display = wm.getDefaultDisplay();
			Request.get(uri, null, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int i, Header[] headers, byte[] bytes) {
					try {
						String cadena=new String(bytes,"UTF-8");
						if(!cadena.equals("")) {
							Bitmap bitmap = ImageUtils.decodeFile(bytes);
							image.setPhotoBitmap(bitmap);
							LinearLayout ln=(LinearLayout)image.getParent();
							image.scaleImage(ln.getWidth()-10);
						}
						else
							image.setPhoto_default(R.mipmap.photo_default);


					} catch (Exception ex) {
						image.setPhoto_default(R.mipmap.photo_default);
					}
					dialog.dismiss();
				}

				@Override
				public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
					image.setPhoto_default(R.mipmap.photo_default);
					dialog.dismiss();

				}
			});

	}
}
