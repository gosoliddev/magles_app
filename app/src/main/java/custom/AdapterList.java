package custom;

import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import entities.Photo;
import es.maglesrevista.maglesapplication.Match;
import es.maglesrevista.maglesapplication.R;
import utils.Request;

/**
 * Created by Romano on 11/11/2015.
 */
public class AdapterList extends BaseAdapter {
    private Context mContext;
    WindowManager wm;
    Display display;
    private final ArrayList<Photo> Imageid;

    public AdapterList(Context c,ArrayList<Photo> Imageid ) {
        mContext = c;
        this.Imageid = Imageid;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.Imageid.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        PhotoView grid;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = (PhotoView)inflater.inflate(R.layout.widget_listachat_item, null);


        } else {
            grid = (PhotoView) convertView;
        }
        try {
            if (Imageid.size() > 0) {
                Photo photo = Imageid.get(position);
                grid.setId(photo.getID_USER());
                grid.setModel(photo.getID_USER());
                if (photo.getPathComplete() != null) {
                    Picasso.with(mContext)
                            .load(Request.BASE_URL + "photos/file/id/" + photo.getID_USER() + "/namePhoto/" + photo.getPathComplete())
                            .placeholder(R.mipmap.photo_default)
                            .resize(display.getWidth() / 3, display.getWidth() / 3)
                            .centerCrop()
                            .into(grid.getImageView());

                }
            }
        }
        catch (Exception e){}
        return grid;
    }
}