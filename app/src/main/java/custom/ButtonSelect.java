package custom;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import entities.KeyPairValue;
import es.maglesrevista.maglesapplication.R;
import utils.TextFont;

/**
 * Created by Romano on 04/09/2015.
 */
public class ButtonSelect extends LinearLayout implements View.OnClickListener {
    String  hint="";
    int image_icon;
    String selectText;
    String selectValue;
    Object  Object;
    Dialog dialog;
    Context context;
    Drawable drawableBottom=null;
    Drawable drawableRight=null;
    View mView;
    ImageView icon;
    String align;
    TextView text,title_dialog;
    int textSize;
    boolean textSelectedBold=true;
    ButtonSelectAdapter adapter;
    boolean show_search=false;
    public ButtonSelect(Context context) {
        super(context);
        this.context=context;
        configure();
        setOnClickListener(this);
    }

    public ButtonSelect(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        init(attrs);
        configure();
        setOnClickListener(this);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ButtonSelect(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        init(attrs);
        configure();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ButtonSelect(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context=context;
        configure();
    }

    public void init(AttributeSet attrs)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.button_select);
        hint=a.getString(R.styleable.button_select_hint);
        image_icon=a.getResourceId(R.styleable.button_select_button_icon, R.mipmap.default_button);
        textSize=a.getInteger(R.styleable.button_select_button_textSize, 14);
        align=a.getString(R.styleable.button_select_button_icon_align);
        textSelectedBold=a.getBoolean(R.styleable.button_select_button_textSelectedBold, true);
        show_search=a.getBoolean(R.styleable.button_select_button_show_search, false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(align.equals("3")) {
            mView = inflater.inflate(R.layout.button_select_right, this, false);
        }
        else
        {
            mView = inflater.inflate(R.layout.button_select_bottom, this, false);
        }
        text=(TextView)mView.findViewById(R.id.selected_text);
        text.setTextSize(textSize);
        icon=(ImageView)mView.findViewById(R.id.icon_selected);
        text.setText(hint);
        icon.setImageResource(image_icon);
        addView(mView);
    }
    public void setSelectItem(String key){
        for (KeyPairValue<Object,Object> value:dataSource)
        {
            if (value.getKey().equals(key)) {
                setSelectItem((KeyPairValue) value);
            }

        }
    }
    public void configure(){
        if(!isInEditMode()) {
            try {
                WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Activity activity = (Activity) context;
                dialog = new Dialog(activity,R.style.dialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.requestWindowFeature(Window.FEATURE_ACTION_MODE_OVERLAY);
                LayoutInflater li = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = li.inflate(R.layout.button_select_list, null, false);
                search=(EditText)v.findViewById(R.id.input_search);
                if(show_search)
                    search.setVisibility(View.VISIBLE);
                else
                    search.setVisibility(View.INVISIBLE);
                lv = (ListView) v.findViewById(R.id.item_select);
                title_dialog=(TextView)v.findViewById(R.id.title_dialog);
                title_dialog.setText(hint);
                dialog.setContentView(v);
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.BOTTOM;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                search.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                            adapter.getFilter().filter(cs);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                    }
                });
            }
            catch (Exception e){}


        }
    }
    public void dataBind(){

        if(dataSource.size()>0) {
            adapter = new ButtonSelectAdapter(context, dataSource);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectItem = (KeyPairValue) parent.getItemAtPosition(position);
                    setSelectItem(selectItem);
                    dialog.hide();
                }
            });
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        showDialog();
    }
    public void showDialog() {
        dialog.show();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if (dataSource!=null) {
            if (dataSource.size() > 5)
                dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, (display.getHeight() / 3) - 10);
            else
                dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        }
    }
    public void closeDialog()
    {
        dialog.hide();
    }
    public ListView getLv() {
        return lv;
    }
    ListView lv;
    EditText search;
    public KeyPairValue<String, String> getSelectItem() {
        return selectItem;
    }
    public void setSelectItem(KeyPairValue<String, String> selectItem) {
        this.selectItem = selectItem;
        this.text.setText(selectItem.getValue());
        if(!selectItem.getKey().equals("")) {
            this.icon.setImageResource(R.mipmap.default_button);
            if(textSelectedBold)
                this.text.setTypeface(TextFont.getInstance().getFont(context, TextFont.PATH_FONT_BOLD));
            else
                this.text.setTypeface(TextFont.getInstance().getFont(context, TextFont.PATH_FONT));
            this.text.setTextColor(Color.parseColor("#7b7677"));
            if(align.equals("3"))
            {
                ViewGroup.LayoutParams params= this.icon.getLayoutParams();
                params.width=0;
                this.icon.setLayoutParams(params);
            }

        }
        else {

            this.text.setTypeface(TextFont.getInstance().getFont(context,TextFont.PATH_FONT));
            this.text.setTextColor(Color.parseColor("#7b7677"));
            this.icon.setImageResource(image_icon);
            if(align.equals("3"))
            {
                ViewGroup.LayoutParams params= this.icon.getLayoutParams();
                params.width=0;
                this.icon.setLayoutParams(params);
            }
        }

    }
    KeyPairValue<String,String> selectItem;
    public ArrayList<KeyPairValue<java.lang.Object, java.lang.Object>> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<KeyPairValue<java.lang.Object, java.lang.Object>> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<KeyPairValue<Object,Object>> dataSource;
    ArrayList<String> values;
    public void setHint(String hint) {
        this.hint = hint;
        this.text.setText(hint);
    }




}
