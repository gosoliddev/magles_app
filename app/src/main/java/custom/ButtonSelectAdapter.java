package custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import entities.KeyPairValue;
import entities.SpinnerValue;
import es.maglesrevista.maglesapplication.R;

/**
 * Created by Romano on 04/09/2015.
 */
public class ButtonSelectAdapter  extends BaseAdapter implements Filterable{
    private ArrayList<KeyPairValue<Object,Object>> items;
    Context context;
    ValueFilter valueFilter;
    public ButtonSelectAdapter(Context context,  ArrayList<KeyPairValue<Object,Object>>items ) {
        this.items = items;
        this.context=context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        KeyPairValue<Object,Object> item=items.get(position);
        TextView txt_item;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.button_select_item, null);
        }
        txt_item = (TextView) convertView.findViewById(R.id.text_value);

        txt_item.setText((String)item.getValue());
        return convertView;
    }
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    private class ValueFilter extends Filter {
        ArrayList<KeyPairValue<Object,Object>> data_origin=items;
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            ArrayList<KeyPairValue<Object,Object>> filters_data = new ArrayList<KeyPairValue<Object, Object>>();

            constraint = constraint.toString().toLowerCase();
            if(!constraint.equals("")) {
                for (int i = 0; i < data_origin.size(); i++) {
                    KeyPairValue<Object, Object> value = data_origin.get(i);
                    if (value.getValue().toString().toLowerCase().startsWith(constraint.toString())) {
                        filters_data.add(value);
                    }
                }
                results.count = filters_data.size();
                results.values = filters_data;
            }
            else
            {
                results.count = data_origin.size();
                results.values = data_origin;
            }




            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<KeyPairValue<Object,Object>>) results.values;
            notifyDataSetChanged();
        }

    }
}


