package custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.koushikdutta.async.http.socketio.StringCallback;

import java.util.ArrayList;

import entities.SpinnerValue;
import es.maglesrevista.maglesapplication.R;

/**
 * Created by Romano on 26/08/2015.
 */
public class ErrorAdapter extends BaseAdapter{
    protected Context context;
    protected ArrayList<String> errors_arraylist;
    public ErrorAdapter(Context context, ArrayList<String> errors_arraylist){
        this.context = context;
        this.errors_arraylist= errors_arraylist;
    }
    @Override
    public int getCount() {
        return errors_arraylist.size();
    }
    @Override
    public Object getItem(int position) {

        String error=errors_arraylist.get(position);
        return error;

    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        TextView txt_item;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.error_item, null);
        }
        String error_value = errors_arraylist.get(position);
        txt_item = (TextView) vi.findViewById(R.id.txt_message_error);
        txt_item.setText(error_value);
        return vi;
    }
}
