package custom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import es.maglesrevista.maglesapplication.Profile;
import es.maglesrevista.maglesapplication.R;
import utils.ImageUtils;
import utils.Request;

/**
 * Created by Romano on 29/07/2015.
 */
public class Image extends LinearLayout  implements View.OnClickListener  {
    View mView;
    ImageView img;
    Context context;
    String uri="base/file";
    public static final String TYPE_GALERY="galery";
    public static final  String TYPE_LIST="button_select_list";
    public static final  String TYPE_LIST_GALERY="list_galery";
    public int id=0;
    float weigth;
    public Image(Context context, String uri,String type_list,int id) {
        super(context);
        this.context=context;
        this.uri=uri;
        this.id=id;
        setOnClickListener(this);
        init(type_list);
    }
    public Image(Context context, String uri,String type_list,float weigth,int id) {
        super(context);
        this.context=context;
        this.uri=uri;
        this.weigth=weigth;
        this.id=id;
        setOnClickListener(this);
        init(type_list);
    }
    private void initGalery(float weigth) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.image_gallery, this, false);
        addView(mView, 0);
        img=(ImageView)mView.findViewById(R.id.imageShape);
        WindowManager wm = (WindowManager) this.context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(display.getWidth()/3, LayoutParams.WRAP_CONTENT,weigth);
        img.setLayoutParams(params);
        getDrawable();
    }
    private void initList() {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.image_list, this, false);
        addView(mView, 0);
        img=(ImageView)mView.findViewById(R.id.imageShape);
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        img.setLayoutParams(params);
        getDrawable();
    }
    private void initListGalery() {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.image, this, false);
        addView(mView, 0);
        img=(ImageView)mView.findViewById(R.id.imageShape);
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        img.setLayoutParams(params);
        getDrawable();
    }
    public void init(String type)
    {
      switch (type){
          case TYPE_GALERY:{
              initGalery(this.weigth);
              break;
          }
          case TYPE_LIST:{
              initList();
              break;
          }
          case TYPE_LIST_GALERY:{
              initListGalery();
              break;
          }
      }
    }

    public  void getDrawable( ){
        if(this.uri!="") {
            Request.get(this.uri, null, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = ImageUtils.decodeFile(bytes);//BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
                        img.setImageBitmap(bitmap);
                        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                        Display display = wm.getDefaultDisplay();
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(display.getWidth() / 3 - 15, display.getWidth() / 3 - 15);
                        params.setMargins(3, 3, 3, 3);
                        img.setLayoutParams(params);
                    } catch (Exception ex) {
                        img.setImageResource(R.mipmap.photo_default);

                    }
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    img.setImageResource(R.mipmap.photo_default);

                }
            });
        }
        else
        {
            img.setImageResource(R.mipmap.photo_default);
        }
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();
        addView(mView, 0);

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }


    @Override
    public void onClick(View v) {
        Intent intent=new Intent(context, Profile.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("userId",this.id);
        context.startActivity(intent);
    }
    public ImageView getImageView(){
        return img;
    }
}
