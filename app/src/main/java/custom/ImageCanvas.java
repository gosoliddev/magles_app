package custom;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

import es.maglesrevista.maglesapplication.R;
import utils.ImageUtils;

/**
 * Created by Romano on 19/08/2015.
 */
public class ImageCanvas extends RelativeLayout {

    Rect button;
    static Context context;
    private Uri mUri;
    ImageView image;
    View mView;
    static ProgressBar mProgressBar;
    RelativeLayout.LayoutParams params;
    Bitmap mBitmap;
    public ImageCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        configure(context,attrs);
    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }
    public void configure(Context context,AttributeSet attrs)
    {
        TypedArray a=getContext().obtainStyledAttributes(attrs, R.styleable.image_canvas);
        String uriString = a.getString(R.styleable.image_canvas_src_uri);
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.image_canvas, this, false);
        image=(ImageView)mView.findViewById(R.id.image_canvas);
        image.setVisibility(View.GONE);
        mProgressBar=(ProgressBar)mView.findViewById(R.id.progress);
        mProgressBar.setIndeterminate(false);
        mProgressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.color_btn_magles),
                android.graphics.PorterDuff.Mode.SRC_IN);
        mProgressBar.setMax(100);
        addView(mView);
        if(uriString!=null && !uriString.isEmpty())
            this.setImageURI(Uri.parse(uriString));
    }

    public void showSpinner(){

    }
    public void setImageURI(Uri uri) {
        mUri = uri;
        showSpinner();
        new ImageDownloader(image).execute(mUri);
   }

    private static class ImageDownloader extends AsyncTask<Uri, Void, Bitmap> {
        ImageView mRef;

        public ImageDownloader(ImageView bmImage) {
            mRef =bmImage;
        }

        protected Bitmap doInBackground(Uri... uris) {
            String url = uris[0].toString();
            Bitmap image = null;
            InputStream in;
            try {
                in = new java.net.URL(url).openStream();
                image = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return image;
        }

        protected void onPostExecute(Bitmap result) {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            final Display display = wm.getDefaultDisplay();
                mProgressBar.setVisibility(View.GONE);
                mRef.setImageBitmap(result);
                mRef.setVisibility(View.VISIBLE);
//                ViewGroup.LayoutParams params=mRef.getLayoutParams();
//                params.width=display.getWidth()-195;

        }
    }


}
