package custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import es.maglesrevista.maglesapplication.Profile;
import es.maglesrevista.maglesapplication.R;
import utils.ImageUtils;
import utils.Request;

/**
 * Created by Romano on 18/08/2015.
 */
public class PhotoView extends LinearLayout implements View.OnClickListener{
    String uri="";
    int id=0;
    Context context;
    View mView;
    AttributeSet attrs;
    android.widget.ImageView image;
    int photo_default;

    public Bitmap getBmp_photo() {
        return bmp_photo;
    }

    public void setBmp_photo(Bitmap bmp_photo) {
        this.bmp_photo = bmp_photo;
    }

    Bitmap bmp_photo;
    public Object getDataItem() {
        return dataItem;
    }

    public void setDataItem(Object dataItem) {
        this.dataItem = dataItem;
    }

    Object dataItem;


    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    int columns;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type="";
    public static final String  TYPE_CIRCLE="circle";
    public PhotoView(Context context) {
        super(context);
        this.context=context;
    }

    public PhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        columns=1;
        configure();
    }

    @SuppressLint("NewApi")
    public PhotoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        this.attrs=attrs;
        columns=1;
        configure();
    }

    @SuppressLint("NewApi")
    public PhotoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context=context;
        this.attrs=attrs;
        columns=1;
        configure();
    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }
    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_imageview);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_imageview_image_default:{
                    photo_default=a.getResourceId(0,R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_imageview_image_uri:{
                    uri=a.getString(1);
                    break;
                }
                case R.styleable.widget_imageview_image_type: {
                    type=a.getString(2);
                    break;
                }
                case R.styleable.widget_imageview_image_columns: {
                    columns=a.getInteger(3,1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (type){
            case TYPE_CIRCLE: {
                mView = inflater.inflate(R.layout.widget_imagecircle, this, false);
                break;
            }
            default:{
                mView = inflater.inflate(R.layout.widget_imagesquare, this, false);
                break;
            }
        }
        addView(mView);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        image = (android.widget.ImageView) mView.findViewById(R.id.image);
        image.setAdjustViewBounds(true);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if(!this.uri.equals("")){
            getDrawable();
        }
        else
        {
            image.setImageResource(photo_default);
        }
        this.setOnClickListener(this);
    }

    public void getDrawable() {
        if(this.uri!="") {
            Request.get(this.uri, null, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    Runtime.getRuntime().gc();
                    System.gc();
                    try {
                        String cadena=new String(bytes,"UTF-8");
                        if(!cadena.equals("")) {
                            Bitmap bitmap = ImageUtils.decodeFile(bytes, 150);
                            image.setImageBitmap(bitmap);
                            image.setAdjustViewBounds(true);
                            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        }
                        else
                            image.setImageResource(photo_default);


                    } catch (Exception ex) {
                        image.setImageResource(photo_default);
                    }
                    catch (OutOfMemoryError e)
                    {
                        image.setImageResource(photo_default);
                    }
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    image.setImageResource(photo_default);

                }
            });
        }
        else {
            image.setImageResource(photo_default);
            image.setAdjustViewBounds(true);
        }
    }
    public int getPhoto_default() {
        return photo_default;
    }

    public void setPhoto_default(int photo_default) {
        this.photo_default = photo_default;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getModel() {
        return id;
    }

    public void setModel(int id) {
        this.id = id;
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent(context, Profile.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("userId",(int)this.id);
        context.startActivity(intent);
    }
    public void scaleImage(int boundBoxInDp)
    {


            // Get the ImageView and its bitmap
            Drawable drawing = image.getDrawable();
            Bitmap bitmap = ((BitmapDrawable)drawing).getBitmap();

            // Get current dimensions
            int width = bitmap.getWidth()>0?bitmap.getWidth():100;
            int height = bitmap.getHeight()>0?bitmap.getHeight():100;

            // Determine how much to scale: the dimension requiring less scaling is
            // closer to the its side. This way the image always stays inside your
            // bounding box AND either x/y axis touches it.
            float xScale = ((float) boundBoxInDp) / width;
            float yScale = ((float) boundBoxInDp) / height;
            float scale = (xScale <= yScale) ? xScale : yScale;

            // Create a matrix for the scaling and add the scaling data
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);

            // Create a new bitmap and convert it to a format understood by the ImageView
            Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
            BitmapDrawable result = new BitmapDrawable(scaledBitmap);
            width = scaledBitmap.getWidth();
            height = scaledBitmap.getHeight();

            // Apply the scaled bitmap
            image.setImageDrawable(result);

            // Now change ImageView's dimensions to Match the scaled image
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) image.getLayoutParams();
            params.width = width;
            params.height = height;
            image.setLayoutParams(params);
            image.setAdjustViewBounds(true);


    }

    private int dpToPx(int dp)
    {
        float density = this.context.getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }
    public void setPhotoBitmap(Bitmap bitmap){
        image.setImageBitmap(bitmap);
    }
    public ImageView getImageView(){
        return this.image;
    }
}
