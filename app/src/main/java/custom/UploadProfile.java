package custom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edmodo.cropper.CropImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import es.maglesrevista.maglesapplication.R;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;

/**
 * Created by Romano on 20/08/2015.
 */
@SuppressLint("NewApi")
public class UploadProfile extends DialogFragment{

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    ImageView btn_galery,btn_cut,btn_cancel,btn_camera,preview;
    CropImageView imageView;
    TextView btn_upload;
    private File file;
    String namePhoto="";
    int userId;
    Bitmap bmp_cut;

    public String  type_upload="";
    public static final String UPLOAD_GALLERY="gallery";
    public static final String UPLOAD_PROFILE="profile";
    public static final String UPLOAD_CONFIG="config";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Subir imagen perfil");
        final View rootView = inflater.inflate(R.layout.camera_image, container, false);
        btn_camera = (ImageView) rootView.findViewById(R.id.btn_camera);
        btn_galery= (ImageView) rootView.findViewById(R.id.btn_gallery);
        btn_cancel= (ImageView) rootView.findViewById(R.id.btn_cancel);
        btn_upload= (TextView) rootView.findViewById(R.id.btn_subir);
        btn_cut= (ImageView) rootView.findViewById(R.id.btn_cut);
        preview = (ImageView) rootView.findViewById(R.id.preview);
        imageView = (CropImageView) rootView.findViewById(R.id.imageview);

        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.VISIBLE);
                preview.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, 1);
            }
        });
        btn_galery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setVisibility(View.VISIBLE);
                preview.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadProfile.this.dismiss();
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bmp_cut=imageView.getCroppedImage();
                    String file_name = "temp_file.jpg";
                    file_name = ImageUtils.getSDCardPath() + "/" + file_name;
                    FileOutputStream out = new FileOutputStream(file_name);

                    if(bmp_cut!=null) {
                        bmp_cut.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    }
                    else
                    {
                        bmp_cut = imageView.getCroppedImage();
                        bmp_cut.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    }

                    out.flush();
                    out.close();
                    String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
                    uploadToProfile(file_name, timeStamp + ".jpg");
                } catch(Exception e) {
                    String msg=e.getMessage();
                    Toast.makeText(getActivity().getApplicationContext(),"No selecciono ninguna imagen",Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bmp_cut = imageView.getCroppedImage();
                imageView.setImageBitmap(imageView.getCroppedImage());
            }
        });
        return rootView;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    System.gc();
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte [] byte_arr = stream.toByteArray();
                    this.imageView.setImageBitmap(bitmap);
                    String namePhoto = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime())+".jpg";
                    f.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            }

            if (requestCode == 2) {
                System.gc();
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                this.imageView.setImageBitmap(thumbnail);
            }
        }
    }
    public void uploadToProfile(String path,String nameFile){
        try {
            file = new File(path);
            if (nameFile.equals(""))
                namePhoto=file.getName();
            else
                namePhoto=nameFile;
            RequestParams params1 = new RequestParams();
            params1.put("file", new FileInputStream(file),namePhoto);
            AsyncHttpClient client = new AsyncHttpClient();
            Preloader.getInstance(getActivity()).dialog().show();
            client.post(Request.BASE_URL+"/photos/upload",params1, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    String cadena = null;
                    try {
                        cadena = new String(bytes, "UTF-8");
                        RequestParams params=new RequestParams();
                        params.put("ID_USER",userId);
                        params.put("namePhoto",namePhoto);
                        Request.post("galleries/changePhotoProfile",params,new AsyncHttpResponseHandler(){

                            @Override
                            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                try {
                                    String cadena = new String(bytes, "UTF-8");
                                    if(cadena.equals("")) {
                                        imageView.setImageResource(R.mipmap.photo_default);
                                    }
                                    Preloader.getInstance(getActivity()).dialog().cancel();
                                    IUploadProfile activity = (IUploadProfile) getActivity();
                                    activity.updateResult();
                                    UploadProfile.this.dismiss();
                                }
                                catch(Exception e)
                                {
                                    Preloader.getInstance(getActivity()).dialog().cancel();
                                    imageView.setImageResource(R.mipmap.photo_default);
                                    UploadProfile.this.dismiss();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                try {
                                    String cadena = new String(bytes, "UTF-8");
                                    imageView.setImageResource(R.mipmap.photo_default);
                                    Preloader.getInstance(getActivity()).dialog().cancel();
                                    UploadProfile.this.dismiss();
                                }catch (Exception e){}
                            }
                        });
                    } catch (Exception e) {
                        Preloader.getInstance(getActivity()).dialog().cancel();
                        imageView.setImageResource(R.mipmap.photo_default);
                        UploadProfile.this.dismiss();
                    }

                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    try {
                        String cadena = new String(bytes, "UTF-8");
                    }catch (Exception e){}
                    Preloader.getInstance(getActivity()).dialog().cancel();
                    imageView.setImageResource(R.mipmap.photo_default);
                    UploadProfile.this.dismiss();
                }
            });
        }
        catch (FileNotFoundException e)
        {
            Preloader.getInstance(getActivity()).dialog().cancel();
            imageView.setImageResource(R.mipmap.photo_default);
            UploadProfile.this.dismiss();
        }

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }



    public String getType_upload() {
        return type_upload;
    }

    public void setType_upload(String type_upload) {
        this.type_upload = type_upload;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public interface IUploadProfile {
        void updateResult();
    }
}