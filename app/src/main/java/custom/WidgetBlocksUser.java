package custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;

import entities.MagHug;
import entities.Photo;
import es.maglesrevista.maglesapplication.Profile;
import es.maglesrevista.maglesapplication.R;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;

/**
 * Created by Romano on 19/08/2015.
 */
public class WidgetBlocksUser extends LinearLayout {
    Context context;
    PhotoView image;
    WindowManager wm ;
    Display display ;
    Dialog dialog;
    public ArrayList<Photo> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<Photo> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<Photo> dataSource=null;
    View mView;
    View mViewItem;
    AttributeSet attrs;
    int photo_dafault=0;
    LinearLayout root_blockeduser;
    int width;
    int height;

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    int columns;
    float weight;
    public WidgetBlocksUser(Context context)
    {
        super(context);
        this.context=context;this.attrs=attrs;


    }
    public WidgetBlocksUser(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        configure();
        dataBind();
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }

    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_favorite);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_favorite_photo_default:{
                    photo_dafault = a.getResourceId(0, R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_favorite_columns:{
                    columns = a.getInteger(1, 1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_blockeduser, this, false);
        addView(mView);
        root_blockeduser=(LinearLayout)mView.findViewById(R.id.root_blockeduser);
    }
    public void dataBind()
    {

        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        LinearLayout conteiners=null;
        int i=0;
        if(dataSource!=null && dataSource.size()>0) {
            for (final Photo photo : dataSource) {
                if (i % columns == 0) {
                    conteiners = new LinearLayout(this.context);
                    conteiners.setOrientation(HORIZONTAL);
                    conteiners.setGravity(Gravity.CENTER_HORIZONTAL);
                    conteiners.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                    root_blockeduser.addView(conteiners);
                }
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mViewItem = inflater.inflate(R.layout.widget_block_item, this, false);
                PhotoView image = (PhotoView)mViewItem.findViewById(R.id.photo_userbocked);
                PhotoView icon_blocked=(PhotoView)mViewItem.findViewById(R.id.icon_blocked);
                //icon_blocked.setPhoto_default(R.mipmap.icon_locked);
                icon_blocked.setDataItem(photo);

                image.setModel(photo.getID_USER());
                image.setType("circle");
                image.setColumns(columns);
                image.setUri("photos/file/id/" + photo.getID_USER() + "/namePhoto/" + photo.getPathComplete());
                image.getDrawable();
                image.scaleImage((display.getWidth() / columns)-10);

                icon_blocked.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog = new Dialog(context);
                        final PhotoView photoView = (PhotoView) v;
                        final Photo photo = (Photo) photoView.getDataItem();
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_confirm);
                        TextView txtTitle = (TextView) dialog.findViewById(R.id.title_dialog);
                        TextView texto = (TextView) dialog.findViewById(R.id.text_dialog);
                        texto.setVisibility(View.INVISIBLE);
                        texto.setWidth(0);
                        texto.setHeight(0);
                        TextView txt_ok = (TextView) dialog.findViewById(R.id.txt_si);
                        TextView txt_no = (TextView) dialog.findViewById(R.id.txt_no);
                        txtTitle.setText(Html.fromHtml("¿QUIERES DESBLOQUEAR A <font color='#bf284a'>" + photo.getName() + "</font>?"));
                        txt_no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });
                        txt_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sendUnLocked(photoView, BaseHelper.getUserId(context), photo.getID_USER());

                            }
                        });
                        dialog.show();
                    }
                });
                LinearLayout ln=new LinearLayout(context);
                LinearLayout.LayoutParams params=new LinearLayout.LayoutParams((display.getWidth() / columns)-10, LayoutParams.WRAP_CONTENT);
                params.setMargins(2, 2, 2, 2);
                ln.setLayoutParams(params);

                ln.addView(mViewItem);
                conteiners.addView(ln);
                i++;

            }
        }
    }
    public void sendUnLocked(final View view,int ownerId, final int contactId){

        Preloader.getInstance((Activity)context).dialog().show();
        RequestParams params=new RequestParams();
        params.put("user_id", ownerId);
        params.put("user_id_locked",contactId );
        Request.post("RelationShips/removeLocked/", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    boolean status = response.getBoolean("status");
                    if (status) {
                        //Toast.makeText(context, "Usuario fue retirado de tu lista de bloqueados", Toast.LENGTH_LONG).show();
                        Preloader.getInstance((Activity)context).dialog().cancel();
                        Intent intent=new Intent(context, Profile.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        intent.putExtra("userId",contactId);
                        context.startActivity(intent);
                        dialog.cancel();
                    } else {
                        Toast.makeText(context, "Error al intentar marcar bloqueado", Toast.LENGTH_SHORT).show();
                        Preloader.getInstance((Activity)context).dialog().cancel();
                        dialog.cancel();
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Error al intentar marcar bloqueado", Toast.LENGTH_SHORT).show();
                    Preloader.getInstance((Activity)context).dialog().cancel();
                    dialog.cancel();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(context, "Error al intentar marcar bloqueado", Toast.LENGTH_SHORT).show();
                Preloader.getInstance((Activity)context).dialog().cancel();
                dialog.cancel();
            }
        });
    }

    public void clear() {
        root_blockeduser.removeAllViews();
    }
}
