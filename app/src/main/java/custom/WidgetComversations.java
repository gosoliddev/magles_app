package custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.comcast.freeflow.layouts.HLayout;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import entities.Photo;
import es.maglesrevista.maglesapplication.MainActivity;
import es.maglesrevista.maglesapplication.R;
import other.Message;
import other.MessageChatUser;
import utils.BaseHelper;
import utils.DateUtils;

/**
 * Created by Romano on 01/09/2015.
 */
public class WidgetComversations extends LinearLayout{
    Context context;
    PhotoView image;
    WindowManager wm ;
    Display display ;
    public ArrayList<MessageChatUser> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<MessageChatUser> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<MessageChatUser> dataSource=null;
    View mView;
    View mViewItem;
    AttributeSet attrs;
    int photo_dafault=0;
    LinearLayout root_messages;
    int width;
    int height;

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    int columns;
    float weight;
    public WidgetComversations(Context context)
    {
        super(context);
        this.context=context;this.attrs=attrs;

    }
    public WidgetComversations(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        configure();
        dataBind();
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }

    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_favorite);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_favorite_photo_default:{
                    photo_dafault = a.getResourceId(0, R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_favorite_columns:{
                    columns = a.getInteger(1, 1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_comversations, this, false);
        addView(mView);
        root_messages=(LinearLayout)mView.findViewById(R.id.root_comversations);
    }
    public void dataBind()
    {
        Date fecha_actual = new Date();
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        final LinearLayout conteiners=null;
        int i=0;
        if(dataSource!=null && dataSource.size()>0) {
            for (final MessageChatUser message : dataSource) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mViewItem = inflater.inflate(R.layout.widget_comversations_item, this, false);
                LinearLayout btnComversation = (LinearLayout)mViewItem.findViewById(R.id.btnComversation);
                btnComversation.setTag(message);
                String friend=(BaseHelper.getNickname(context).equals(message.getFromName()))?message.getReceiver():message.getFromName();
                btnComversation.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LinearLayout ln = (LinearLayout) v;
                        MessageChatUser msg = (MessageChatUser) ln.getTag();
                        String friend = (BaseHelper.getNickname(context).equals(msg.getFromName())) ? msg.getReceiver() : msg.getFromName();
                        Intent intent = new Intent((Activity) context, MainActivity.class);
                        intent.putExtra("nicknameFriend", friend);
                        intent.putExtra("id",msg.getID_USER());
                        intent.putExtra("name", BaseHelper.getNickname(context));
                        context.startActivity(intent);
                    }
                });
                TextView txtUser = (TextView)mViewItem.findViewById(R.id.txtUsr);
                TextView txtMsg = (TextView)mViewItem.findViewById(R.id.txtMsg);
                String strMesage=message.getMessage();
                switch (message.getType_message())
                {
                    case "request":{
                        if(BaseHelper.getNickname(context).equals(message.getFromName())) {
                            strMesage = context.getString(R.string.text_sendding_invitation);
                            strMesage = "Tú: " + strMesage.replace("#NickName", message.getReceiver());
                        }
                        else
                        {
                            strMesage = context.getString(R.string.text_received_invitacion);
                            strMesage = strMesage.replace("#NickName", message.getFromName());
                        }
                        break;
                    }

                    case "accepted":{
                        if(BaseHelper.getNickname(context).equals(message.getFromName())) {
                            strMesage = context.getString(R.string.text_send_accepted_invitacion);
                            strMesage = "Tú: " +strMesage.replace("#NickName", message.getReceiver());
                        }
                        else
                        {
                            strMesage = context.getString(R.string.text_accepted_invitacion);
                            strMesage = strMesage.replace("#NickName", message.getFromName());
                        }
                        break;
                    }
                    case "message":{
                        strMesage=(BaseHelper.getNickname(context).equals(message.getFromName()))?"Tú: "+message.getMessage():message.getMessage();
                        break;
                    }
                }


                if(!message.getIsView())
                {
                    SpannableString spanString = new SpannableString(strMesage);
                    spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
                    txtMsg.setTextColor(Color.BLACK);
                    txtMsg.setText(spanString);
                }
                else
                    txtMsg.setText(strMesage);
                TextView txtFecha = (TextView)mViewItem.findViewById(R.id.txtFecha);
                txtUser.setText(friend);

                String date_current = DateUtils.getTimeAgo(message.getFromDate(),context);
                txtFecha.setText(date_current);


                PhotoView image = (PhotoView)mViewItem.findViewById(R.id.photoMsg);
                image.setPhoto_default(R.mipmap.photo_default);
                image.setModel(message.getID_USER());
                image.setType("circle");
                image.setUri("Photos/getPhotoByNickname/nickname/"+friend);
                image.getDrawable();
                ViewGroup.LayoutParams params=image.getLayoutParams();
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                image.setLayoutParams(params);
                root_messages.addView(mViewItem);
                i++;
            }
        }
    }

    public void clear(){
        root_messages.removeAllViewsInLayout();
    }
}
