package custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import entities.Photo;
import es.maglesrevista.maglesapplication.R;

/**
 * Created by Romano on 17/08/2015.
 */
public class WidgetFavorites extends LinearLayout {
    Context context;
    custom.PhotoView image;
    WindowManager wm ;
    Display display ;
    public ArrayList<Photo> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<Photo> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<Photo> dataSource=null;
    View mView;
    AttributeSet attrs;
    int photo_dafault=0;
    LinearLayout root_favorites;
    int width;
    int height;

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    int columns;
    float weight;
    public WidgetFavorites(Context context)
    {
        super(context);
        this.context=context;this.attrs=attrs;

    }
    public WidgetFavorites(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        configure();
        dataBind();
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }

    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_favorite);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_favorite_photo_default:{
                    photo_dafault = a.getResourceId(0, R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_favorite_columns:{
                    columns = a.getInteger(1, 1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_favarites, this, false);
        addView(mView);
        root_favorites=(LinearLayout)mView.findViewById(R.id.root_favorites);
    }
    public void dataBind()
    {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        LinearLayout conteiners=null;
        int i=0;
        if(dataSource!=null && dataSource.size()>0) {
            for (Photo photo : dataSource) {
                if (i % columns == 0) {
                    conteiners = new LinearLayout(this.context);
                    conteiners.setOrientation(HORIZONTAL);
                    conteiners.setGravity(Gravity.CENTER_HORIZONTAL);
                    LayoutParams params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                    conteiners.setPadding(5,5,5,5);
                    conteiners.setLayoutParams(params);
                    root_favorites.addView(conteiners);
                }
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mViewItem = inflater.inflate(R.layout.widget_favorites_item, this, false);
                TextView txtNombre=(TextView)mViewItem.findViewById(R.id.txt_favorite_name);
                txtNombre.setText(photo.getName());
                image = (PhotoView)mViewItem.findViewById(R.id.image_item_favorite);
                image.setModel(photo.getID_USER());
                image.setType("circle");
                image.setColumns(columns);
                image.setUri("photos/file/id/" + photo.getID_USER() + "/namePhoto/" + photo.getPathComplete());
                image.getDrawable();
                image.scaleImage((display.getWidth()/3-20));
                LinearLayout ln=new LinearLayout(context);
                LinearLayout.LayoutParams params=new LinearLayout.LayoutParams((display.getWidth() / columns)-10, LayoutParams.WRAP_CONTENT);
                params.setMargins(2, 2, 2, 2);
                ln.setPadding(5,5,5,5);
                ln.setGravity(Gravity.CENTER_HORIZONTAL);
                ln.setLayoutParams(params);
                ln.addView(mViewItem);
                conteiners.addView(ln);
                i++;

            }
        }
    }


    public void clear() {
        root_favorites.removeAllViews();
    }
}
