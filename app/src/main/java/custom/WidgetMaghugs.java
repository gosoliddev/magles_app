package custom;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.nfc.FormatException;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import entities.MagHug;
import es.maglesrevista.maglesapplication.R;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;

/**
 * Created by Romano on 17/08/2015.
 */
public class WidgetMaghugs extends LinearLayout {
    Context context;
    PhotoView image;
    WindowManager wm ;
    Display display ;
    public ArrayList<MagHug> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<MagHug> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<MagHug> dataSource=null;
    View mView;
    View mViewItem;
    AttributeSet attrs;
    int photo_dafault=0;
    LinearLayout root_maghug;
    int width,height,columns;
    float weight;
    public WidgetMaghugs(Context context)
    {
        super(context);
        this.context=context;this.attrs=attrs;

    }
    public WidgetMaghugs(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        configure();
        dataBind();
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }

    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_favorite);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_favorite_photo_default:{
                    photo_dafault = a.getResourceId(0, R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_favorite_columns:{
                    columns = a.getInteger(1, 1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_maghug, this, false);
        addView(mView);
        root_maghug=(LinearLayout)mView.findViewById(R.id.root_maghug);
    }
    public void dataBind()
    {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        LinearLayout conteiners=null;
        int i=0;
        if(dataSource!=null && dataSource.size()>0) {
            for (MagHug maghug : dataSource) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mViewItem = inflater.inflate(R.layout.widget_maghug_item, this, false);
                final LinearLayout container_action=(LinearLayout)mViewItem.findViewById(R.id.container_action);
                container_action.setTag(R.string.user_id,maghug.getID_USER());
                container_action.setTag(R.string.user_nickname,maghug.getName_contact());
                PhotoView image = (PhotoView)mViewItem.findViewById(R.id.image_item_maghug);

                TextView  txt_name= (TextView)mViewItem.findViewById(R.id.text_name);
                TextView  txt_message= (TextView)mViewItem.findViewById(R.id.text_message);
                final TextView  txt_link= (TextView)mViewItem.findViewById(R.id.text_link);
                txt_name.setText(maghug.getName_contact());

                SimpleDateFormat formatter = new SimpleDateFormat("dd|MM|yyyy");
                try {
                    String date_current =formatter.format(maghug.getEntry_date());
                    txt_message.setText("Te mandó un MagHug el " + date_current);
                } catch (Exception e) {
                    e.printStackTrace();
                    txt_message.setText("Te mandó un MagHug el ");
                }

                if(maghug.getReturned()!=1) {
                    txt_link.setText("¡Devuélvele un MagHug!");
                }

                image.setModel(maghug.getID_USER());
                image.setType("circle");
                image.setUri("photos/file/id/" + maghug.getID_USER() + "/namePhoto/" + maghug.getPathComplete());
                image.getDrawable();

                image.scaleImage((display.getWidth()/3)-45);
                if(maghug.getReturned()!=1) {
                    container_action.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout linear = (LinearLayout) v;
                            String id = String.valueOf(linear.getTag(R.string.user_id));
                            final String nickname = String.valueOf(linear.getTag(R.string.user_nickname));
                            RequestParams params = new RequestParams();
                            params.put("user_id", BaseHelper.getUserId(context));
                            params.put("user_id_maghug", id);
                            Request.post("RelationShips/saveMaghug/", params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    super.onSuccess(statusCode, headers, response);
                                    try {

                                        int status = response.getInt("MagHug");
                                        if (status == 1) {
                                            Preloader.getInstance((Activity) context).dialogInfo("Se enviado un MagHug a " + nickname);
                                            txt_link.setText("");
                                            container_action.setClickable(false);
                                        } else if (status == 0) {
                                            Preloader.getInstance((Activity) context).dialogInfo("Se ha retirado el MagHug a " + nickname);
                                        } else {
                                            Preloader.getInstance((Activity) context).dialogInfo(nickname + " está bloqueada");
                                        }
                                    } catch (Exception e) {
                                        Preloader.getInstance((Activity) context).dialogInfo("Error no se logró enviar el MagHug");
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                    super.onFailure(statusCode, headers, throwable, errorResponse);
                                    Preloader.getInstance((Activity) context).dialogInfo("Error no se logró enviar el MagHug");
                                }
                            });
                        }
                    });
                }
                root_maghug.addView(mViewItem);
                i++;

            }
        }
    }
    public void clear(){
        root_maghug.removeAllViews();

    }

}
