package custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import entities.Photo;
import es.maglesrevista.maglesapplication.R;
import utils.ImageUtils;

/**
 * Created by Romano on 17/08/2015.
 */
public class WidgetMatch extends LinearLayout {
    Context context;
    PhotoView image;
    WindowManager wm ;
    Display display ;
    int typeView=0;

    public ArrayList<Photo> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<Photo> dataSource) {
        this.dataSource = dataSource;
    }

    ArrayList<Photo> dataSource=null;
    View mView;
    AttributeSet attrs;
    int photo_dafault=0;
    LinearLayout root_favorites;
    int width;
    int height;

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    int columns;
    float weight;
    public WidgetMatch(Context context)
    {
        super(context);
        this.context=context;this.attrs=attrs;

    }
    public WidgetMatch(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        configure();
        dataBind();
    }
    @Override
    public void onFinishInflate(){
        super.onFinishInflate();

    }
    @Override
    public boolean  isInEditMode()
    {
        return false;

    }

    public void configure(){
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.widget_favorite);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case R.styleable.widget_favorite_photo_default:{
                    photo_dafault = a.getResourceId(0, R.mipmap.photo_default);
                    break;
                }
                case R.styleable.widget_favorite_columns:{
                    columns = a.getInteger(1, 1);
                    break;
                }
            }
        }
        a.recycle();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.widget_favarites, this, false);
        addView(mView);
        root_favorites=(LinearLayout)mView.findViewById(R.id.root_favorites);
    }
    public void dataBind()
    {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        LinearLayout conteiners=null;
        int i=0;
        if(dataSource!=null && dataSource.size()>0) {
            for (Photo photo : dataSource) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mViewItem = inflater.inflate(R.layout.match_item, this, false);
                TextView txt_item = (TextView) mViewItem.findViewById(R.id.txt_nickname2);
                TextView txt_nickname = (TextView) mViewItem.findViewById(R.id.txt_nickname);
                PhotoView user_left = (PhotoView) mViewItem.findViewById(R.id.user_right);
                user_left.setModel(photo.getID_USER());
                user_left.setType("circle");
                user_left.setColumns(2);
                user_left.setUri("photos/file/id/" + photo.getID_USER() + "/namePhoto/" + photo.getPathComplete());
                user_left.getDrawable();
                user_left.scaleImage(display.getWidth() / 3);
                PhotoView user_right = (PhotoView) mViewItem.findViewById(R.id.user_left);
                user_right.scaleImage(display.getWidth() / 3);
                user_right.setClickable(false);
                loadOwnerPhoto(user_right);
                if(typeView==1)
                {
                    txt_item.setText(photo.getNickname());
                }
                else
                {
                    txt_nickname.setText(photo.getName());
                    txt_nickname.setTypeface(Typeface.DEFAULT_BOLD);
                    txt_item.setText(photo.getNickname());
                }
                root_favorites.addView(mViewItem);
                i++;
            }
        }
    }
    public void loadOwnerPhoto(PhotoView photo){
        Bitmap bmp= ImageUtils.loadFromCacheFile();
        if(bmp!=null) {
            photo.setPhotoBitmap(bmp);
        }
    }

    public void clear() {
        root_favorites.removeAllViews();
    }

    public void setTypeView(int typeView) {
        this.typeView = typeView;
    }
}
