package entities;

import java.util.ArrayList;

/**
 * Created by Romano on 07/09/2015.
 */
public class DataValues  {
    public ArrayList<KeyPairValue<Object,Object>>  getEstadoCivil(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Casada","Casada"));
        list.add(new KeyPairValue<Object, Object>("Con pareja","Con pareja"));
        list.add(new KeyPairValue<Object, Object>("Soltera","Soltera"));
        list.add(new KeyPairValue<Object, Object>("Relación abierta","Relación abierta"));
        list.add(new KeyPairValue<Object, Object>("Es complicado","Es complicado"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getEducacion(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Nivel básico","Nivel básico"));
        list.add(new KeyPairValue<Object, Object>("Nivel Medio","Nivel Medio"));
        list.add(new KeyPairValue<Object, Object>("Medio-Superior","Medio-Superior"));
        list.add(new KeyPairValue<Object, Object>("Nivel superior","Nivel superior"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getReligion(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Católica","Católica"));
        list.add(new KeyPairValue<Object, Object>("Cristiana","Cristiana"));
        list.add(new KeyPairValue<Object, Object>("Judía","Judía"));
        list.add(new KeyPairValue<Object, Object>("Atea","Atea"));
        list.add(new KeyPairValue<Object, Object>("Hindú","Hindú"));
        list.add(new KeyPairValue<Object, Object>("Ortodoxa","Ortodoxa"));
        list.add(new KeyPairValue<Object, Object>("Confucionismo","Confucionismo"));
        list.add(new KeyPairValue<Object, Object>("Budista Agnóstica","Budista Agnóstica"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }

    public ArrayList<KeyPairValue<Object,Object>>  getFisico(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Delgada","Delgada"));
        list.add(new KeyPairValue<Object, Object>("Normal","Normal"));
        list.add(new KeyPairValue<Object, Object>("Con unos kilos extras","Con unos kilos extras"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }


    public ArrayList<KeyPairValue<Object,Object>>  getOjos(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Cafés","Cafés"));
        list.add(new KeyPairValue<Object, Object>("Azules","Azules"));
        list.add(new KeyPairValue<Object, Object>("Verdes","Verdes"));
        list.add(new KeyPairValue<Object, Object>("Miel","Miel"));
        list.add(new KeyPairValue<Object, Object>("Negros","Negros"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getPelo(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Castaño","Castaño"));
        list.add(new KeyPairValue<Object, Object>("Castaño claro","Castaño claro"));
        list.add(new KeyPairValue<Object, Object>("Rubio","Rubio"));
        list.add(new KeyPairValue<Object, Object>("Negro","Negro"));
        list.add(new KeyPairValue<Object, Object>("De colores","De colores"));
        list.add(new KeyPairValue<Object, Object>("Peliroja","Peliroja"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getTataujes(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getPiercings(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }


    public ArrayList<KeyPairValue<Object,Object>>  getIntecion(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Amor","Amor"));
        list.add(new KeyPairValue<Object, Object>("Amistad","Amistad"));
        list.add(new KeyPairValue<Object, Object>("Sexo","Sexo"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getFumar(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("De vez en cuando","De vez en cuando"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }

    public ArrayList<KeyPairValue<Object,Object>>  getbeber(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("De vez en cuando","De vez en cuando"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getMascotas(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }
    public ArrayList<KeyPairValue<Object,Object>>  getNinos(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Sí","Sí"));
        list.add(new KeyPairValue<Object, Object>("En un futuro","En un futuro"));
        list.add(new KeyPairValue<Object, Object>("No","No"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }


    public ArrayList<KeyPairValue<Object,Object>>  getDeporte(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Sí","Sí"));
        list.add(new KeyPairValue<Object, Object>("Mucho","Mucho"));
        list.add(new KeyPairValue<Object, Object>("Lo necesario","Lo necesario"));
        list.add(new KeyPairValue<Object, Object>("No","No"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }

    public ArrayList<KeyPairValue<Object,Object>>  getAltura(){
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1.45","1.45"));
        list.add(new KeyPairValue<Object, Object>("1.50","1.50"));
        list.add(new KeyPairValue<Object, Object>("1.55","1.55"));
        list.add(new KeyPairValue<Object, Object>("1.60","1.60"));
        list.add(new KeyPairValue<Object, Object>("1.65","1.65"));
        list.add(new KeyPairValue<Object, Object>("1.70","1.70"));
        list.add(new KeyPairValue<Object, Object>("1.75","1.75"));
        list.add(new KeyPairValue<Object, Object>("1.80","1.80"));
        list.add(new KeyPairValue<Object, Object>("1.85","1.85"));
        list.add(new KeyPairValue<Object, Object>("1.90","1.90"));
        list.add(new KeyPairValue<Object, Object>("1.90","1.95"));
        list.add(new KeyPairValue<Object, Object>("2.00","2.00"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;

    }

    public ArrayList<KeyPairValue<Object, Object>> getUbicacion() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Indiferente","Indiferente"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }

    public ArrayList<KeyPairValue<Object, Object>> getEstilo() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Glamorosa","Glamorosa"));
        list.add(new KeyPairValue<Object, Object>("Bohemia","Bohemia"));
        list.add(new KeyPairValue<Object, Object>("Urbana","Urbana"));
        list.add(new KeyPairValue<Object, Object>("Roquera","Roquera"));
        list.add(new KeyPairValue<Object, Object>("Sofisticada","Sofisticada"));
        list.add(new KeyPairValue<Object, Object>("Hippy","Hippy"));
        list.add(new KeyPairValue<Object, Object>("Romántica","Romántica"));
        list.add(new KeyPairValue<Object, Object>("Clásica","Clásica"));
        list.add(new KeyPairValue<Object, Object>("Moderna","Moderna"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }

    public ArrayList<KeyPairValue<Object, Object>> getEdad() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("16","16"));
        list.add(new KeyPairValue<Object, Object>("20","20"));
        list.add(new KeyPairValue<Object, Object>("25","25"));
        list.add(new KeyPairValue<Object, Object>("30","30"));
        list.add(new KeyPairValue<Object, Object>("35","35"));
        list.add(new KeyPairValue<Object, Object>("40","40"));
        list.add(new KeyPairValue<Object, Object>("45","45"));
        list.add(new KeyPairValue<Object, Object>("50","50"));
        list.add(new KeyPairValue<Object, Object>("55","55"));
        list.add(new KeyPairValue<Object, Object>("65","65"));
        list.add(new KeyPairValue<Object, Object>("75","75"));
        list.add(new KeyPairValue<Object, Object>("80","80"));
        list.add(new KeyPairValue<Object, Object>("85","85"));
        list.add(new KeyPairValue<Object, Object>("90","90"));
        list.add(new KeyPairValue<Object, Object>("95","95"));
        list.add(new KeyPairValue<Object, Object>("100","100"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }
    public ArrayList<KeyPairValue<Object, Object>> getEdadPreference() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("16-26","Entre 16 y 26"));
        list.add(new KeyPairValue<Object, Object>("27-37","Entre 27 y 37"));
        list.add(new KeyPairValue<Object, Object>("38-48","Entre 38 y 48"));
        list.add(new KeyPairValue<Object, Object>("49-59","Entre 49 y 59"));
        list.add(new KeyPairValue<Object, Object>(">60","Más de 60"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }
    public ArrayList<KeyPairValue<Object, Object>> getSiNo() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("1","SÍ"));
        list.add(new KeyPairValue<Object, Object>("0","NO"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }
    public ArrayList<KeyPairValue<Object, Object>> getFreeTime() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("Cine","Cine"));
        list.add(new KeyPairValue<Object, Object>("Teatro","Teatro"));
        list.add(new KeyPairValue<Object, Object>("Playa","Playa"));
        list.add(new KeyPairValue<Object, Object>("Montaña","Montaña"));
        list.add(new KeyPairValue<Object, Object>("Bar","Bar"));
        list.add(new KeyPairValue<Object, Object>("Discoteca","Discoteca"));
        list.add(new KeyPairValue<Object, Object>("Campo","Campo"));
        list.add(new KeyPairValue<Object, Object>("Ciudad","Ciudad"));
        list.add(new KeyPairValue<Object, Object>("Restaurante","Restaurante"));
        list.add(new KeyPairValue<Object, Object>("Casa con amigos","Casa con amigos"));
        list.add(new KeyPairValue<Object, Object>("Caminatas","Caminatas"));
        list.add(new KeyPairValue<Object, Object>("Centro Comercial","Centro Comercial"));
        list.add(new KeyPairValue<Object, Object>("Salir de ambiente","Salir de ambiente"));
        list.add(new KeyPairValue<Object, Object>("Sitios hetero","Sitios hetero"));
        list.add(new KeyPairValue<Object, Object>("Museo / Cultura","Museo / Cultura"));
        list.add(new KeyPairValue<Object, Object>("Literatura","Literatura"));
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }
    public ArrayList<KeyPairValue<Object, Object>> getOcupacion() {
        ArrayList<KeyPairValue<Object,Object>> list=new ArrayList<KeyPairValue<Object, Object>>();
        list.add(new KeyPairValue<Object, Object>("","Indiferente"));
        return list;
    }
}
