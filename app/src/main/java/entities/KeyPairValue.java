package entities;

import java.io.Serializable;

/**
 * Created by Romano on 04/09/2015.
 */
public class KeyPairValue<K,V> implements Serializable {
    K key;
    V value;

    public KeyPairValue() {
    }

    public KeyPairValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
