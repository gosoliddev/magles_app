package entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Romano on 18/08/2015.
 */
public class MagHug implements Serializable {
    int ID_USER;
    int ID_USER_CONTACT;
    Date entry_date;
    int IsFavorite;
    int isMagHug;
    int TrustRelationship;

    public int getReturned() {
        return returned;
    }

    public void setReturned(int returned) {
        this.returned = returned;
    }

    int returned;
    String name_contact;

    public String getPathComplete() {
        return PathComplete;
    }

    public void setPathComplete(String pathComplete) {
        PathComplete = pathComplete;
    }

    String PathComplete;

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }

    public int getID_USER_CONTACT() {
        return ID_USER_CONTACT;
    }

    public void setID_USER_CONTACT(int ID_USER_CONTACT) {
        this.ID_USER_CONTACT = ID_USER_CONTACT;
    }

    public Date getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(Date entry_date) {
        this.entry_date = entry_date;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public int getIsMagHug() {
        return isMagHug;
    }

    public void setIsMagHug(int isMagHug) {
        this.isMagHug = isMagHug;
    }

    public int getTrustRelationship() {
        return TrustRelationship;
    }

    public void setTrustRelationship(int trustRelationship) {
        TrustRelationship = trustRelationship;
    }

    public String getName_contact() {
        return name_contact;
    }

    public void setName_contact(String name_contact) {
        this.name_contact = name_contact;
    }
}
