package entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Usuario on 10/07/2015.
 */
public class ObjectSection {

    private int _id;
    private String _text;
    private boolean isPreference;
    private List<ParamSection> params;

    public ObjectSection(){
        this._id = 0;
        this._text = "";
        this.isPreference = false;
        this.params = new ArrayList<ParamSection>();
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setText(String text){
        this._text = text;
    }

    public String getText(){
        return this._text;
    }

    public void setIsPreference(boolean isPreference){
        this.isPreference = isPreference;
    }

    public boolean getIsPreference(){
        return this.isPreference;
    }

    public void setParams(List<ParamSection> params){
        this.params = params;
    }

    public List<ParamSection> getParams(){
        return this.params;
    }

    public void addParam(ParamSection param){
        this.params.add(param);
    }

    public ParamSection getParam(int id){
        ParamSection param = new ParamSection();

        for (ParamSection ps : this.params){
            if (ps.getId() == id){
                param = ps;
            }
        }

        return param;
    }
}
