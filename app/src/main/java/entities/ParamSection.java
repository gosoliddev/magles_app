package entities;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Usuario on 10/07/2015.
 */
public class ParamSection {

    private int _id;
    private int _idObject;
    private String _text;
    private ParamType _paramType;
    private List<ValueSection> values;
    private View view;

    public ParamSection(){
        this._id = 0;
        this._idObject = 0;
        this._text = "";
        this._paramType = ParamType.BUTTON_SPINNER;
        this.values = new ArrayList<ValueSection>();
        this.view = null;
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setIdObject(int id){
        this._idObject = id;
    }

    public int getIdObject(){
        return this._idObject;
    }

    public void setText(String text){
        this._text = text;
    }

    public String getText(){
        return this._text;
    }

    public void setParamType(ParamType type){
        this._paramType = type;
    }

    public ParamType getParamType(){
        return this._paramType;
    }

    public void setValues(List<ValueSection> values){
        this.values = values;
    }

    public List<ValueSection> getValues(){
        return this.values;
    }

    public void addValue(ValueSection value){
        this.values.add(value);
    }

    public ValueSection getValue(int id){
        ValueSection value = new ValueSection();

        for (ValueSection vs : this.values){
            if (vs.getId() == id){
                value = vs;
            }
        }

        return value;
    }

    public void setView(View view){
        this.view = view;
    }

    public View getView(){
        return this.view;
    }
}



