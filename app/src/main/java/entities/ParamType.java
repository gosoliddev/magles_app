package entities;

/**
 * Created by Usuario on 10/07/2015.
 */
public enum ParamType {

    BUTTON_SPINNER("spinner"),
    CHECKBOX("checkbox");

    private String value;

    private ParamType(String value) {
        this.value = value;
    }

    public String getValue(){ return this.value; }
};