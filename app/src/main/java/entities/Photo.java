package entities;

/**
 * Created by Romano on 27/07/2015.
 */
public class Photo  {
    int ID_PHOTO;
    String Name;
    String PathPhysical;
    String PathComplete;
    int IsDeleted;
    int IsProfile;
    int ID_GALLERY;
    String Nickname;

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }

    int ID_USER;

    public int getFromRresource() {
        return fromRresource;
    }

    public void setFromRresource(int fromRresource) {
        this.fromRresource = fromRresource;
    }

    int fromRresource;
    public int getID_PHOTO() {
        return ID_PHOTO;
    }

    public void setID_PHOTO(int ID_PHOTO) {
        this.ID_PHOTO = ID_PHOTO;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPathPhysical() {
        return PathPhysical;
    }

    public void setPathPhysical(String pathPhysical) {
        PathPhysical = pathPhysical;
    }

    public String getPathComplete() {
        return PathComplete;
    }

    public void setPathComplete(String pathComplete) {
        PathComplete = pathComplete;
    }

    public int getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        IsDeleted = isDeleted;
    }

    public int getIsProfile() {
        return IsProfile;
    }

    public void setIsProfile(int isProfile) {
        IsProfile = isProfile;
    }

    public int getID_GALLERY() {
        return ID_GALLERY;
    }

    public void setID_GALLERY(int ID_GALLERY) {
        this.ID_GALLERY = ID_GALLERY;
    }
    public String getNickname() {
        return Nickname;
    }

    public void setNickname(String nickname) {
        Nickname = nickname;
    }

}

