package entities;

import java.io.Serializable;

/**
 * Created by Usuario on 16/07/2015.
 */
public  class Preference implements Serializable {

    public int getID_PREFERENCE() {
        return ID_PREFERENCE;
    }

    public void setID_PREFERENCE(int ID_PREFERENCE) {
        this.ID_PREFERENCE = ID_PREFERENCE;
    }

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }

    public int getID_OBJECT() {
        return ID_OBJECT;
    }

    public void setID_OBJECT(int ID_OBJECT) {
        this.ID_OBJECT = ID_OBJECT;
    }

    public int getID_PARAM() {
        return ID_PARAM;
    }

    public void setID_PARAM(int ID_PARAM) {
        this.ID_PARAM = ID_PARAM;
    }

    public int getID_VALUE() {
        return ID_VALUE;
    }

    public void setID_VALUE(int ID_VALUE) {
        this.ID_VALUE = ID_VALUE;
    }

    public boolean getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        IsDeleted = isDeleted;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean isInvited) {
        this.IsActive = isInvited;
    }

    private int ID_PREFERENCE;
    private int ID_USER;
    private int ID_OBJECT;
    private int ID_PARAM;
    private int ID_VALUE;
    private boolean IsDeleted;
    private boolean IsActive;

    public Preference() {
        this.ID_PREFERENCE = 0;
        this.ID_USER = 0;
        this.ID_OBJECT = 0;
        this.ID_PARAM = 0;
        this.ID_VALUE = 0;
        this.IsDeleted = false;
        this.IsActive = false;
    }
}
