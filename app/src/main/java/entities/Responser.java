package entities;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Romano on 06/07/2015.
 */
public class Responser {
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public JSONArray getArray() {
        return array;
    }

    public void setArray(JSONArray array) {
        this.array = array;
    }

    public JSONObject getObject() {
        return object;
    }

    public void setObject(JSONObject object) {
        this.object = object;
    }

    private String message;
    private boolean status;
    private JSONArray array;
    private JSONObject object;
}
