package entities;

/**
 * Created by Usuario on 10/07/2015.
 */
public class SpinnerValue {

    private int _id;
    private String _code;
    private String _text;

    public SpinnerValue(){
        this._id = 0;
        this._text = "";
        this._code = "";
    }

    public SpinnerValue(int id, String text){
        this._id = id;
        this._text = text;
    }

    public SpinnerValue(String code, String text){
        this._code = code;
        this._text = text;
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setCode(String code){
        this._code = code;
    }

    public String getCode(){
        return this._code;
    }

    public void setText(String name){
        this._text = name;
    }

    public String getText(){
        return this._text;
    }
}
