package entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.security.Timestamp;
import java.sql.Time;
import java.util.Date;

/**
 * Created by Romano on 06/07/2015.
 */
public  class User implements Serializable {


    private int ID_USER;
    private String Name;
    private String LastName;
    private String Nickname;
    private String CODE_COUNTRY;
    private String ID_CITY;
    private String ID_HOROSCOPE;
    private String Email;
    private String Birthday;
    private int IsInvisible;
    private int IsDeleted;
    private int IsInvited;
    private String Status;
    private String LastOnline;
    private String Phone;
    private String Password;

    private String registrationId;

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getNickname() {
        return Nickname;
    }

    public void setNickname(String nickname) {
        Nickname = nickname;
    }

    public String getCODE_COUNTRY() {
        return CODE_COUNTRY;
    }

    public void setCODE_COUNTRY(String CODE_COUNTRY) {
        this.CODE_COUNTRY = CODE_COUNTRY;
    }

    public String getID_CITY() {
        return ID_CITY;
    }

    public void setID_CITY(String ID_CITY) {
        this.ID_CITY = ID_CITY;
    }

    public String getID_HOROSCOPE() {
        return ID_HOROSCOPE;
    }

    public void setID_HOROSCOPE(String ID_HOROSCOPE) {
        this.ID_HOROSCOPE = ID_HOROSCOPE;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public int getIsInvisible() {
        return IsInvisible;
    }

    public void setIsInvisible(int isInvisible) {
        IsInvisible = isInvisible;
    }

    public int getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        IsDeleted = isDeleted;
    }

    public int getIsInvited() {
        return IsInvited;
    }

    public void setIsInvited(int isInvited) {
        IsInvited = isInvited;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getLastOnline() {
        return LastOnline;
    }

    public void setLastOnline(String lastOnline) {
        LastOnline = lastOnline;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
    public User() {
    }

}
