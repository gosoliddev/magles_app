package entities;

/**
 * Created by Usuario on 10/07/2015.
 */
public class ValueSection {

    private int _id;
    private int _idParam;
    private String _text;
    private boolean _isStatus;

    public ValueSection(){
        this._id = 0;
        this._idParam = 0;
        this._text = "";
        this._isStatus = false;
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setIdParam(int id){
        this._idParam = id;
    }

    public int getIdParam(){
        return this._idParam;
    }

    public void setText(String text){
        this._text = text;
    }

    public String getText(){
        return this._text;
    }

    public void setIsStatus(boolean isStatus){
        this._isStatus = isStatus;
    }

    public boolean getIsStatus(){
        return this._isStatus;
    }
}
