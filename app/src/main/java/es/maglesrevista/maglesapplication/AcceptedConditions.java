package es.maglesrevista.maglesapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;
import org.w3c.dom.Text;

import utils.Preloader;
import utils.Request;
import utils.TextFont;

public class AcceptedConditions extends BaseBarActivity {


    Bundle bundle;

    private WebView webView;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_accepted_conditions);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        bundle = getIntent().getExtras();

        webView = (WebView) findViewById(R.id.webview_conditions);
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        if(bundle.getString("url").contains("maglesmatch.com")){
            webView.loadUrl(bundle.getString("url"));
        } else {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(bundle.getString("url")));
            startActivity(i);
        }
    }

//    @Override
//    protected void onResume() {
//        if(bundle.getString("url").contains("maglesmatch.com")){
//            webView.loadUrl(bundle.getString("url"));
//        } else {
//            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(bundle.getString("url")));
//            startActivity(i);
//        }
//
//        super.onResume();
//    }

    @Override
    protected void onNewIntent(Intent intent) {
        bundle=intent.getExtras();
        webView.loadUrl(bundle.getString("url"));
        super.onNewIntent(intent);
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            Preloader.getInstance(AcceptedConditions.this).dialog().show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            if(url.contains("maglesmatch.com")){
                view.loadUrl(url);
            } else {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            Preloader.getInstance(AcceptedConditions.this).dialog().cancel();
        }
    }



    public void setValue(int progress) {
        this.progress.setProgress(progress);
    }
}
