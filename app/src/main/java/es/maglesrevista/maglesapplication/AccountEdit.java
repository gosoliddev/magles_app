package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.sax.StartElementListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import custom.ErrorAdapter;
import entities.User;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;

public class AccountEdit extends BaseBarActivity  implements Validator.ValidationListener{

    int idUser = 1;
    @NotEmpty(message = "El campo nombre y apellido, es obligatorio")
    private EditText nameLastname;

    @NotEmpty(message = "El campo password, es obligatorio")
    @Password(message = "El password debe contener mínimo 6 caracteres")
    private EditText password;

    @NotEmpty(message = "El campo email, es obligatorio")
    @Email(message = "Debe ingresar un email válido")
    private EditText email;

    private EditText codeInvitation;
    Validator validator;
    Button btn_create;
    int userId=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.userId=BaseHelper.getUserId(getApplicationContext());
        session=new SessionManager(this);
        NUMBER_PROCESS=1;
        CURRENT_PROCESS=0;
        runProcess(this);
        setContentView(R.layout.activity_account_edit);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        this.nameLastname = (EditText) findViewById(R.id.etx_name_lastname);
        this.nameLastname.setFocusableInTouchMode(false);
        this.nameLastname.setFocusable(false);
        this.password = (EditText) findViewById(R.id.etx_password);
        this.email = (EditText) findViewById(R.id.etx_email);
        this.codeInvitation = (EditText) findViewById(R.id.etx_invitation_code);
        this.btn_create=(Button)findViewById(R.id.btn_create);
        validator = new Validator(this);
        validator.setValidationListener(this);
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
        loadUserProfile(this.userId);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    public void eliminar_onclick(View view)
    {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        TextView txtTitle=(TextView)dialog.findViewById(R.id.title_dialog);
        TextView texto=(TextView)dialog.findViewById(R.id.text_dialog);
        texto.setVisibility(View.INVISIBLE);
        texto.setWidth(0);
        texto.setHeight(0);
        TextView txt_ok= (TextView)dialog.findViewById(R.id.txt_si);
        TextView txt_no= (TextView)dialog.findViewById(R.id.txt_no);
        txtTitle.setText(Html.fromHtml("¿Estás segura que quieres eliminar tu cuenta en MagLes Match?"));
        txtTitle.setTextSize(20);
        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Preloader.getInstance(AccountEdit.this).dialog().show();
                RequestParams params = new RequestParams();
                params.put("user_id",  userId);
                Request.post("users/deleteaccount", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, JSONObject object) {
                        try {
                            ArrayList<String> error_list=new ArrayList<String>();
                            final Dialog dialog = new Dialog(AccountEdit.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_info);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            dialog.getWindow().setAttributes(lp);
                            TextView text=(TextView)dialog.findViewById(R.id.text_dialog);
                            text.setText("Podrás volver a activar tu cuenta iniciando sesión con tus datos de siempre.");
                            dialog.show();
                            final Timer t = new Timer();
                            t.schedule(new TimerTask() {
                                public void run() {
                                    dialog.cancel(); // when the task active then close the dialog
                                    closeApp();
                                    t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                                }
                            }, 6000);


                        } catch (Exception ex) {
                            Preloader.getInstance(AccountEdit.this).dialogInfo("Fallo inesperado por favor vuelva intentarlo.");
                            Preloader.getInstance(AccountEdit.this).dialog().hide();
                            closeApp();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Preloader.getInstance(AccountEdit.this).dialog().hide();
                        Preloader.getInstance(AccountEdit.this).dialogInfo("Fallo inesperado por favor vuelva intentarlo.");
                        closeApp();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        Preloader.getInstance(AccountEdit.this).dialogInfo("Fallo inesperado por favor vuelva intentarlo.");
                        closeApp();
                    }
                });

            }
        });
        dialog.show();

    }
    public void closeApp(){
        Intent intent=new Intent(getApplicationContext(),Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        session=new SessionManager(getApplicationContext());
        session.logoutUser();
    }
    public void loadUserProfile(int userId){
        Request.get("users/user/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                User user = new User();
                try {
                    nameLastname.setText(response.getString("Nickname"));
                    email.setText(response.getString("Email"));
                    password.setText(response.getString("Password"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CURRENT_PROCESS++;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                CURRENT_PROCESS++;
            }
        });
    }
    @Override
    public void onValidationSucceeded() {
        Preloader.getInstance(AccountEdit.this).dialog().show();
        RequestParams params = new RequestParams();
        params.put("password", this.password.getText().toString());
        params.put("email", this.email.getText().toString());
        params.put("user_id", this.userId);
        params.put("codeInvitation", this.codeInvitation.getText().toString());
        Request.post("users/changecreateaccount", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, JSONObject object) {
                try {
                    session.setStatus(object.getString("Status"));
                    AccountEdit.this.finish();
                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "Error de modificacion de datos.", Toast.LENGTH_LONG).show();
                    Preloader.getInstance(AccountEdit.this).dialog().hide();
                    AccountEdit.this.finish();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Preloader.getInstance(AccountEdit.this).dialog().hide();
                try {
                    Preloader.getInstance(AccountEdit.this).dialogInfo(errorResponse.getString("error"));
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Preloader.getInstance(AccountEdit.this).dialogInfo("Falló inesperado, no se realizaron los cambios.");
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        ArrayList<String> error_list=new ArrayList<String>();
        int pos=0;
        int birthday_failed=0;
        for (ValidationError error : errors) {
            View view = error.getView();

            String message = error.getCollatedErrorMessage(this);
            error_list.add(message);

        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialgo_error);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        String validations="";
        TextView txtTitle=(TextView)dialog.findViewById(R.id.title_dialog);
        ListView list=(ListView)dialog.findViewById(R.id.error_list);
        txtTitle.setHeight(0);
        ErrorAdapter adapter=new ErrorAdapter(AccountEdit.this,error_list);
        list.setAdapter(adapter);
        dialog.show();

    }
}
