package es.maglesrevista.maglesapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import pushnotifications.Globals;
import utils.BaseHelper;
import utils.DateUtils;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;

/**
 * Created by Romano on 03/08/2015.
 */
public  class BaseBarActivity extends ActionBarActivity{

    int NUMBER_PROCESS=0;
    int CURRENT_PROCESS=0;
    FlyOutContainer root;
    Dialog dialog;
    SessionManager session;
    SwipeRefreshLayout swipeContainer;
    ImageView imghead_profile;
    Dialog preloader_global;
    GoogleCloudMessaging gcm_root;
    public void loadHeader(){
        Bundle bundle=getIntent().getExtras();
        ActionBar actionBar = getSupportActionBar();  //to support lower version too
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.hide();

        imghead_profile= (ImageView)root.findViewById(R.id.icn_profilehead);
        imghead_profile.setImageDrawable(null);
        ViewGroup.LayoutParams params1=imghead_profile.getLayoutParams();
        Bitmap bmp= ImageUtils.loadFromCacheFile();
        if(bmp!=null) {
            imghead_profile.setImageBitmap(bmp);
        }
        else
            imghead_profile.setImageResource(R.mipmap.photo_default);
        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(10, 1, 1, 1);
        imghead_profile.setLayoutParams(params);
        Runtime.getRuntime().gc();
        System.gc();
        configureMenu();
        loadMessagesNotReaded();
        reviewDateExpiredAccount();

    }

    public void loadMessagesNotReaded(){
        RequestParams params=new RequestParams();
        params.put("nickname",BaseHelper.getNickname(getApplicationContext()));
        Request.get("chats/messagesnotreaded/", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ImageView img = (ImageView) root.findViewById(R.id.icn_4);
                if (response.length() > 0) {
                    img.setImageResource(R.mipmap.icn_message_dot);
                } else {
                    img.setImageResource(R.mipmap.icn_message);

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }
        });
    }
    public void reviewDateExpiredAccount(){
        int userId=BaseHelper.getUserId(getApplicationContext());
        Request.get("users/userProfile/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    if(response.getString("Status").equals("P")) {
                            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date dateTo = formatter1.parse(response.getString("dateTo"));
                            if (DateUtils.compareDates(Calendar.getInstance().getTime(), dateTo) > 0) {
                                final Dialog dialog1 = new Dialog(BaseBarActivity.this);
                                dialog1.setCancelable(false);
                                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog1.setContentView(R.layout.dialog_premium_success);
                                TextView texto1 = (TextView) dialog1.findViewById(R.id.text_dialog1);
                                texto1.setTypeface(TextFont.getInstance().getFont(BaseBarActivity.this, TextFont.PATH_FONT_BOLD));
                                texto1.setTextSize(18);
                                TextView texto2 = (TextView) dialog1.findViewById(R.id.text_dialog2);
                                texto2.setVisibility(View.GONE);
                                String text1 = "", text2 = "";
                                text1 = getString(R.string.premium_expiration);
                                texto1.setText(text1);
                                dialog1.show();
                                final Timer t = new Timer();
                                t.schedule(new TimerTask() {
                                    public void run() {
                                        try {
                                            dialog1.dismiss();
                                            t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                                            Intent intent = new Intent(BaseBarActivity.this, splashscreen.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        } catch (Exception e) {
                                        }
                                    }
                                }, 4000);
                            }

                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }
        });
    }

    public void configureMenu(){
        try {
            SessionManager sesion1 = new SessionManager(getApplicationContext());
            String status = BaseHelper.getUserStatus(getApplicationContext());
            ImageView icn_2 = (ImageView) root.findViewById(R.id.icn_2);
            ImageView icn_3 = (ImageView) root.findViewById(R.id.icn_3);
            ImageView icn_4 = (ImageView) root.findViewById(R.id.icn_4);
            Button menu_option_1=(Button)root.findViewById(R.id.menu_option_1);
            if(status.equals("B"))
            {
                if(icn_2!=null) {
                    BaseHelper.applyAlpha(icn_2, 0.5f);
                }
                if(icn_3!=null) {
                    BaseHelper.applyAlpha(icn_3, 0.5f);
                }
                if(icn_4!=null) {
                    BaseHelper.applyAlpha(icn_4, 0.5f);
                }
            }
            else
            {
                if(icn_2!=null) {
                    BaseHelper.applyAlpha(icn_2, 1.0f);
                }
                if(icn_3!=null) {
                    BaseHelper.applyAlpha(icn_3, 1.0f);
                }
                if(icn_4!=null) {
                    BaseHelper.applyAlpha(icn_4, 1.0f);
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

    }
    public void home_onclick(View view){
        Intent intent  =new Intent(this,home.class);
        startActivity(intent);
    }
    public void optionchat_onclick(View view){

        Intent intent=new Intent(this,menuchat.class);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optionnoticies_onclick(View view){
        Intent intent=new Intent(this,notices.class);
        intent.putExtra("url","http://www.maglesmatch.com/app");
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optionweb_onclick(View view){

        Intent intent;
        Uri uri = Uri.parse("http://www.maglesrevista.es/");
        intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optiontwitter_onclick(View view){
        Intent intent;
        Uri uri = Uri.parse("https://twitter.com/maglesrevista");
        intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optionfacebook_onclick(View view){
        Intent intent;
        Uri uri = Uri.parse("https://www.facebook.com/magles.revista");
        intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        root.toggleMenu();
    }

    public void optionfaq_onclick(View view){
        Intent intent=new Intent(this,notices.class);
        intent.putExtra("url", "http://www.maglesmatch.com/app/page.php?id_page=990");
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optioncontact_onclick(View view){

        Intent intent=new Intent(this,notices.class);
        intent.putExtra("url", "http://www.maglesmatch.com/app/page.php?id_page=403");
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        root.toggleMenu();
    }
    public void optionlogout_onclick(View view){
        unregister();
        root.toggleMenu();
    }
    private void unregister()
    {
        preloader_global=Preloader.getInstance(BaseBarActivity.this).build();
        preloader_global.show();
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (gcm_root == null)
                    {
                        gcm_root = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    gcm_root.unregister();
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg)
            {
                session=new SessionManager(getApplicationContext());
                session.setRegistrationId("");
                sendRegistrationIdToServer("");

            }
        }.execute();
    }
    public void sendRegistrationIdToServer(String registrationId){
        RequestParams params=new RequestParams();
        params.put("user_id",BaseHelper.getUserId(getApplicationContext()));
        params.put("registrationId", registrationId);
        Request.post("users/saveregistration", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    BaseHelper.logout(getApplicationContext());
                    Intent intent  =new Intent(getApplicationContext(),Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    try {
                        File f=new File(ImageUtils.getCacheFilename());
                        f.delete();
                    }
                    catch(Exception e)
                    {

                    }

                } catch (Exception ex) {
                }
                preloader_global.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader_global.dismiss();
                Preloader.getInstance(BaseBarActivity.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader_global.dismiss();
                Preloader.getInstance(BaseBarActivity.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }
        });
    }
    public void topmenuread_onclick(View view){

        Intent intent=new Intent(this,notices.class);
        intent.putExtra("url", "http://www.maglesmatch.com/app/noticias.php");
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void topmenuchat_onclick(View view){

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(this,ListChat.class);
                intent.putExtra("WITH_PHOTO", "");
                intent.putExtra("ID_USER", BaseHelper.getUserId(this));
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(this,ListChat.class);
                intent.putExtra("WITH_PHOTO", "");
                intent.putExtra("ID_USER", BaseHelper.getUserId(this));
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void topmenufavorites_onclick(View view){
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(this,Favorite.class);
                intent.putExtra("ID_USER", BaseHelper.getUserId(this) );
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(this,Favorite.class);
                intent.putExtra("ID_USER", BaseHelper.getUserId(this) );
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }

    }
    public void topmenucomversations_onclick(View view){

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent = new Intent (this, ConversationActivity.class);
                intent.putExtra("name", BaseHelper.getNickname(this));
                startActivity(intent);
                break;
            }
            case "I":{
                //Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                Intent intent = new Intent (this, ConversationActivity.class);
                intent.putExtra("name", BaseHelper.getNickname(this));
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void topmenuyourprofile_onclick(View view) {
        Intent intent =new Intent(this,YourProfile.class);
        startActivity(intent);
    }
    public void optionfindmore_onclick(View view){
        Intent intent=new Intent(this,notices.class);
        intent.putExtra("url", "http://www.maglesmatch.com/app/categorias-descubre-mas.php");
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        root.toggleMenu();
    }
    public void toggleMenu(View v){
        this.root.toggleMenu();
    }

    @Override
    protected void onDestroy() {
        try {

            super.onDestroy();
            if (Preloader.getInstance(BaseBarActivity.this).dialog() != null) {
                if (Preloader.getInstance(BaseBarActivity.this).dialog().isShowing()) {
                    Preloader.getInstance(BaseBarActivity.this).dialog().dismiss();
                }
            }
            super.onDestroy();
            BaseHelper.onDestroy(this);
        }
        catch (Exception e)
        {
        }
    }
    public void runProcess(final Activity activity){
        if(isConected()) {
            Preloader.getInstance(activity).dialog().show();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    long endTime = System.currentTimeMillis() + 3000;
                    while (!Thread.currentThread().isInterrupted() ) {
                        try {
                            if (CURRENT_PROCESS == NUMBER_PROCESS || System.currentTimeMillis() >=endTime) {
                            Thread.currentThread().interrupt();

                                Preloader.getInstance(activity).dialog().cancel();
                            }
                        }
                        catch (Exception e)
                        {
                            Preloader.getInstance(activity).dialog().cancel();
                        }
                    }
                }
            };
            new Thread(runnable).start();
        }
        else
        {
           // createNotification();
        }
    }

    public boolean isConected(){
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }




}
