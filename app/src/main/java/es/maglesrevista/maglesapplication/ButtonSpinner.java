package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.AvoidXfermode;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.GpsStatus;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import entities.SpinnerValue;
import utils.TextFont;

/**
 * Created by Usuario on 09/07/2015.
 */
public class ButtonSpinner extends Button {

    private Spinner spinner;
    private SpinnerAdapter adapter;
    private SpinnerValue[] values;
    private Context context;
    private String titleButton;

    public ButtonSpinner(Context context) {
        super(context);
        this.context = context;
    }

    public ButtonSpinner(Context context, int buttonId, int resButtonText) {
        super(context);

        this.context = context;
        this.spinner = new Spinner(context);
        this.setId(buttonId);
        this.setText(resButtonText);
        this.titleButton = this.getText().toString();
    }

    public ButtonSpinner(Context context, int buttonId, String buttonText) {
        super(context);

        this.context = context;
        this.spinner = new Spinner(context);
        this.setId(buttonId);
        this.setText(buttonText);
        this.titleButton = buttonText;
    }

    public ButtonSpinner(Context context, SpinnerValue[] values, int buttonId, String buttonText) {
        super(context);

        this.context = context;
        this.spinner = new Spinner(context);
        this.setSpinnerValues(values);
        this.setId(buttonId);
        this.setText(buttonText);
        this.titleButton = buttonText;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setSpinnerValues(SpinnerValue[] values){
        this.spinner.setVisibility(View.INVISIBLE);
        this.spinner.setPadding(20, 0, 0, 0);
        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (getSelectedText().equals("")) {
                    setText(titleButton);
                } else {
                    setText(getSelectedText());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        this.adapter = new SpinnerAdapter(context, R.layout.abc_screen_simple, values);
        //this.spinner.setPopupBackgroundResource(R.color.white);
        this.values = values;
        this.spinner.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        //this.spinner.setSelection(0);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.performClick();
            }
        });
    }

    public int getSelectedId(){
        return this.getSelectedSpinnerValue().getId();
    }

    public String getSelectedCode(){
        return this.getSelectedSpinnerValue().getCode();
    }

    public String getSelectedText(){
        return this.getSelectedSpinnerValue().getText();
    }

    public void setTitleButton(String title) {
        this.titleButton = title;
    }

    public String getTitleButton() {
        return this.titleButton;
    }

    public SpinnerValue getSelectedSpinnerValue(){
        int selected = (int) this.spinner.getSelectedItemId();
        return this.values[selected];
    }

    public Spinner getControl(){
        return this.spinner;
    }

    public LinearLayout getInLinearLayout(){
        this.spinner.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 20));
        LinearLayout ln = new LinearLayout(context);
        ln.setOrientation(LinearLayout.VERTICAL);
        ln.setLayoutParams(new TableRow.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
        ln.addView(this);
        ln.addView(this.spinner);
        return ln;
    }

    public LinearLayout getInLinearLayoutSearchFilter(){
        this.spinner.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        LinearLayout ln = new LinearLayout(this.context);
        ln.setOrientation(LinearLayout.VERTICAL);
        ln.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        ln.addView(this);
        ln.addView(this.spinner);

        return ln;
    }

    public void setStyleCreateAccount(boolean isTwoColumns){
        if (isTwoColumns)
        {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            params.setMargins(10, 0, 10, 0);
            this.setLayoutParams(params);
        }
        else { this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)); }

        this.setBackgroundResource(R.color.color_btn_magles_dropdown);
        this.setPadding(0, 0, 0, 5);
        this.setTextColor(getContext().getResources().getColor(R.color.color_btn_magles_dropdown_text));
        this.setTextSize(16f);
        this.setTypeface(TextFont.getInstance().getFont(this.context));
        this.setSingleLine(true);
        this.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.mipmap.icon_arrow_down);
        this.setCompoundDrawablePadding(3);
    }

    public void setStyleSearchFilter(boolean isTwoColumns, boolean isTogether) {
        if (isTwoColumns && isTogether) {
            this.setLayoutParams(new LinearLayout.LayoutParams(250, 105));
        }
        else if (isTwoColumns && !isTogether) {
            this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 105));
        }
        else if (!isTwoColumns && !isTogether) {
            this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 60));
        }

        this.setBackgroundResource(R.color.color_white_transparent_50);
        this.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_arrow_down_32, 0);
        this.setSingleLine(true);
        this.setPadding(0, 0, 0, 0);
        this.setTextColor(getContext().getResources().getColor(R.color.color_btn_magles_dropdown_text));
        this.setTextSize(16f);
    }

    public void setSelection(int idValue) {
        for (int position = 0; position < this.values.length; position++) {
            if (this.values[position].getId() == idValue) {
                this.spinner.setSelection(position);
                break;
            }
        }
    }

    public void setSelection(String codeValue) {
        for (int position = 0; position < this.values.length; position++) {
            if (this.values[position].getCode().equals(codeValue)) {
                this.spinner.setSelection(position);
                break;
            }
        }
    }
}
