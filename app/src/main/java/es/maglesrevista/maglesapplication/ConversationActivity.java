package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.content.Intent;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import chatsocket.MessagesListAdapter;
import custom.ObservableScrollView;
import custom.WidgetComversations;
import entities.User;
import other.Message;
import other.MessageChatUser;
import utils.BaseHelper;
import utils.DateUtils;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;

public class ConversationActivity extends BaseBarActivity implements ScrollViewListener {
    ArrayList<User> users_list_history;
    private ArrayList<MessageChatUser> listMessages;
    ListView listview;
    MessageListAdapterHistoryActivity adapter;
    WidgetComversations widgetComversations;
    ObservableScrollView scrollOnDemand;
    String usuario="";
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=10;
    int TOTAL_ROWS=0;
    Boolean isBussy=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=0;
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_conversation, null);
        this.setContentView(root);

        session = new SessionManager(getApplicationContext());
        widgetComversations=(WidgetComversations)root.findViewById(R.id.list_comversations);
        widgetComversations.clear();
        listMessages = new ArrayList<MessageChatUser>();
        usuario = getIntent().getStringExtra("name");
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        swipeContainer=(SwipeRefreshLayout)root.findViewById(R.id.swipeContainer);
        reloadActivity();
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isBussy=true;
                NUMBER_PROCESS = 0;
                CURRENT_PROCESS = 0;
                CURRENT_PAGE = 0;
                //runProcess(ConversationActivity.this);
                widgetComversations.clear();
                listMessages.clear();

                getMassageChatComplete(usuario);
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
    @Override
    protected void onResume() {
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=0;
        CURRENT_PAGE=0;
        loadHeader();
        load();
        super.onResume();
    }

    public void load(){
        listMessages.clear();
        widgetComversations.clear();
        getMassageChatComplete(usuario);
    }
    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);

        // if diff is zero, then the bottom has been reached
        if( diff == 0)
        {
            if(isBussy==false) {
                isBussy=true;
                getMassageChatComplete(usuario);
            }
        }
    }
    public void getMassageChatComplete(String nickname) {
        final Dialog preloader1=Preloader.getInstance(ConversationActivity.this).build();
        preloader1.show();
        RequestParams params = new RequestParams();
        params.put("nickname", nickname);
        params.put("page_size", PAGE_SIZE);
        params.put("page",CURRENT_PAGE);
        Request.post("Chats/messagechatwait", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Preloader.getInstance(ConversationActivity.this).dialog().dismiss();
                isBussy=false;
                preloader1.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                listMessages.clear();
                    if (response.length() > 0) {
                        for (int i = 0; i < response.length(); i++) {

                            try {
                                JSONObject obj = response.getJSONObject(i);
                                MessageChatUser message = new MessageChatUser();

                                String transmiter = obj.getString("transmitter");
                                String receiver = obj.getString("receiver");
                                message.setFromName(transmiter);
                                message.setReceiver(receiver);
                                message.setMessage(obj.getString("text"));
                                if(transmiter.equals(BaseHelper.getNickname(getApplicationContext()))) {
                                    message.setID_USER(obj.getInt("ID_USER"));
                                }
                                else
                                    message.setID_USER(obj.getInt("ID_USER1"));

                                message.setType_message(obj.getString("type_message"));
                                SimpleDateFormat formatter_origin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date fecha= DateUtils.convertFromOneTimeZoneToOhter(formatter_origin.parse(obj.getString("date")), "Europe/Madrid", TimeZone.getDefault().getID());
                                message.setFromDate(fecha);

                                String readed = obj.getString("readed");
                                boolean visto = readed.equals("1") ? true : false;
                                message.setIsView(visto);
                                TOTAL_ROWS = obj.getInt("total_rows");
                                TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                                listMessages.add(message);
                            }catch(Exception ex){

                            }
                        }
                        widgetComversations.setDataSource(listMessages);
                        widgetComversations.setColumns(1);
                        widgetComversations.dataBind();
                        CURRENT_PAGE++;
                    }else if(CURRENT_PAGE==0 && response.length()<=0)
                    {
                        Preloader.getInstance(ConversationActivity.this).dialogInfo("No tienes ninguna conversación iniciada. ¡Busca otras usuarias en Buscar o Chat y contáctalas!");
                    }


                CURRENT_PROCESS++;
                isBussy=false;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listMessages.clear();
                CURRENT_PROCESS++;
                isBussy=false;
                preloader1.dismiss();
                try {
                    if(errorResponse.getJSONObject(0).getInt("code_error")==-2) {
                        Preloader.getInstance(ConversationActivity.this).dialogInfo("No tienes ninguna conversación iniciada. ¡Busca otras usuarias en Buscar o Chat y contáctalas!");

                    }
                }
                catch (Exception ex){}
            }
        });

    }



}
