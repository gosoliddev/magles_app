package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.koushikdutta.async.http.socketio.StringCallback;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import custom.ErrorAdapter;
import pushnotifications.Globals;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;


public class CreateAccount extends BaseBarActivity implements Validator.ValidationListener {

    GoogleCloudMessaging gcm;
    int idUser = 1;
    String regid;
    @Pattern(regex = "^[A-Za-z][A-Za-z0-9]{5,15}$" , message = "Utiliza sólo letras y números para tu nickname")
    @NotEmpty(message = "El campo nickname, es obligatorio")
    @Length(max = 15,min = 6,message = "El nickname debe contener entre 6 y 15 caracteres")
    private EditText nickname;

    @NotEmpty(message = "El campo password, es obligatorio")
    @Password(message = "El password debe contener mínimo 6 caracteres")
    private EditText password;

    @NotEmpty(message = "El campo email, es obligatorio")
    @Email(message = "Debe ingresar un email válido")
    private EditText email;

    private EditText codeInvitation;

    @Checked(message = "Debes aceptar las Condiciones para crear tu cuenta")
    private CheckBox acceptCondition;

    Validator validator;
    Button btn_create;
    Boolean statusNickname=true;
    Dialog preloader1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        this.nickname = (EditText) findViewById(R.id.etx_nickname);

        this.password = (EditText) findViewById(R.id.etx_password);
        this.email = (EditText) findViewById(R.id.etx_email);
        this.codeInvitation = (EditText) findViewById(R.id.etx_invitation_code);
        this.acceptCondition = (CheckBox) findViewById(R.id.chb_accept_condition);
        this.btn_create=(Button)findViewById(R.id.btn_create);
        validator = new Validator(this);
        validator.setValidationListener(this);
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);

    }

    public void onclick_WhoIs(View v){
        Intent intent=new Intent(getApplicationContext(),what_is_it.class);
        startActivity(intent);
    }
    public void acceptedConditions_onclick(View view){
        Intent intent=new Intent(getApplicationContext(),AcceptedConditions.class);
        intent.putExtra("url","http://www.maglesmatch.com/app/page.php?id_page=1328");
        startActivity(intent);
    }
    public void login_onclick(View v) {
        Intent intent=new Intent(this,Login.class);
        startActivity(intent) ;
    }

    public void onclick_Register(View v){
        boolean valid=this.validationFields();
        if(valid) {

        }
    }
    public boolean  validationFields(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        String validations="";
        TextView txtTitle=(TextView)dialog.findViewById(R.id.title_dialog);
        TextView txt_ok= (TextView)dialog.findViewById(R.id.txt_ok);
        txtTitle.setText("Debes rellenar todos los campos del formulario.");
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(!validations.equals("")) {
            dialog.show();
            return false;
        }
        else
            return true;

    }

    @Override
    public void onValidationSucceeded() {

            Preloader.getInstance(CreateAccount.this).dialog().show();
            RequestParams params = new RequestParams();
            params.put("nickname", this.nickname.getText().toString());
            params.put("password", this.password.getText().toString());
            params.put("email", this.email.getText().toString());
            params.put("codeInvitation", this.codeInvitation.getText().toString());
            params.put("acceptCondition", this.acceptCondition.isChecked());
            Request.post("users/userCreateAccount", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, JSONObject object) {
                    try {
                        idUser=object.getInt("ID_USER");
                        registerInBackground();
                        Preloader.getInstance(CreateAccount.this).dialog().cancel();
                    } catch (Exception ex) {
                        Preloader.getInstance(CreateAccount.this).dialogInfo("Fallo inesperado por favor vuelva intentarlo.");
                        Preloader.getInstance(CreateAccount.this).dialog().cancel();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Preloader.getInstance(CreateAccount.this).dialog().cancel();
                    try {
                        statusNickname = false;
                        Preloader.getInstance(CreateAccount.this).dialogInfo(errorResponse.getString("error"));
                    }
                    catch (Exception e)
                    {
                        statusNickname = false;
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Preloader.getInstance(CreateAccount.this).dialog().cancel();
                    Preloader.getInstance(CreateAccount.this).dialogInfo("Fallo inesperado por favor vuelva intentarlo.");
                }
            });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
//        String [] error_list=new String[errors.size()];
        ArrayList<String> error_list=new ArrayList<String>();
        int pos=0;
        int birthday_failed=0;

        for (ValidationError error : errors) {
            View view = error.getView();
            try {
                EditText edit = (EditText) view;
                String message = error.getCollatedErrorMessage(this);
                if (!edit.getHint().toString().equals("Día") && !edit.getHint().toString().equals("Mes") && !edit.getHint().toString().equals("Año")) {
                    error_list.add(message);
                } else {
                    birthday_failed++;
                }
            }catch (Exception ex)
            {
                CheckBox edit = (CheckBox) view;
                String message = error.getCollatedErrorMessage(this);
                error_list.add(message);
            }

        }
        if(birthday_failed>0)
        {
            String message="La fecha de cumpleaños es incorrecta";
            error_list.add(message);
        }

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialgo_error);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        String validations="";
        TextView txtTitle=(TextView)dialog.findViewById(R.id.title_dialog);
        ListView list=(ListView)dialog.findViewById(R.id.error_list);
        txtTitle.setHeight(0);
        ErrorAdapter adapter=new ErrorAdapter(CreateAccount.this,error_list);
        list.setAdapter(adapter);
        dialog.show();

    }
    private void registerInBackground()
    {
        preloader1=Preloader.getInstance(CreateAccount.this).build();
        preloader1.show();
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (gcm == null)
                    {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(Globals.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg)
            {
                preloader1.dismiss();
                sendRegistrationIdToServer(regid);
            }
        }.execute(null, null, null);
    }
    public void sendRegistrationIdToServer(String registrationId){
        preloader1=Preloader.getInstance(CreateAccount.this).build();
        preloader1.show();
        RequestParams params=new RequestParams();
        params.put("user_id",idUser);
        params.put("registrationId", registrationId);
        Request.post("users/saveregistration", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    session=new SessionManager(getApplicationContext());
                    session.setRegistrationId(response.getString("registrationId"));
                    Intent intent = new Intent(CreateAccount.this, CreateAccountProfile.class);
                    intent.putExtra("new_user_id", idUser);
                    intent.putExtra("password", password.getText().toString());
                    intent.putExtra("nickname", nickname.getText().toString());
                    intent.putExtra("view", "create");
                    startActivity(intent);

                } catch (Exception ex) {
                }
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader1.dismiss();
                Preloader.getInstance(CreateAccount.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader1.dismiss();
                Preloader.getInstance(CreateAccount.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }
        });
    }
}
