package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.ObservableScrollView;
import entities.Photo;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import custom.WidgetFavorites;
import utils.TextFont;

public class Favorite extends BaseBarActivity implements ScrollViewListener {

    private LinearLayout result_list;
    final float NUM_COLUMNS = 3;
    int idUser = 0;
    WidgetFavorites favorites_list;
    ObservableScrollView scrollOnDemand;
    SwipeRefreshLayout swipeContainer;
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=20;
    int TOTAL_ROWS=0;
    Boolean isBusy =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=1;
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_favorite, null);
        this.setContentView(root);

        session=new SessionManager(getApplicationContext());
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        swipeContainer=(SwipeRefreshLayout)root.findViewById(R.id.swipeContainer);
        reloadActivity();
        favorites_list=(WidgetFavorites)root.findViewById(R.id.favorites_list);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }

    @Override
    protected void onResume() {
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=1;
        CURRENT_PAGE=0;
        isBusy =false;
        favorites_list.clear();
        loadHeader();
        getListFavorties();
        super.onResume();
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);

        // if diff is zero, then the bottom has been reached
        if( diff == 0)
        {
            if(isBusy ==false) {
                isBusy =true;
                NUMBER_PROCESS=1;
                CURRENT_PROCESS=0;
                getListFavorties();
            }
        }
    }
    public void getListFavorties() {
        final Dialog preloader1=Preloader.getInstance(Favorite.this).build();
        preloader1.show();
        RequestParams params = new RequestParams();
        String id_user = getIntent().getStringExtra("ID_USER");
        params.put("user_id", BaseHelper.getUserId(getApplicationContext()));
        params.put("page_size",PAGE_SIZE);
        params.put("page", CURRENT_PAGE);
        Request.post("RelationShips/findAllFavorites", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                ArrayList<Photo> photos = new ArrayList<Photo>();
                for (int index = 0; index < response.length(); index++) {
                    try {
                        JSONObject item = response.getJSONObject(index);
                        Photo photo1 = new Photo();
                        String path = item.getString("PathComplete") != "null" ? item.getString("PathComplete") : "";
                        int photoId = item.getString("ID_PHOTO") != "null" ? Integer.valueOf(item.getInt("ID_PHOTO")) : 0;
                        photo1.setID_PHOTO(photoId);
                        photo1.setPathComplete(path);
                        photo1.setName(item.getString("Nickname"));
                        photo1.setID_USER(item.getInt("ID_USER"));
                        TOTAL_ROWS = item.getInt("total_rows");
                        TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                        photos.add(photo1);
                    } catch (Exception e) {

                    }
                }
                favorites_list.setColumns(2);
                favorites_list.setDataSource(photos);
                favorites_list.dataBind();
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy =false;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy =false;
                preloader1.dismiss();
            }
        });
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                NUMBER_PROCESS = 1;
                CURRENT_PROCESS = 0;
                CURRENT_PAGE = 0;
                favorites_list.clear();
                getListFavorties();
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
}
