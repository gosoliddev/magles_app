package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.ObservableScrollView;
import custom.UploadGallery;
import custom.WidgetGallery;
import entities.Photo;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.TextFont;


public class Gallery extends BaseBarActivity implements ScrollViewListener , UploadGallery.IUploadGallery{

    int idUser=0;
    WidgetGallery widget_gallery;
    ObservableScrollView scrollOnDemand;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=1;
        this.idUser= BaseHelper.getUserId(getApplicationContext());
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_gallery, null);
        this.setContentView(root);

        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        widget_gallery=(WidgetGallery)root.findViewById(R.id.list_gallery);
        this.idUser= BaseHelper.getUserId(getApplicationContext());
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }

    @Override
    protected void onResume() {
        loadHeader();
        loadPhotoProfile();
        widget_gallery.clear();
        loadGallery();
        super.onResume();
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached

        if( diff == 0)
        {


        }
    }
    public void loadGallery(){
        final Dialog preloader1=Preloader.getInstance(Gallery.this).build();
        preloader1.show();
        RequestParams params=new RequestParams();
        params.put("ID_USER",this.idUser);
        Request.get("Galleries/findAllPhotos", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<Photo> photo_list = new ArrayList<Photo>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        Photo photo = new Photo();
                        photo.setID_USER(obj.getInt("ID_USER"));
                        photo.setID_PHOTO(obj.getInt("ID_PHOTO"));
                        photo.setPathComplete(obj.getString("PathComplete"));
                        photo_list.add(photo);
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }
                }
                widget_gallery.setDataSource(photo_list);
                widget_gallery.setColumns(3);
                widget_gallery.dataBind();
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void uploadToGallery_onclick(View view){
        UploadGallery dialog = new UploadGallery();
        dialog.setUserId(BaseHelper.getUserId(getApplicationContext()));
        dialog.show(getFragmentManager(), "uploader");
    }
    public void  loadPhotoProfile(){
        Bitmap bmp= ImageUtils.loadFromCacheFile();
        if(bmp!=null) {

            imghead_profile.setImageBitmap(bmp);
        }
        else
            imghead_profile.setImageResource(R.mipmap.photo_default);
    }
    @Override
    public void updateResult() {
        loadPhotoProfile();
        widget_gallery.clear();
        loadGallery();

    }
}
