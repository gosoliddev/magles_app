package es.maglesrevista.maglesapplication;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.view.View;

import utils.BaseHelper;
import utils.Preloader;
import utils.SessionManager;
import utils.TextFont;

public class GettingPremium extends BaseBarActivity {

    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session=new SessionManager(getApplicationContext());
        setContentView(R.layout.activity_getting_premium);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);

    }
    public void paymentsPaypal_onclick(View view){
        switch (BaseHelper.getUserStatus(getApplicationContext()))
        {
            case "B":{
                Intent intent=new Intent(this,InvitedPremium.class);
                startActivity(intent);
                break;
            }
            case "I":{

                Intent intent=new Intent(this,OffersPayments.class);
                startActivity(intent);
                break;
            }
            case "P":{
                Intent intent=new Intent(this,RenewPremium.class);
                startActivity(intent);
                break;
            }
        }

    }
    public void conditions_onclick(View view){
        Intent intent=new Intent(getApplicationContext(),AcceptedConditions.class);
        intent.putExtra("url", "http://www.maglesmatch.com/app/page.php?id_page=1329");
        startActivity(intent);
    }


}
