package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.okhttp.internal.Util;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.FileNameMap;
import java.text.SimpleDateFormat;
import java.util.Date;

import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;

public class Invitations extends BaseBarActivity {
    TextView text1;
    Button btn_invitar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session=new SessionManager(this);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_invitations, null);
        this.setContentView(root);
        text1=(TextView)findViewById(R.id.inv_text1);
        btn_invitar=(Button)findViewById(R.id.btn_invitar);
        TextView text4=(TextView)findViewById(R.id.inv_text4);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        text1.setText(Html.fromHtml("Tienes <strong><font color='#bf284a'>"+getString(R.string.max_invitation) +"</font></strong> invitaciones"));

        SpannableString contentText = new SpannableString("Si ya tienes Premium, ahora y por tiempo limitado, te añadimos <strong>1 semana de PREMIUM gratis <br />+ 3 nuevas invitaciones</strong> por cada invitación que usen tus amigas.");
        text4.setText(Html.fromHtml(contentText.toString()));


    }
    @Override
    public void onResume(){
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=0;
        loadHeader();
        loadUserProfile(BaseHelper.getUserId(getApplicationContext()),true);
        super.onResume();
    }
    public void invitartion_onclick(View view){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invitation);
        final EditText input_email=(EditText)dialog.findViewById(R.id.input_email);
        TextView btn_whatsapp=(TextView)dialog.findViewById(R.id.btn_whatsapp);
        TextView txt_ok= (TextView)dialog.findViewById(R.id.txt_si);
        btn_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createInvitation("", BaseHelper.getUserId(getApplicationContext()));
                dialog.cancel();
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!input_email.getText().toString().equals(""))
                    sendInvitation(input_email.getText().toString(), BaseHelper.getUserId(getApplicationContext()));
                else {
                    Preloader.getInstance(Invitations.this).dialogInfo("Debe ingresar un mail valido");
                }
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void sendInvitation(final String email,int userId) {
        final Dialog preloader3=Preloader.getInstance(Invitations.this).build();
        preloader3.show();
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("user_id", userId);
        Request.post("Invitations/invitar", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                preloader3.dismiss();
                loadUserProfile(BaseHelper.getUserId(getApplicationContext()), false);
                Preloader.getInstance(Invitations.this).dialogInfo("Hemos enviado tu Invitación al email: " + email);



            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    if(errorResponse.getInt("code_error")==-1)
                        Preloader.getInstance(Invitations.this).dialogInfo("No tiene invitaciones que compartir!");
                    else
                        Preloader.getInstance(Invitations.this).dialogInfo("Fallo de envio de código al email : " + email + "!");
                }
                catch (Exception e)
                {
                    Preloader.getInstance(Invitations.this).dialogInfo("Fallo de envio de código al email : " + email + "!");
                }
                preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader3.dismiss();
                Preloader.getInstance(Invitations.this).dialogInfo("Ha habido un error en la generación del Código de Invitación");

            }
        });
    }
    private void createInvitation(final String number,int userId) {
        final Dialog preloader2=Preloader.getInstance(Invitations.this).build();
        preloader2.show();
        RequestParams params = new RequestParams();
        params.put("user_id", userId);
        Request.post("Invitations/create", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    String code =response.getString("InvitationCode");
                    boolean isWhatsappInstalled = isInstalled("com.whatsapp");

                    if (isWhatsappInstalled) {
                        Uri imageUri = Uri.parse("android.resource://es.maglesrevista.maglesapplication/"+ R.mipmap.logo);
                        Uri uri = Uri.parse("smsto:" + number);
                        Intent shareIntent = new Intent(Intent.ACTION_SEND,uri);
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.setType("image/jpeg");
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        //Target whatsapp:
                        shareIntent.setPackage("com.whatsapp");
                        //Add text and then Image URI
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "La usuaria " + BaseHelper.getNickname(getApplicationContext()) + " te ha enviado un código de invitación para MagLes Match. Tu Código de Invitación es: " + code + ". Busca la App en Play Store");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);

                        startActivity(shareIntent);
                    } else {
                        Preloader.getInstance(Invitations.this).dialogInfo("WhatsApp no esta instalado en este dispositivo");
                        Uri uri = Uri.parse("market://details?id=com.whatsapp");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(goToMarket);
                    }
                }
                catch (Exception e)
                {
                    Preloader.getInstance(Invitations.this).dialogInfo("Fallo inesperado, código de invitación no revibido");
                }
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader2.dismiss();
                try {
                    if(errorResponse.getInt("code_error")==-1)
                        Preloader.getInstance(Invitations.this).dialogInfo("No tiene invitaciones para compartir!");
                    else
                        Preloader.getInstance(Invitations.this).dialogInfo("Fallo de envio de código !");
                }
                catch (Exception e)
                {
                    Preloader.getInstance(Invitations.this).dialogInfo("Ha habido un error en la generación del Código de Invitación");
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader2.dismiss();
                Preloader.getInstance(Invitations.this).dialogInfo("Ha habido un error en la generación del Código de Invitación");
            }
        });
    }
    private boolean isInstalled(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    public void loadUserProfile(int userId,final boolean showloader){
        final Dialog preloader1=Preloader.getInstance(Invitations.this).build();
        if(showloader)
            preloader1.show();
        Request.get("users/userProfile/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    String visible = response.getString("IsInvisible");
                    String Stutus = response.getString("Status");
                    String number_invitations = String.valueOf(response.getInt("Invitations"));

                    if (visible.equals("0")) {
                        session.setVisibility(true);
                        session.setStatus(Stutus);
                    } else {
                        session.setVisibility(false);
                    }
                    text1.setText(Html.fromHtml("Tienes <strong><font color='#bf284a'>" + number_invitations + "</font></strong> invitaciones"));
                    if(response.getInt("Invitations")<=0)
                    {
                        btn_invitar.setVisibility(View.GONE);
                        Preloader.getInstance(Invitations.this).dialogInfo("No tiene invitaciones para compartir!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    ;
                }
                CURRENT_PROCESS++;
                if(showloader)
                    preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                if(showloader)
                    preloader1.dismiss();
            }
        });
    }


}
