package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import custom.AdapterList;
import custom.ObservableScrollView;
import custom.WidgetListChat;
import custom.WidgetResults;
import entities.ObjectSection;
import entities.Photo;
import pushnotifications.Globals;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;


public class ListChat extends BaseBarActivity   {

    Button btn_visible;
    Button btn_invisible;
    EditText txt_input;
    boolean IsInvisible;

    ObservableScrollView scrollView;
    TableLayout tableLayoutContent;
    Bitmap bitmap;
    Bitmap scaledBitmap;
    float dpHeightScreen = 0;
    float dpWidthScreen = 0;
    final float NUM_COLUMNS = 3;
    int threshold = 20;
    int idUser = 0;
    ObservableScrollView scrollOnDemand;
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=105;
    int TOTAL_ROWS=0;
    Bundle bundle;
    boolean isBusy;
    WidgetResults widgetResults;
    SwipeRefreshLayout swipeContainer;
    GridView grid;
    AdapterList adaper;
    ArrayList<Photo> photos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=1;
        //runProcess(this);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_listchat, null);
        this.setContentView(root);
        session=new SessionManager(getApplicationContext());
        reloadActivity();
        bundle = this.getIntent().getExtras();

        this.btn_visible = (Button) findViewById(R.id.btn_visible);
        this.txt_input = (EditText) findViewById(R.id.txt_input);
        this.IsInvisible = false;
        this.idUser= BaseHelper.getUserId(getApplicationContext());
        this.loadUser();
        isBusy =false;
        txt_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchProfiles_onclick(v);
                    return true;
                }
                return false;
            }
        });

        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        photos=new ArrayList<Photo>();
        adaper=new AdapterList(getApplicationContext(),photos);
        grid=(GridView)findViewById(R.id.grid);
        grid.setAdapter(adaper);
        grid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
                if (firstVisibleItem == 0)
                    swipeContainer.setEnabled(true);
                else
                    swipeContainer.setEnabled(false);
                if (!isBusy && firstVisibleItem > 30 && (totalItemCount - visibleItemCount) <= (firstVisibleItem + (totalItemCount -30))) {
                    // I load the next page of gigs using a background task,
                    // but you can call any function here.
                    isBusy = true;
                    getSearchResults(bundle);

                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub

            }
        });

        getSearchResults(bundle);
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                CURRENT_PAGE = 0;
                photos.clear();
                adaper.notifyDataSetChanged();
                getSearchResults(bundle);
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
    @Override
    public void onResume(){
        loadHeader();
        configure();
        loadUser();
        super.onResume();

    }
    @Override
    protected void onNewIntent(Intent intent) {
        CURRENT_PAGE = 0;
        bundle = intent.getExtras();
        photos.clear();
        adaper.notifyDataSetChanged();
        getSearchResults(bundle);
        super.onNewIntent(intent);
    }
    private void configure() {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(btn_visible, 1.0f);
                break;
            }
            case "I":{
                BaseHelper.applyAlpha(btn_visible, 0.5f);

                break;
            }
            case "B":{
                BaseHelper.applyAlpha(btn_visible, 0.5f);
                break;
            }
        }
    }
    public void getSearchResults(Bundle bundle) {
        RequestParams params = new RequestParams();
        params.put("ID_USER", this.idUser);
        params.put("page", CURRENT_PAGE);
        params.put("page_size", PAGE_SIZE);
        if(bundle.getString("nickname")!=null)
            params.put("nickname", bundle.getString("nickname"));
        Request.post("SearchFilters/listachat", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                try {
                    if(response.length()>0) {
                        ListChat.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (int index = 0; index < response.length(); index++) {
                                    try {
                                        JSONObject item = response.getJSONObject(index);
                                        Photo photo1 = new Photo();
                                        String path = item.getString("PathComplete") != "null" ? item.getString("PathComplete") : "";
                                        int photoId = item.getString("ID_PHOTO") != "null" ? Integer.valueOf(item.getInt("ID_PHOTO")) : 0;
                                        photo1.setID_PHOTO(photoId);
                                        photo1.setPathComplete(path);
                                        photo1.setID_USER(item.getInt("ID_USER"));
                                        photo1.setName(item.getString("Nickname"));
                                        TOTAL_ROWS = item.getInt("total_rows");
                                        TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                                        photos.add(photo1);
                                        adaper.notifyDataSetChanged();
                                    } catch (Exception e) {
                                    }
                                }

                            }
                        });

                        CURRENT_PAGE++;

                    }
                    isBusy = false;
                } catch (Exception e) {
                    isBusy = false;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                isBusy = false;
                Preloader.getInstance(ListChat.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    public void changeVisible_onclick(View v) {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                this.setVisibility();
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }

    public void searchProfiles_onclick(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        String value=txt_input.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("nickname", value.trim());
        if(isBusy ==false && value.length()>0) {
            isBusy = true;
            CURRENT_PAGE = 0;
            photos.clear();
            getSearchResults(bundle);
        }
    }

    public void loadUser() {

        RequestParams params = new RequestParams();
        params.put("id", this.idUser);
        Request.get("users/user", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    IsInvisible = (response.getInt("IsInvisible") == 1) ? true : false;
                    setShowVisibility();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


            }
        });
    }

    public void setVisibility() {

        RequestParams params = new RequestParams();
        params.put("ID_USER", this.idUser);
        Request.post("users/changevisible", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    IsInvisible = (response.getInt("IsInvisible") == 1) ? true : false;
                    setShowVisibility();
                }
                catch (Exception e){}

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }
        });
    }

    public void setShowVisibility() {
        btn_visible = (Button) root.findViewById(R.id.btn_visible);
        if ( IsInvisible) {
            btn_visible.setText(R.string.sf_btn_invisible);
            btn_visible.setBackgroundColor(Color.parseColor("#bf284a"));
            Drawable img = getApplicationContext().getResources().getDrawable( R.mipmap.icon_visible_not_24 );
            btn_visible.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
            session.setVisibility(false);

        }
        else {
            btn_visible.setText(R.string.sf_btn_visible);
            btn_visible.setBackgroundColor(Color.parseColor("#51C794"));
            Drawable img = getApplicationContext().getResources().getDrawable( R.mipmap.icon_visible_24 );
            btn_visible.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
            session.setVisibility(true);
        }

    }

}
