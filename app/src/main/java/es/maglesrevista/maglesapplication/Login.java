package es.maglesrevista.maglesapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import custom.ErrorAdapter;
import entities.User;
import pushnotifications.Globals;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;


public class Login extends BaseBarActivity implements Validator.ValidationListener {

    public static final String EXTRA_MESSAGE = "message";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    GoogleCloudMessaging gcm;
    String regid;

    AtomicInteger msgId = new AtomicInteger();

    @NotEmpty(message = "El campo usuario o email, es obligatorio.")
    EditText txt_username;
    @NotEmpty(message = "El campo contraseña, es obligatorio.")
    EditText txt_password;
    TextView textIniSession;
    TextView textCreateAccount;
    TextView chk_close_session_text;
    TextView txv_forget_passwortd;
    CheckBox cbx_close_session;
    Context currentContext;
    Button btnLogin;
    SessionManager session;
    ImageView img;
    Validator validator;
    Dialog preloder=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preloder=Preloader.getInstance(Login.this).build();
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        currentContext = getApplicationContext();
        this.txt_username = (EditText) findViewById(R.id.username);
        this.txt_password = (EditText) findViewById(R.id.password);
        this.textIniSession = (TextView) findViewById(R.id.textIniSession);
        this.textCreateAccount = (TextView) findViewById(R.id.textCreateAccount);
        this.chk_close_session_text = (TextView) findViewById(R.id.chk_close_session_text);
        this.txv_forget_passwortd = (TextView) findViewById(R.id.txv_forget_passwortd);
        this.cbx_close_session = (CheckBox) findViewById(R.id.chk_close_session);
        cbx_close_session.setChecked(true);
        this.btnLogin = (Button) findViewById(R.id.btnLogin);
        validator=new Validator(this);
        validator.setValidationListener(this);
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
        session = new SessionManager(getApplicationContext());
        txt_username.setText(session.getUserDetails().get(SessionManager.KEY_NICKNAME));

        img= (ImageView)findViewById(R.id.imageLogo);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    public void testDownload(){
        Request.get("/base/file", null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    Log.w("Magles", cadena);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
                    img.setImageBitmap(bitmap);
                } catch (Exception ex) {
                    Log.v("testDownload1_login", ex.getMessage());
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                try {

                } catch (Exception ex) {
                    Log.v("testDownload2_login", ex.getMessage());
                }
            }
        });

    }



    public void createAccount_OnClick(View v) {
        Preloader.getInstance(Login.this).dialog().cancel();
        Intent intent = new Intent(Login.this, CreateAccount.class);
        startActivity(intent);
        Preloader.getInstance(Login.this).dialog().cancel();
    }


    @Override
    public void onValidationSucceeded() {
        preloder.show();
            if (!txt_username.getText().toString().equals("") && !txt_password.getText().toString().equals("")) {
                RequestParams params = new RequestParams();
                params.put("username", txt_username.getText());
                params.put("password", txt_password.getText());
                params.put("timezone", TimeZone.getDefault().getID());
                Request.post("users/login", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, JSONObject object) {
                        try {
                            User user = new User();
                            user.setID_USER(object.getInt("ID_USER"));
                            user.setName(object.getString("Name"));
                            user.setLastName(object.getString("LastName"));
                            user.setNickname(object.getString("Nickname"));
                            user.setEmail(object.getString("Email"));
                            user.setPassword(txt_password.getText().toString());
                            user.setStatus(object.getString("Status"));
                            user.setRegistrationId(object.getString("registrationId"));
                            session.createLoginSession(user.getNickname(), user.getEmail(), String.valueOf(user.getID_USER()), txt_username.getText().toString(), txt_password.getText().toString(), user.getStatus(), true);
                            if (!cbx_close_session.isChecked()) {
                                session.setKeepLogin(false);
                            }
                            preloder.hide();
                                registerInBackground();

                        } catch (Exception ex) {
                            preloder.hide();
                            session.setLogin(false);
                            Preloader.getInstance(Login.this).dialogInfo("Invalid username or password !");
                        }
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        preloder.hide();
                        session.setLogin(false);
                       Preloader.getInstance(Login.this).dialogInfo("Invalid username or password !");
                    }
                });
            } else {
                preloder.hide();
                session.setLogin(false);
                Preloader.getInstance(Login.this).dialogInfo("Debe rellenar los datos para logearse!");
            }
    }
    private void registerInBackground()
    {
        preloder.show();
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (gcm == null)
                    {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(Globals.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg)
            {
                sendRegistrationIdToServer(regid);
            }
        }.execute(null, null, null);
    }
    public void sendRegistrationIdToServer(String registrationId){
        RequestParams params=new RequestParams();
        params.put("user_id",BaseHelper.getUserId(getApplicationContext()));
        params.put("registrationId", registrationId);
        Request.post("users/saveregistration", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    session.setRegistrationId(response.getString("registrationId"));
                    session.setLogin(true);
                    Intent intent = new Intent().setClass(Login.this, home.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } catch (Exception ex) {
                }
                preloder.hide();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloder.hide();
                Preloader.getInstance(Login.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloder.hide();
                Preloader.getInstance(Login.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }
        });
    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        ArrayList<String> error_list=new ArrayList<String>();
        int pos=0;
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            error_list.add(message);
            pos++;
        }
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialgo_error);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        String validations = "";
        TextView txtTitle = (TextView) dialog.findViewById(R.id.title_dialog);
        ListView list = (ListView) dialog.findViewById(R.id.error_list);
        txtTitle.setHeight(0);
        ErrorAdapter adapter = new ErrorAdapter(Login.this, error_list);
        list.setAdapter(adapter);
        dialog.show();
    }
    public void recoverypass_onclick(View view)
    {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_recoverypassword);
        final EditText  txt_input=(EditText)dialog.findViewById(R.id.txt_input);
        txt_input.setHint(R.string.ca_email);
        txt_input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
        txt_input.setTypeface(TextFont.getInstance().getFont(getApplicationContext()));
        TextView txt_ok= (TextView)dialog.findViewById(R.id.txt_si);
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recoveryPassword(txt_input.getText().toString());
                dialog.cancel();
            }
        });
        dialog.show();

    }
    private void recoveryPassword(final String email) {
        RequestParams params = new RequestParams();
        params.put("email", email);
        Request.post("users/recoverypassword", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                dialog = new Dialog(Login.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_info);
                TextView texto = (TextView) dialog.findViewById(R.id.text_dialog);
                texto.setText("Hemos enviado una nueva contraseña para la usuaria del email: " + email);
                dialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }
    public void check_onclick(View view){
        if(cbx_close_session.isChecked())
            cbx_close_session.setChecked(false);
        else
            cbx_close_session.setChecked(true);
    }
    private void unregister()
    {
        Log.d(Globals.TAG, "UNREGISTER USERID: " + regid);
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (gcm == null)
                    {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    gcm.unregister();
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg)
            {
                session.setRegistrationId("");
                sendRegistrationIdToServer("");

            }
        }.execute();
    }
     
}
