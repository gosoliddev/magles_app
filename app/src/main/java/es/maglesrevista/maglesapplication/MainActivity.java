package es.maglesrevista.maglesapplication;

//import android.app.ActionBar;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.nfc.FormatException;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.support.v7.app.ActionBar;

import com.codebutler.android_websockets.WebSocketClient;
import com.comcast.freeflow.layouts.HLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Time;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import chatsocket.MessagesListAdapter;
import custom.UploadChat;
import entities.User;
import other.Message;
import other.Utils;
import other.WsConfig;
import utils.BaseHelper;
import utils.DateUtils;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.ResizeAnimation;
import utils.SessionManager;
import utils.TextFont;

public class MainActivity extends BaseBarActivity implements UploadChat.IUploadChat{

	ArrayList<User> users_list;
	ListView list_userschat;
	public static final String GROUP_KEY="chat_magles";
	// LogCat tag
	private static final String TAG = MainActivity.class.getSimpleName();

	private Button btnSend;
	private EditText inputMsg;

	private WebSocketClient client;

	// Chat messages button_select_list adapter
	private MessagesListAdapter adapter;
	private List<Message> listMessages;
	private ListView listViewMessages;

	private Utils utils;

	// Client name
	private String name = null;
	private String nicknameFriend=null;
	// JSON flags to identify the kind of JSON response
	private static final String TAG_SELF = "self", TAG_NEW = "new",
			TAG_MESSAGE = "message", TAG_EXIT = "exit";

	MainActivity current;
	boolean collapse=true;
	LinearLayout container_text;
	LinearLayout more,photo;
	LinearLayout options;
	int widthPhoto,widthMore;
	Bundle bundle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		session = new SessionManager(getApplicationContext());
		this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_main, null);
		this.setContentView(root);
		loadHeader();
		current = this;
		users_list = new ArrayList<User>();

		btnSend = (Button) findViewById(R.id.btnSend);
		inputMsg = (EditText) findViewById(R.id.inputMsg);
		inputMsg.clearFocus();
		listViewMessages = (ListView) findViewById(R.id.list_view_messages);
		container_text = (LinearLayout) findViewById(R.id.container_text);
		options = (LinearLayout) root.findViewById(R.id.options);
		more = (LinearLayout) root.findViewWithTag("more");
		photo = (LinearLayout) root.findViewWithTag("send_photo");
		enableChat(false);
		utils = new Utils(getApplicationContext());

		// Getting the person name from previous screen

		bundle = getIntent().getExtras();
		if (bundle.getString("type") == null || bundle.getString("type").isEmpty()) {
			if (bundle.getString("name") != null) {
				name = bundle.getString("name");
			} else {
				name = BaseHelper.getNickname(this);
			}

/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				nicknameFriend = bundle.getString("nicknameFriend");
			}
		} else {
			if (bundle.getString("name") != null) {
				nicknameFriend = bundle.getString("name");
			} else {
				nicknameFriend = BaseHelper.getNickname(this);
			}

/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				name = bundle.getString("nicknameFriend");
			}
		}

		btnSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// Sending message to web socket server
				if (!inputMsg.getText().toString().equals("")) {
					sendMessageToServer(utils.getSendMessageJSONChat(inputMsg.getText().toString(), nicknameFriend));
					inputMsg.setText("");
				}
				// Clearing the input filed once message was sent

			}
		});

		listMessages = new ArrayList<Message>();

		adapter = new MessagesListAdapter(this, listMessages);
		listViewMessages.setAdapter(adapter);
		TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);

	}

	private void actualizarChat()
	{
		try
		{
			String usuario_transmisor="";
			String usuario_receiver="";
			if(bundle.getString("type")==null || bundle.getString("type").isEmpty()) {
				 usuario_transmisor = getIntent().getStringExtra("name");//getIntent().getStringExtra("nickname_transmisor");
				 usuario_receiver = getIntent().getStringExtra("nicknameFriend");//getIntent().getStringExtra("nickname_receiver");
			}
			else
			{
				usuario_transmisor = getIntent().getStringExtra("nicknameFriend");
				usuario_receiver = getIntent().getStringExtra("name");;
			}

			//String nickname_transmisor, String nickname_receiver
			getMassageChatComplete(usuario_transmisor, usuario_receiver);

		}catch(Exception ex)
		{
			Log.v("MainActivity", "actualizarChat -- "+ex.fillInStackTrace());
		}
	}

	public void cerrarRootToggleMenu(){
	   this.root.toggleMenu();
   }
	/**
	 * Method to send message to web socket server
	 * */
	private void sendMessageToServer(String message) {
		if (client != null && client.isConnected()) {
			client.send(message);
		}
	}

	/**
	 * Parsing the JSON message received from server The intent of message will
	 * be identified by JSON node 'flag'. flag = self, message belongs to the
	 * person. flag = new, a new person joined the conversation. flag = message,
	 * a new message received from server. flag = exit, somebody left the
	 * conversation.
	 * */
	private void parseMessage(final String msg) {

		try {
			JSONObject jObj = new JSONObject(msg);

			// JSON node 'flag'
			String flag = jObj.getString("flag");

			// if flag is 'self', this JSON contains session id
			if (flag.equalsIgnoreCase(TAG_SELF)) {

				String sessionId = jObj.getString("sessionId");

				// Save the session id in shared preferences
				utils.storeSessionId(sessionId);

				Log.e(TAG, "Your session id: " + utils.getSessionId());

			} else if (flag.equalsIgnoreCase(TAG_NEW)) {
				// If the flag is 'new', new person joined the room
				String name = jObj.getString("name");
				String message = jObj.getString("message");

				// number of people online
				String onlineCount = jObj.getString("onlineCount");

				//showToast(name + message + ". Currently " + onlineCount + " people online!");
				getActivesOnChat();


			} else if (flag.equalsIgnoreCase(TAG_MESSAGE) || flag.equalsIgnoreCase("accepted") || flag.equalsIgnoreCase("image"))  {
				// if the flag is 'message', new message received
				String fromName = name;
				String message = jObj.getString("message");
				String sessionId = jObj.getString("sessionId");
				boolean isSelf = true;
				fromName = jObj.getString("name");
				// Checking if the message was sent by you
				boolean readed=true;
				if (!name.equals(fromName)) {
					isSelf = false;

				}else
				{
					String strReaded = jObj.getString("readed");
					readed=strReaded.equals("1");
				}
				if(nicknameFriend.equals(fromName) || name.equals(fromName)) {
					Message m = new Message(fromName, message, isSelf);
					Date date = new Date();
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String fecha = formatter.format(date);
					try {

						Date date_send = formatter.parse(fecha);
						String date_current = DateUtils.getTimeAgo(date_send,getApplicationContext());
						m.setDate(date_current);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					m.setType_message(flag);
					m.setReceiver(nicknameFriend);
					m.setReaded(readed);
					// Appending the message to chat button_select_list
					appendMessage(m);
				}else
				{
					buildNotification(fromName,name,message,jObj.getString("flag"));
				}

			} else if (flag.equalsIgnoreCase(TAG_EXIT)) {
				// If the flag is 'exit', somebody left the conversation
				String name = jObj.getString("name");
				String message = jObj.getString("message");

				//showToast(name + message);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	public void clearNotification(){
		int id=bundle.getInt("id");
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
		nMgr.cancel(id);
	}
	@Override
	public void onNewIntent(Intent intent){
		// Getting the person name from previous screen
		bundle = intent.getExtras();
		if(bundle.getString("type")==null || bundle.getString("type").isEmpty()) {
			if (bundle.getString("name") != null) {
				name = bundle.getString("name");
			} else {
				name = BaseHelper.getNickname(this);
			}

		/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				nicknameFriend = bundle.getString("nicknameFriend");
			}
		}
		else
		{
			if (bundle.getString("name") != null) {
				nicknameFriend = bundle.getString("name");
			} else {
				nicknameFriend = BaseHelper.getNickname(this);
			}

		/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				name = bundle.getString("nicknameFriend");
			}
			listMessages.clear();
			actualizarChat();
			clearNotification();
		}

		setIntent(intent);
		super.onNewIntent(intent);
	}
	@Override
	public void onResume(){
		super.onResume();
		if(bundle.getString("type")==null || bundle.getString("type").isEmpty()) {
			if (bundle.getString("name") != null) {
				name = bundle.getString("name");
			} else {
				name = BaseHelper.getNickname(this);
			}
		/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				nicknameFriend = bundle.getString("nicknameFriend");
			}
		}
		else
		{
			if (bundle.getString("name") != null) {
				nicknameFriend = bundle.getString("name");
			} else {
				nicknameFriend = BaseHelper.getNickname(this);
			}

		/*for chat friend*/
			if (bundle.getString("nicknameFriend") != null) {
				name = bundle.getString("nicknameFriend");
			}
		}

		listMessages.clear();
		actualizarChat();
		clearNotification();
		try {
			client = new WebSocketClient(URI.create(WsConfig.URL_WEBSOCKET +URLEncoder.encode(name)+"&friend="+URLEncoder.encode(nicknameFriend)), new WebSocketClient.Listener() {

                @Override
                public void onConnect() {
    //					actualizarChat();
                }

                /**
                 * On receiving the message from web socket server
                 */
                @Override
                public void onMessage(String message) {
                    Log.d(TAG, String.format("Got string message! %s", message));
                    parseMessage(message);
                }

                @Override
                public void onMessage(byte[] data) {
                    Log.d(TAG, String.format("Got binary message! %s", bytesToHex(data)));
                    // Message will be in JSON format
                    parseMessage(bytesToHex(data));
                }

                /**
                 * Called when the connection is terminated
                 */
                @Override
                public void onDisconnect(int code, String reason) {
                    String message = String.format(Locale.US,
                            "Disconnected! Code: %d Reason: %s", code, reason);
                    //showToast(message);
                    // clear the session id from shared preferences
                    utils.storeSessionId(null);
                }

                @Override
                public void onError(Exception error) {
                    Log.e(TAG, "Error! : " + error);

                    //showToast("Error! : " + error);
                }

            }, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		client.connect();

	}
	@Override
	public void onPause() {
		super.onPause();
		if(client != null & client.isConnected()){
			client.disconnect();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if(client != null & client.isConnected()){
			client.disconnect();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(client != null & client.isConnected()){
			client.disconnect();
		}
	}

	/**
	 * Appending message to button_select_list view
	 * */
	private void appendMessage(final Message m) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				listMessages.add(m);

				adapter.notifyDataSetChanged();

				// Playing device's notification
				if(!m.isSelf())
				{
					playBeep();
				}
				listViewMessages.smoothScrollToPosition(adapter.getCount() - 1);

			}
		});

	}

	private void showToast(final String message) {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), message,
						Toast.LENGTH_LONG).show();
			}
		});

	}

	/**
	 * Plays device's default notification sound
	 * */
	public void playBeep() {

		try {
			Uri notification = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
					notification);
			r.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public void getActivesOnChat() {
		RequestParams params = new RequestParams();
		params.put("nickname", this.name);
		try
		{
			Request.get("chats/activesonchat", params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				try {
					users_list.clear();
					list_userschat.invalidateViews();
					list_userschat.refreshDrawableState();
					//list_userschat.clearChoices();
					for (int i = 0; i < response.length(); i++) {
						JSONObject obj = response.getJSONObject(i);
						User user = new User();
						user.setID_USER(obj.getInt("ID_USER"));
						user.setNickname(obj.getString("Nickname"));
						users_list.add(user);
					}
					UserAdapter adapter = new UserAdapter(current, users_list);
					list_userschat.setAdapter(adapter);
				} catch (Exception ex) {

				}
			}

				@Override
				public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
			}
			});
		}catch(Exception ex)
		{

		}
	}
	public void enableChat(boolean status)
	{
		if(status)
		{
			BaseHelper.applyAlpha(container_text, 1.0f);
			btnSend.setClickable(true);
			btnSend.setEnabled(true);
			inputMsg.setClickable(true);
			inputMsg.setEnabled(true);

		}
		else
		{
			BaseHelper.applyAlpha(container_text,0.5f);
			btnSend.setClickable(false);
			btnSend.setEnabled(false);
			inputMsg.setClickable(false);
			inputMsg.setEnabled(false);
		}
	}
	public void getMassageChatComplete(String nickname_transmisor, final String nickname_receiver) {

		RequestParams params = new RequestParams();
		params.put("nickname_owner", nickname_transmisor);
		params.put("nickname_friends", nickname_receiver);
		Request.post("Chats/messageschat", params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				try {
					String usuario = BaseHelper.getNickname(getApplicationContext());
					//if(response.length()>1)
					enableChat(true);
					for (int i = 0; i < response.length(); i++) {
						JSONObject obj = response.getJSONObject(i);
						Message message = new Message();
						String transmiter = obj.getString("transmitter");
						message.setFromName(transmiter);
						message.setReceiver(obj.getString("receiver"));
						message.setMessage(obj.getString("text"));
						message.setType_message(obj.getString("type_message"));

						try {
							SimpleDateFormat formatter_origin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date fecha=DateUtils.convertFromOneTimeZoneToOhter(formatter_origin.parse(obj.getString("date")), "Europe/Madrid", TimeZone.getDefault().getID());

							String date_current = DateUtils.getTimeAgo(fecha, getApplicationContext());
							message.setDate(date_current);

						} catch (ParseException e) {
							e.printStackTrace();
						}
						message.setReaded((obj.getString("readed").equals("1")) ? true : false);
						if (usuario.equals(transmiter)) {
							message.setSelf(true);
						} else {
							message.setSelf(false);
						}
						listMessages.add(message);
						adapter.notifyDataSetChanged();

					}

					//adapter = new MessagesListAdapter(current, listMessages);
					//listViewMessages.setAdapter(adapter);
				} catch (Exception ex) {
				}

				widthPhoto = photo.getWidth();
				widthMore = more.getWidth();
				ResizeAnimation resizeAnimation = new ResizeAnimation(options, widthMore);
				resizeAnimation.setDuration(300);
				options.startAnimation(resizeAnimation);
				options.bringToFront();
				RequestParams params=new RequestParams();
				params.put("nickname", BaseHelper.getNickname(getApplicationContext()));
				Request.get("chats/messagesnotreaded/", params, new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
						super.onSuccess(statusCode, headers, response);
						ImageView img = (ImageView) root.findViewById(R.id.icn_4);
						if (response.length() > 0) {
							img.setImageResource(R.mipmap.icn_message_dot);
						} else {
							img.setImageResource(R.mipmap.icn_message);

						}
					}

					@Override
					public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
						super.onFailure(statusCode, headers, throwable, errorResponse);

					}
				});
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				enableChat(true);
				listMessages.clear();
				adapter.notifyDataSetChanged();
			}
		});

	}

	/**
	 * Appending message to button_select_list view
	 * */
	private void appendMessageStart(final Message m) {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				listMessages.add(m);

				adapter.notifyDataSetChanged();

				// Playing device's notification
			}
		});
		loadHeader();
	}


	public  void expand(final LinearLayout v) {
		View view=(View)options;
		photo.setVisibility(View.VISIBLE);
		ResizeAnimation resizeAnimation = new ResizeAnimation(view,widthMore*2 );
		resizeAnimation.setDuration(300);
		view.startAnimation(resizeAnimation);
		view.bringToFront();
	}

	public   void collapse(final View v) {
		View view=(View)options;
		photo.setVisibility(View.INVISIBLE);
		ResizeAnimation resizeAnimation = new ResizeAnimation(view, widthMore);
		resizeAnimation.setDuration(300);
		view.startAnimation(resizeAnimation);
		view.bringToFront();
	}
	public void openoptions_onclick(View view)
	{
		LinearLayout ln=(LinearLayout)root.findViewById(R.id.options);
		if(collapse)
		{
			expand(ln);
			collapse=false;
		}
		else {
			collapse(ln);
			collapse=true;
		}
	}
	public void acepted_onclick(View view){
		 sendReponse(2);

	}
	public void denied_onclick(View view){
		sendReponse(3);
	}
	public void sendReponse(int band)
	{
		Preloader.getInstance(MainActivity.this).dialog().show();
		RequestParams params=new RequestParams();
		params.put("accepted", band);
		params.put("owner", name);
		params.put("contact", nicknameFriend);
		Request.post("RelationShips/acceptchat", params, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				Preloader.getInstance(MainActivity.this).dialog().cancel();
				try {
					int status = response.getInt("TrustRelationships");
					if (status == 1) {
						sendMessageToServer(utils.getSendMessageAceptedJSONChat("accepted", nicknameFriend));
					} else if (status == -0) {
						MainActivity.this.finish();
					}
				} catch (Exception e) {
					Preloader.getInstance(MainActivity.this).dialogInfo("Falló insperado, vuelva intentarlo.");
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				Preloader.getInstance(MainActivity.this).dialog().cancel();
				String error = "";
				try {
					error = errorResponse.getString("error");
				} catch (Exception ex) {
					error = "Falló insperado, vuelva intentarlo.";
				}
				Preloader.getInstance(MainActivity.this).dialogInfo(error);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				Preloader.getInstance(MainActivity.this).dialog().cancel();
				Preloader.getInstance(MainActivity.this).dialogInfo("Falló insperado, vuelva intentarlo.");
			}
		});

	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void upload_onclick(View view){
		UploadChat dialog = new UploadChat();
		dialog.setUserId(BaseHelper.getUserId(getApplicationContext()));
		dialog.show(getFragmentManager(), "send");
		ResizeAnimation resizeAnimation = new ResizeAnimation(options, widthMore);
		resizeAnimation.setDuration(300);
		options.startAnimation(resizeAnimation);
		options.bringToFront();
		collapse=true;
	}

	@Override
	public void updateResult(String name) {
		sendMessageToServer(utils.getSendMessageImageJSONChat("photos/file/id/" + BaseHelper.getUserId(getApplicationContext()) + "/namePhoto/" + name, nicknameFriend));
	}
	public void buildNotification(String name,String nicknameFriend,String message,String type_message){
		Bitmap bitmap=getBitmapFromURL(Request.BASE_URL+"Photos/getPhotoByNickname/nickname/"+name);
		NotificationCompat.Builder builder;
		Preloader preloder=Preloader.getInstance(MainActivity.this);
		int i=preloder.getNoti_local_id();
		int id=i;
		preloder.setNoti_local_id(i-1);
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("type", "1");
		intent.putExtra("id",id);
		intent.putExtra("name", name);
		intent.putExtra("nicknameFriend",nicknameFriend);
		intent.putExtra("type_message",type_message);
		intent.putExtra("date","");
		Date date= DateUtils.currentDate();
		String date_sent="";
		try
		{
			SimpleDateFormat formatter =new SimpleDateFormat("HH:mm");
			date_sent=formatter.format(date);
		}
		catch (Exception e){
			;
		}
		PendingIntent pIntent = PendingIntent.getActivity(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);
		builder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_stat_logo)
				.setContent(remoteViews)
				.setContentIntent(pIntent)
				.setAutoCancel(true)
				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
				.setGroup(GROUP_KEY)
				.setGroupSummary(true);
		remoteViews.setImageViewBitmap(R.id.notifiation_image, bitmap);

		remoteViews.setTextViewText(R.id.notification_title, "MagLes Match");
		remoteViews.setTextViewText(R.id.notification_text, name + "\n" + message);
		remoteViews.setTextViewText(R.id.notification_time ,date_sent);


		// Create Notification Manager
		NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Build Notification with Notification Manager
		notificationmanager.notify(id, builder.build());
	}
	public Bitmap getBitmapFromURL(String strURL) {
		try {
			URL url = new URL(strURL);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			myBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] byteArray = stream.toByteArray();
			myBitmap= ImageUtils.decodeFile(byteArray, 200);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
