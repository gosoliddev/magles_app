package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.ObservableScrollView;
import custom.WidgetMatch;
import entities.User;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.TextFont;

public class Match extends BaseBarActivity implements ScrollViewListener{

    int userId=0;
    WidgetMatch list_match;
    TextView title_math;
    int type=1;
    TextView txt_matchfavorite,txt_matchinteres;
    ImageView icon_matchfavorite;
    LinearLayout btn_mathinterest;
    LinearLayout btn_mathfavorite;
    Bundle bundle;
    SwipeRefreshLayout swipeContainer;
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=20;
    int TOTAL_ROWS=0;
    Boolean isBusy=true;
    ObservableScrollView scrollOnDemand;
    TextView btnupdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle=getIntent().getExtras();
        if(bundle!=null && bundle.getInt("type")!=0)
        {
            type=bundle.getInt("type");
        }
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_match, null);
        this.setContentView(root);
        btnupdate=(TextView)root.findViewById(R.id.btnupdate);
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        swipeContainer=(SwipeRefreshLayout)root.findViewById(R.id.swipeContainer);
        reloadActivity();
        this.userId= BaseHelper.getUserId(getApplicationContext());
        list_match=(WidgetMatch)root.findViewById(R.id.list_match);
        title_math=(TextView)root.findViewById(R.id.title_math);
        txt_matchinteres=(TextView)root.findViewById(R.id.txt_matchinteres);
        txt_matchfavorite=(TextView)root.findViewById(R.id.txt_matchfavorite);
        icon_matchfavorite=(ImageView)root.findViewById(R.id.icon_matchfavorite);
        btn_mathfavorite=(LinearLayout)root.findViewById(R.id.btn_mathfavorite);
        btn_mathinterest=(LinearLayout)root.findViewById(R.id.btn_mathinterest);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        txt_matchinteres.setText(Html.fromHtml("MATCH POR <font color='#bf284a'>INTERESES</font>"));
        load(type);
    }
    @Override
    public void onResume(){
        loadHeader();
        super.onResume();
    }
    public void loadByFavorite_onclick(View view){
        if(!isBusy) {
            title_math.setText("");
            type = 1;
            list_match.clear();
            CURRENT_PAGE = 0;
            load(type);
        }
    }
    public void loadByInterest_onclick(View view){
        if(!isBusy) {
            type = 2;
            list_match.clear();
            CURRENT_PAGE = 0;
            load(type);
        }
    }
    public void loadUserProfile(int userId){
//        final Dialog preloader3=Preloader.getInstance(Match.this).build();
//        preloader3.show();
        Request.get("users/userProfile/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                User user = new User();
                try {
                    if (response.getString("ID_PREFERENCE").isEmpty() || response.getString("ID_PREFERENCE").equals("null")) {
                        Preloader.getInstance(Match.this).dialogInfo("Para ver tu Match con otras usuarias, has de rellenar el formulario \n Lo que buscas desde Configuración");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CURRENT_PROCESS++;
                //preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                //preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                //preloader3.dismiss();
            }
        });
    }
    public void load(int type){
        if(type==1)
        {
            btnupdate.setVisibility(View.GONE);
            title_math.setText("¡Tú también eres su favorita!");
            txt_matchfavorite.setTextColor(Color.parseColor("#ffffff"));
            btn_mathfavorite.setBackgroundColor(Color.parseColor("#bf284a"));
            icon_matchfavorite.setImageResource(R.mipmap.icon_favorite_white);

            txt_matchinteres.setTextColor(Color.parseColor("#343434"));
            btn_mathinterest.setBackgroundColor(Color.parseColor("#ffffff"));
            txt_matchinteres.setText(Html.fromHtml("MATCH POR <font color='#242424'><strong>INTERESES</strong></font>"));
            isBusy=true;
            loadMatchByFavorite(userId);
        }
        else if(type==2)
        {
            btnupdate.setVisibility(View.VISIBLE);
            title_math.setText("Tenéis un Match según vuestros perfiles e intereses.");
            txt_matchinteres.setTextColor(Color.parseColor("#ffffff"));
            btn_mathinterest.setBackgroundColor(Color.parseColor("#bf284a"));
            txt_matchinteres.setText(Html.fromHtml("MATCH POR <font color='#ffffff'><strong>INTERESES</strong></font>"));

            txt_matchfavorite.setTextColor(Color.parseColor("#343434"));
            btn_mathfavorite.setBackgroundColor(Color.parseColor("#ffffff"));
            icon_matchfavorite.setImageResource(R.mipmap.icon_favorite_dark);
            loadUserProfile(BaseHelper.getUserId(getApplicationContext()));
            isBusy=true;
            loadMatchByInterest(userId);
        }
    }

    private void loadMatchByFavorite(int userId) {
//        final Dialog preloader1=Preloader.getInstance(Match.this).build();
//        preloader1.show();
        RequestParams params = new RequestParams();
        params.put("user_id", BaseHelper.getUserId(getApplicationContext()));
        params.put("page_size",PAGE_SIZE);
        params.put("page", CURRENT_PAGE);
        Request.post("RelationShips/findMatchByFavorite", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                if(response.length()>0) {
                    ArrayList<entities.Photo> photos = new ArrayList<entities.Photo>();
                    for (int index = 0; index < response.length(); index++) {
                        try {
                            JSONObject item = response.getJSONObject(index);
                            entities.Photo photo1 = new entities.Photo();
                            String path = item.getString("PathComplete") != "null" ? item.getString("PathComplete") : "";
                            int photoId = item.getString("ID_PHOTO") != "null" ? Integer.valueOf(item.getInt("ID_PHOTO")) : 0;
                            photo1.setID_PHOTO(photoId);
                            photo1.setPathComplete(path);
                            photo1.setName("");
                            photo1.setNickname(item.getString("Nickname"));
                            photo1.setID_USER(item.getInt("ID_USER"));
                            TOTAL_ROWS = item.getInt("total_rows");
                            TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                            photos.add(photo1);
                        } catch (Exception e) {

                        }
                    }
                    list_match.setColumns(1);
                    list_match.setTypeView(1);
                    list_match.setDataSource(photos);
                    list_match.dataBind();
                    CURRENT_PAGE++;

                }
                isBusy = false;
                //preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                isBusy=false;
                Preloader.getInstance(Match.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                isBusy=false;
                Preloader.getInstance(Match.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");
            }
        });
    }
    private void loadMatchByInterest(int userId) {
//        final Dialog preloader2=Preloader.getInstance(Match.this).build();
//        preloader2.show();
        RequestParams params = new RequestParams();
        params.put("user_id", BaseHelper.getUserId(getApplicationContext()));
        params.put("page_size", PAGE_SIZE);
        params.put("page", CURRENT_PAGE);
        Request.post("RelationShips/findmatchiyinterest", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                if (response.length() > 0) {
                    ArrayList<entities.Photo> photos = new ArrayList<entities.Photo>();
                    for (int index = 0; index < response.length(); index++) {
                        try {
                            JSONObject item = response.getJSONObject(index);
                            entities.Photo photo1 = new entities.Photo();
                            String path = item.getString("PathComplete") != "null" ? item.getString("PathComplete") : "";
                            int photoId = item.getString("ID_PHOTO") != "null" ? Integer.valueOf(item.getInt("ID_PHOTO")) : 0;
                            photo1.setID_PHOTO(photoId);
                            photo1.setPathComplete(path);
                            photo1.setName("+" + item.getString("percent_match") + "%");
                            photo1.setNickname(item.getString("Nickname"));
                            photo1.setID_USER(item.getInt("ID_USER"));
                            TOTAL_ROWS = item.getInt("total_rows");
                            TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                            photos.add(photo1);
                        } catch (Exception e) {

                        }
                    }
                    list_match.setColumns(1);
                    list_match.setTypeView(2);
                    list_match.setDataSource(photos);
                    list_match.dataBind();
                    CURRENT_PAGE++;
                }
                isBusy = false;
                //preloader2.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                isBusy = false;
                //preloader2.dismiss();
                Preloader.getInstance(Match.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                isBusy = false;
                //preloader2.dismiss();
                Preloader.getInstance(Match.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");
            }
        });
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isBusy) {
                    CURRENT_PAGE = 0;
                    list_match.clear();
                    load(type);
                }
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);

        // if diff is zero, then the bottom has been reached
        if( diff == 0)
        {
            if(isBusy==false) {
                isBusy=true;
                load(type);
            }
        }
    }
    public void btnupdate_onclick(View view){
        if(!isBusy) {
            CURRENT_PAGE = 0;
            list_match.clear();
            load(type);
        }
    }
}
