package es.maglesrevista.maglesapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.Date;
import java.util.List;

import other.MessageChatUser;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Request;

public class MessageListAdapterHistoryActivity extends BaseAdapter {

    private Context context;
    private List<MessageChatUser> messagesItems;
    private MessageChatUser current;
    LayoutInflater mInflater;
    public MessageListAdapterHistoryActivity(Context context, List<MessageChatUser> navDrawerItems) {
        this.context = context;
        this.messagesItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following button_select_list not implemented reusable button_select_list items as button_select_list items
         * are showing incorrect data Add the solution if you have one
         * */

        MessageChatUser m = messagesItems.get(position);
        ImageView img;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = mInflater.inflate(R.layout.activity_message_list_adapter_history, null);
        img=(ImageView)convertView.findViewById(R.id.photoMsg);
        getDrawable(img,m);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Troasdn asdn ,asnd",Toast.LENGTH_LONG).show();
            }
        });

        TextView txtUsr = (TextView) convertView.findViewById(R.id.txtUsr);
        TextView txtFecha = (TextView) convertView.findViewById(R.id.txtFecha);
        TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);
        ImageView txtVisto = (ImageView) convertView.findViewById(R.id.txtVisto);

        txtUsr.setTypeface(txtUsr.getTypeface(), Typeface.BOLD);

        txtUsr.setText(m.getFromName());
        Date fecha_actual = new Date();

        String date_current = BaseHelper.DiferenciaFechas(fecha_actual ,m.getFromDate());
        txtFecha.setText(date_current);
        txtMsg.setText(m.getMessage());

//        if(m.getIsView())
//            txtVisto.setImageResource(R.mipmap.ic_arrow_list);   //agregar la foto bien

        return convertView;
    }
    public  void getDrawable(final ImageView image ,  MessageChatUser current){
        RequestParams params=new RequestParams();
        params.put("nickname",current.getFromName());
        Request.get("Photos/getPhotoByNickname", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    Bitmap bitmap = ImageUtils.decodeFile(bytes,100);
                    image.setImageBitmap(bitmap);
                    image.setAdjustViewBounds(true);
                } catch (Exception ex) {
                    image.setImageResource(R.mipmap.photo_default);
                }
                catch (OutOfMemoryError ex) {
                    image.setImageResource(R.mipmap.photo_default);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                image.setImageResource(R.mipmap.photo_default);
            }
        });
    }


}
