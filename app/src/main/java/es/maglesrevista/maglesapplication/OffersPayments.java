package es.maglesrevista.maglesapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;

public class OffersPayments extends BaseBarActivity {
    SessionManager session;
    Bundle bundle;
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AQw6F5Tr5XvOt5dJoGAxkJ9ye5zpweAfNztU8IlHziGQRDzplGb_SH2RAi8bwT4Dv_VQ8W-8pcpym3mm";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    LinearLayout btn_offers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session=new SessionManager(getApplicationContext());
        bundle=getIntent().getExtras();
        setContentView(R.layout.activity_offers_payment);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        btn_offers=(LinearLayout)findViewById(R.id.btn_offers);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    @Override
    public void onResume(){
        btn_offers.removeAllViews();
        loadPayments();
        super.onResume();
    }

    public void loadPayments(){
        final Dialog preloader1=Preloader.getInstance(OffersPayments.this).build();
        preloader1.show();
        Request.get("UsersPremium/packages", null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                createButtons(response);
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader1.dismiss();
            }
        });
    }
    public void createButtons(JSONArray array){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i=0;i<array.length();i++)
        {
            try {
                JSONObject item = array.getJSONObject(i);
                View mViewItem = inflater.inflate(R.layout.button_payment, null, false);
                LinearLayout btn = (LinearLayout) mViewItem.findViewById(R.id.btn_payment);
                btn.setClickable(true);
                TextView text_payment=(TextView)mViewItem.findViewById(R.id.text_payment_name);
                TextView text_payment_price=(TextView)mViewItem.findViewById(R.id.text_payment_price);
                TextView duration = (TextView) mViewItem.findViewById(R.id.text_duration);
                text_payment.setText(item.getString("Name"));
                int monthsFree=item.getInt("MonthsFree");
                int monthsDuration=item.getInt("MonthsDuration");
                btn.setTag(item);
                String free = "";
                Spanned textbtn=null;
                if(monthsFree>0 && monthsFree<2) {
                    free=getString(R.string.paypal_btn_help_single).replace("[MONTHS]", item.getString("MonthsFree"));
                }else if(monthsFree>0 && monthsFree>1)
                {
                    free=getString(R.string.paypal_btn_help_pluralize).replace("[MONTHS]", item.getString("MonthsFree"));
                }
                if(monthsDuration>0 && monthsDuration<2) {
                    textbtn= Html.fromHtml(getText(R.string.paypal_btn1).toString().replace("[MONTHS]", monthsDuration + ""));
                }else if(monthsDuration>0 && monthsDuration>1)
                {
                    textbtn=Html.fromHtml(getText(R.string.paypal_btn1_pluralize).toString().replace("[MONTHS]", monthsDuration + ""));
                }

                text_payment.setText(textbtn);
                NumberFormat formatter = new DecimalFormat("#0.00");
                text_payment_price.setText("("+formatter.format(item.getDouble("Price"))+item.getString("Currency_symbol")+")");
                duration.setText(free);
                text_payment.setTypeface(TextFont.getInstance().getFont(getApplicationContext(), TextFont.PATH_FONT));
                text_payment_price.setTypeface(TextFont.getInstance().getFont(getApplicationContext(), TextFont.PATH_FONT));
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            LinearLayout btn = (LinearLayout) v;
                            JSONObject object = (JSONObject) btn.getTag();
                            float price=Float.valueOf(object.getString("Price"));
                            String package_id=object.getString("PACKAGE_ID");
                            String description=object.getString("Name");
                            String currency=object.getString("Currency");
                            Preloader.getInstance(OffersPayments.this).setPACKAGE_ID(package_id);
                            PayPalPayment thingToBuy =  new PayPalPayment(new BigDecimal(price)  , currency, description,PayPalPayment.PAYMENT_INTENT_SALE);

                            Intent intent = new Intent(OffersPayments.this, PaymentActivity.class);

                            // send the same configuration for restart resiliency
                            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

                            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

                            startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                        }
                        catch (Exception e) {}
                    }
                });
                btn_offers.addView(mViewItem);
            }
            catch (Exception e)
            {

            }
        }
    }
    public void onBuyPressed(View pressed) {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(OffersPayments.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        float price=bundle.getFloat("price");
        String description=bundle.getString("description");
        String currency=bundle.getString("currency");
        return new PayPalPayment(new BigDecimal(price)  , currency, description, paymentIntent);
    }

    private PayPalOAuthScopes getOauthScopes() {
        /* create the set of required scopes
         * Note: see https://developer.paypal.com/docs/integration/direct/identity/attributes/ for mapping between the
         * attributes you select for this app in the PayPal developer portal and the scopes required here.
         */
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS) );
        return new PayPalOAuthScopes(scopes);
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        JSONObject root=confirm.toJSONObject();
                        JSONObject response=confirm.toJSONObject().getJSONObject("response");
                        JSONObject client=confirm.toJSONObject().getJSONObject("client");
                        JSONObject payment=confirm.getPayment().toJSONObject();

                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        String id=response.getString("id");
                        String state=response.getString("state");
                        String create_time=response.getString("create_time");
                        String response_type=root.getString("response_type");
                        String platform=client.getString("platform");
                        String paypal_sdk_version=client.getString("paypal_sdk_version");
                        String environment=client.getString("environment");
                        String product_name=client.getString("product_name");

                        Double amount=payment.getDouble("amount");
                        String currency_code=payment.getString("currency_code");
                        String short_description=payment.getString("short_description");
                        String intent=payment.getString("intent");
                        RequestParams param=new RequestParams();
                        param.put("user_id", BaseHelper.getUserId(getApplicationContext()));
                        param.put("package_id", Preloader.getInstance(OffersPayments.this).getPACKAGE_ID());
                        if(BaseHelper.getUserStatus(getApplicationContext()).equals("P")) {
                            param.put("renewpremium", 1);
                        }
                        else
                        {
                            param.put("renewpremium", 0);
                        }

                        param.put("id",id);
                        param.put("state",state);
                        param.put("create_time",create_time);
                        param.put("response_type",response_type);
                        param.put("platform",platform);
                        param.put("paypal_sdk_version",paypal_sdk_version);
                        param.put("environment",environment);
                        param.put("product_name",product_name);
                        param.put("amount",amount);
                        param.put("currency_code",currency_code);
                        param.put("short_description",short_description);
                        param.put("intent",intent);
                        Request.post("Payments/savepayment",param,new JsonHttpResponseHandler()
                        {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                super.onSuccess(statusCode, headers, response);
                                try {
                                    String visible = response.getString("IsInvisible");
                                    if (visible.equals("0")) {
                                        session.setVisibility(true);
                                    }
                                    else
                                    {
                                        session.setVisibility(false);
                                    }
                                    session.setStatus(response.getString("Status"));
                                }
                                catch (Exception e) {}
                                final Dialog dialog1 = new Dialog(OffersPayments.this);
                                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog1.setContentView(R.layout.dialog_premium_success);
                                TextView texto1=(TextView)dialog1.findViewById(R.id.text_dialog1);
                                texto1.setTypeface(TextFont.getInstance().getFont(OffersPayments.this,TextFont.PATH_FONT_BOLD));
                                texto1.setTextSize(18);
                                TextView texto2=(TextView)dialog1.findViewById(R.id.text_dialog2);
                                String text1="",text2="";

                                text1=getString(R.string.methods_message_success_premium1);
                                text2=getString(R.string.methods_message_success_premium2);

                                texto1.setText(text1);
                                texto2.setText(text2);
                                dialog1.show();
                                final Timer t = new Timer();
                                t.schedule(new TimerTask() {
                                    public void run() {
                                        try {

                                            if (dialog1 != null && dialog1.isShowing())
                                                dialog1.cancel(); // when the task active then close the dialog
                                            t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                                            Intent intent = new Intent(OffersPayments.this, home.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                        catch (Exception e){}
                                    }
                                }, 6000);

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                super.onFailure(statusCode, headers, throwable, errorResponse);
                                try {
                                    Preloader.getInstance(OffersPayments.this).dialogInfo(errorResponse.getString("error"));
                                }
                                catch (Exception e){}

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                super.onFailure(statusCode, headers, responseString, throwable);
                                Preloader.getInstance(OffersPayments.this).dialogInfo(getString(R.string.methods_message_wrong));
                            }
                        });
//                        Toast.makeText(
//                                getApplicationContext(),
//                                "PaymentConfirmation info received from PayPal", Toast.LENGTH_LONG)
//                                .show();

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                        Preloader.getInstance(OffersPayments.this).dialogInfo(getString(R.string.methods_message_failed));
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
                Preloader.getInstance(OffersPayments.this).dialogInfo(getString(R.string.methods_message_canceled));
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                Preloader.getInstance(OffersPayments.this).dialogInfo(getString(R.string.methods_message_invalid));
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }

    public void onFuturePaymentPurchasePressed(View pressed) {
        // Get the Client Metadata ID from the SDK
        String metadataId = PayPalConfiguration.getClientMetadataId(this);

        Log.i("FuturePaymentExample", "Client Metadata ID: " + metadataId);

        // TODO: Send metadataId and transaction details to your server for processing with
        // PayPal...
        Toast.makeText(
                getApplicationContext(), "Client Metadata Id received from SDK", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
