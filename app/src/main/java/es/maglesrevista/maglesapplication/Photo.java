package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import custom.PhotoView;
import custom.UploadGallery;
import ru.truba.touchgallery.GalleryWidget.BasePagerAdapter;
import ru.truba.touchgallery.GalleryWidget.GalleryViewPager;
import ru.truba.touchgallery.GalleryWidget.UrlPagerAdapter;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.TextFont;


public class Photo extends BaseBarActivity implements UploadGallery.IUploadGallery {

    int photoId = 0;
    int userId = 0;
    String namePhoto = "";
    ImageView photo;
    Bundle bundle;
    GalleryViewPager mViewPager;
    int current_item = 0;
    UrlPagerAdapter pagerAdapter;
    ArrayList<String> items;
    ArrayList<entities.Photo> photo_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_photo, null);
        this.setContentView(root);

        mViewPager = (GalleryViewPager) findViewById(R.id.viewer);
        bundle = this.getIntent().getExtras();
        photoId = bundle.getInt("photoId");
        userId = bundle.getInt("userId");
        namePhoto = bundle.getString("namePhoto");
        loadGallery();
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }

    @Override
    protected void onResume() {
        loadHeader();
        super.onResume();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        //now getIntent() should always return the last received intent
    }

    public void load() {

        Request.get("photos/getPhotoById/userId/" + userId + "/photoId/" + photoId, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    if (!cadena.equals("")) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        photo.setImageBitmap(bitmap);
                        photo.setAdjustViewBounds(true);
                        photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else
                        photo.setImageResource(R.mipmap.photo_default);


                } catch (Exception ex) {
                    photo.setImageResource(R.mipmap.photo_default);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                photo.setImageResource(R.mipmap.photo_default);
            }
        });

    }

    public void delete_onclick(View view) {
        final Dialog preloader4=Preloader.getInstance(Photo.this).build();
        preloader4.show();
        entities.Photo photo_current= photo_list.get(mViewPager.getCurrentItem());
        RequestParams params = new RequestParams();
        params.put("photoId", photo_current.getID_PHOTO());
        params.put("namePhoto", photo_current.getPathComplete());
        params.put("userId", photo_current.getID_USER());
        Request.post("photos/deletephoto", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    items.remove(mViewPager.getCurrentItem());
                    mViewPager.setAdapter(pagerAdapter);
                    mViewPager.setCurrentItem(0);

                } catch (Exception ex) {
                }
                preloader4.dismiss();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                try {
                    String cadena = new String(bytes, "UTF-8");

                } catch (Exception ex) {

                }
                preloader4.dismiss();
            }
        });
    }
    public void setPhotoAsProgifle_onclick(View view) {
        final Dialog preloader2=Preloader.getInstance(Photo.this).build();
        preloader2.show();
        entities.Photo photo_current= photo_list.get(mViewPager.getCurrentItem());
        RequestParams params = new RequestParams();
        params.put("photoId", photo_current.getID_PHOTO());
        params.put("ID_USER", photo_current.getID_USER());
        Request.post("Galleries/setphotoprofile", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    try {
                        String cadena = new String(bytes, "UTF-8");
                        if (!cadena.equals("")) {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            Bitmap bitmap = ImageUtils.decodeFile(bytes, 100);
                            ImageUtils.saveToCacheFile(bitmap);
                            loadHeader();
                        } else {
                        }
                    } catch (Exception ex) {
                    } catch (OutOfMemoryError ex) {
                    }

                } catch (Exception ex) {
                }
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                try {
                    String cadena = new String(bytes, "UTF-8");

                } catch (Exception ex) {

                }
                preloader2.dismiss();
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void uploadPhoto_onclick(View view) {
        UploadGallery dialog = new UploadGallery();
        dialog.setUserId(BaseHelper.getUserId(getApplicationContext()));
        dialog.show(getFragmentManager(), "uploader");
    }

    public void loadGallery() {
        final Dialog preloader1=Preloader.getInstance(Photo.this).build();
        preloader1.show();
        RequestParams params = new RequestParams();
        params.put("ID_USER", userId);
        Request.get("Galleries/findAllPhotos", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                items = new ArrayList<String>();
                photo_list = new ArrayList<entities.Photo>();
                int cont = 0;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        entities.Photo photo = new entities.Photo();
                        photo.setID_PHOTO(obj.getInt("ID_PHOTO"));
                        photo.setID_USER(obj.getInt("ID_USER"));
                        photo.setPathComplete(obj.getString("PathComplete"));
                        photo_list.add(photo);
                        items.add(Request.BASE_URL + "photos/getPhoto/userId/" + photo.getID_USER() + "/photoId/" + photo.getID_PHOTO());
                        if (photoId == photo.getID_PHOTO()) {
                            current_item = cont;
                        }
                        cont++;
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }
                }

                pagerAdapter = new UrlPagerAdapter(getApplicationContext(), items);
                pagerAdapter.setOnItemChangeListener(new BasePagerAdapter.OnItemChangeListener() {
                    @Override
                    public void onItemChange(int currentPosition) {
                    }
                });

                mViewPager = (GalleryViewPager) findViewById(R.id.viewer);
                mViewPager.setOffscreenPageLimit(3);
                mViewPager.setAdapter(pagerAdapter);
                mViewPager.setCurrentItem(current_item);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }
        });
    }

    @Override
    public void updateResult() {
        photoId=-1;
        current_item=items.size()+1;
        loadGallery();

    }
}
