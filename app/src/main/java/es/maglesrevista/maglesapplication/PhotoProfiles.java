package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.readystatesoftware.viewbadger.BadgeView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import entities.User;
import ru.truba.touchgallery.GalleryWidget.BasePagerAdapter;
import ru.truba.touchgallery.GalleryWidget.GalleryViewPager;
import ru.truba.touchgallery.GalleryWidget.UrlPagerAdapter;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.TextFont;


public class PhotoProfiles extends BaseBarActivity {

    int photoId=0;
    int userId=0;
    String namePhoto="";
    ImageView photo;
    Bundle bundle;
    ImageView icnFavorite,icnLocked,icnMugHug,icnReport;
    TextView txtFavorite,txtLocked,txtMugHug,txtReport,txtLastOnline;
    boolean isLocked=false,isFavorite=false,isMugHug=false;
    TextView txt_basic_information1,txt_basic_information2,txt_basic_information3,txt_basic_information4;
    TextView txv_description,txv_address;
    TextView txtname_profile,txtnumber_maghugs;
    String nicknameCurrent;
    GalleryViewPager mViewPager;
    int current_item=0;
    BadgeView badge;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_photo_profiles, null);
        this.setContentView(root);

        bundle = this.getIntent().getExtras();
        photoId=bundle.getInt("photoId");
        userId=bundle.getInt("userId");
        this.user=new User();
        namePhoto=bundle.getString("namePhoto");
        mViewPager = (GalleryViewPager)findViewById(R.id.viewer);
        txtnumber_maghugs=(TextView)root.findViewById(R.id.txtnumber_maghugs);
        icnFavorite=(ImageView)root.findViewById(R.id.icnFavorite);
        icnLocked=(ImageView)root.findViewById(R.id.icnLocked);
        icnMugHug=(ImageView)root.findViewById(R.id.icnMugHug);
        icnReport=(ImageView)root.findViewById(R.id.icnReport);

        txtFavorite=(TextView)root.findViewById(R.id.txtFavorite);
        txtLocked=(TextView)root.findViewById(R.id.txtLocked);
        txtMugHug=(TextView)root.findViewById(R.id.txtMugHug);
        txtReport=(TextView)root.findViewById(R.id.txtReport);
        View target = root.findViewById(R.id.icon_messages);
        badge = new BadgeView(this, target);
        badge.setBadgePosition(BadgeView.POSITION_CENTER);
        badge.setBadgeBackgroundColor(getResources().getColor(R.color.color_btn_magles));
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        txtnumber_maghugs.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));

    }
    @Override
    protected void onResume() {
        loadHeader();
        bundle=getIntent().getExtras();
        loadGallery();
        loadUserProfile(userId);
        loadMugHug(userId);
        loadMessagesNotReaded(this.userId);
        super.onResume();


    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    public void loadUserProfile(final int userId){
        final Dialog preloader6=Preloader.getInstance(PhotoProfiles.this).build();
        preloader6.show();
        RequestParams params=new RequestParams();
        params.put("user_id",BaseHelper.getUserId(getApplicationContext()));
        params.put("contact_id", userId);
        Request.post("users/getContactProfile", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    user.setID_USER(response.getInt("ID_USER"));
                    user.setName(response.getString("Name"));
                    user.setNickname(response.getString("Nickname"));
                    nicknameCurrent=response.getString("Nickname");
                    int favorite = response.getInt("IsFavorite");
                    int mughug = response.getInt("MagHug");
                    int locked = response.getInt("IsLocked");
                    int turstrelationship = response.getInt("IsReported");
                    if (favorite == 1) {
                        icnFavorite.setImageResource(R.mipmap.icn_favorite);
                        txtFavorite.setText("Favorita");
                        isFavorite = true;
                    } else {
                        icnFavorite.setImageResource(R.mipmap.icn_notfavorite);
                        txtFavorite.setText("Favorita");
                        isFavorite = false;
                    }
                    if (mughug == 1) {
                        icnMugHug.setImageResource(R.mipmap.icn_maghug);
                        txtMugHug.setText("MagHug");
                        isMugHug = true;
                    } else {
                        icnMugHug.setImageResource(R.mipmap.icn_notmaghug);
                        txtMugHug.setText("MagHug");
                        isMugHug = false;
                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }
                CURRENT_PROCESS++;
                preloader6.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                badge.hide();
                preloader6.dismiss();
            }
        });
    }
    public void sendfavorite_onclick(View v)
    {
        final Dialog preloader5=Preloader.getInstance(PhotoProfiles.this).build();
        preloader5.show();
        RequestParams params=new RequestParams();
        params.put("user_id", BaseHelper.getUserId(getApplicationContext()));
        params.put("user_id_favorite", this.userId);
        Request.post("RelationShips/saveFavorites/", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    int status = response.getInt("IsFavorite");
                    if (status == 1) {
                        icnFavorite.setImageResource(R.mipmap.icn_favorite);
                        icnFavorite.setEnabled(false);
                        isFavorite = true;
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo(nicknameCurrent + " fue agregada a tu lista de favoritas");
                    } else if (status == 0) {
                        icnFavorite.setImageResource(R.mipmap.icn_notfavorite);
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo(nicknameCurrent + " ya no esta en tu lista de favoritas");
                    } else {
                        icnFavorite.setImageResource(R.mipmap.icn_notfavorite);
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo(nicknameCurrent + " está bloqueada");
                    }

                } catch (Exception e) {
                    Preloader.getInstance(PhotoProfiles.this).dialogInfo("Error no se logró agregar a tu lista de favoritas");
                }
                preloader5.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader5.dismiss();
                Preloader.getInstance(PhotoProfiles.this).dialogInfo("Error no se logró agregar a tu lista de favoritas");
            }
        });

    }
    public void sendMagHugs_onclick(View view){
        final Dialog preloader4=Preloader.getInstance(PhotoProfiles.this).build();
        preloader4.show();
        RequestParams params=new RequestParams();
        params.put("user_id", BaseHelper.getUserId(getApplicationContext()));
        params.put("user_id_maghug", this.userId);
        Request.post("RelationShips/saveMaghug/", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    loadMugHug(userId);
                    int status = response.getInt("MagHug");
                    if (status == 1) {
                        icnMugHug.setImageResource(R.mipmap.icn_maghug);
                        isMugHug = true;
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo("Has enviado un MagHug a " + nicknameCurrent);
                    } else if (status == 0) {
                        icnMugHug.setImageResource(R.mipmap.icn_notmaghug);
                        isMugHug = false;
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo("Se ha retirado el MagHug a " + nicknameCurrent);
                    } else {
                        icnMugHug.setImageResource(R.mipmap.icn_notmaghug);
                        isMugHug = false;
                        Preloader.getInstance(PhotoProfiles.this).dialogInfo(nicknameCurrent + " está bloqueada");
                    }

                } catch (Exception e) {
                    Preloader.getInstance(PhotoProfiles.this).dialogInfo("Error no se logró enviar el MagHug");
                }
                preloader4.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                isMugHug = false;
                preloader4.dismiss();
                Preloader.getInstance(PhotoProfiles.this).dialogInfo("Error no se logró enviar el MagHug");
            }
        });

    }
    public void loadMugHug(int userId){
        final Dialog preloader3=Preloader.getInstance(PhotoProfiles.this).build();
        preloader3.show();
        RequestParams params=new RequestParams();
        params.put("user_id",userId);
        Request.post("RelationShips/findAllMagHugs", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.length() > 0) {
                        txtnumber_maghugs.setText(response.length() + "");
                    } else
                        txtnumber_maghugs.setText("0");
                } catch (Exception e) {
                    txtnumber_maghugs.setText("0");
                }
                CURRENT_PROCESS++;
                preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                txtnumber_maghugs.setText("0");
                CURRENT_PROCESS++;
                preloader3.dismiss();
            }
        });
    }
    public void loadMessagesNotReaded(int userId){
        final Dialog preloader2=Preloader.getInstance(PhotoProfiles.this).build();
        preloader2.show();
        RequestParams params=new RequestParams();
        params.put("id_user_contact", userId);
        params.put("nickname_owner",BaseHelper.getNickname(getApplicationContext()));
        Request.get("chats/messagesnotreadedbycontact/", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.length() > 0) {
                        badge.setText(response.length() + "");
                        badge.show();
                    } else
                        badge.hide();
                } catch (Exception e) {
                    badge.hide();
                }
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }
        });
    }
    public void comversations_onclick(View view ){
        Intent intent =new Intent(this, MainActivity.class);
        intent.putExtra("name", BaseHelper.getNickname(this));
        intent.putExtra("nicknameFriend", nicknameCurrent);
        startActivity(intent);
    }
    public void loadGallery(){
        final Dialog preloader1=Preloader.getInstance(PhotoProfiles.this).build();
        preloader1.show();
        RequestParams params=new RequestParams();
        params.put("ID_USER", userId);
        Request.get("Galleries/findAllPhotos", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<String> items = new ArrayList<String>();
                ArrayList<entities.Photo> photo_list = new ArrayList<entities.Photo>();
                int cont = 0;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        entities.Photo photo = new entities.Photo();
                        photo.setID_PHOTO(obj.getInt("ID_PHOTO"));
                        photo.setID_USER(obj.getInt("ID_USER"));
                        photo.setPathComplete(obj.getString("PathComplete"));
                        photo_list.add(photo);
                        items.add(Request.BASE_URL + "photos/getPhoto/userId/" + photo.getID_USER() + "/photoId/" + photo.getID_PHOTO());

                        if (photoId == photo.getID_PHOTO()) {
                            current_item = cont;
                        }
                        cont++;
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }
                }

                UrlPagerAdapter pagerAdapter = new UrlPagerAdapter(getApplicationContext(), items);
                pagerAdapter.setOnItemChangeListener(new BasePagerAdapter.OnItemChangeListener() {
                    @Override
                    public void onItemChange(int currentPosition) {
                    }
                });

                mViewPager = (GalleryViewPager) findViewById(R.id.viewer);
                mViewPager.setOffscreenPageLimit(3);
                mViewPager.setAdapter(pagerAdapter);
                mViewPager.setCurrentItem(current_item);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }
        });
    }
}
