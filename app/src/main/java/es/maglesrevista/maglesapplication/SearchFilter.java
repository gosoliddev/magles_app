package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.ActionBarActivity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ActionMenuView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
//import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import custom.ButtonSelect;
import entities.DataValues;
import entities.KeyPairValue;
import entities.SpinnerValue;
import entities.ValueSection;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;


import android.app.Activity;

public class SearchFilter extends BaseBarActivity {

    DataValues dataValues;
    ButtonSelect btn_country,btn_town,btn_city,btn_ageFrom,btn_ageTo,btn_heightFrom,btn_heightTo;
    ButtonSelect btn_intention,btn_ocupation,btn_withPicture,btn_Online,btn_freeTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_search_filter, null);
        this.setContentView(root);

        dataValues=new DataValues();
        LinearLayout row1=(LinearLayout)root.findViewById(R.id.row1);
        LinearLayout row2=(LinearLayout)root.findViewById(R.id.row2);
        LinearLayout row3=(LinearLayout)root.findViewById(R.id.row3);
//        LinearLayout row4=(LinearLayout)root.findViewById(R.id.row4);
        LinearLayout row5=(LinearLayout)root.findViewById(R.id.row5);
        LinearLayout row7=(LinearLayout)root.findViewById(R.id.row7);
        LinearLayout row8=(LinearLayout)root.findViewById(R.id.row8);
        LinearLayout row9=(LinearLayout)root.findViewById(R.id.row9);

        btn_country=(ButtonSelect)root.findViewById(R.id.btn_country);
        btn_country.configure();
        btn_country.setHint("");
        btn_town=(ButtonSelect)root.findViewById(R.id.btn_Town);
        btn_town.configure();
        btn_town.setHint("");
//        btn_city=(ButtonSelect)root.findViewById(R.id.btn_city);
        btn_ageFrom=(ButtonSelect)root.findViewById(R.id.btn_ageFrom);
        btn_ageFrom.setHint("");
        btn_ageTo=(ButtonSelect)root.findViewById(R.id.btn_ageTo);
        btn_ageTo.setHint("");
        btn_intention=(ButtonSelect)root.findViewById(R.id.btn_intention);
        btn_intention.setHint("");
        btn_heightFrom=(ButtonSelect)root.findViewById(R.id.btn_hegihtFrom);
        btn_heightFrom.setHint("");
        btn_heightTo=(ButtonSelect)root.findViewById(R.id.btn_hegihtTo);
        btn_heightTo.setHint("");
        btn_Online=(ButtonSelect)root.findViewById(R.id.btn_online);
        btn_Online.setHint("");
        btn_withPicture=(ButtonSelect)root.findViewById(R.id.btn_withPicture);
        btn_withPicture.setHint("");
        btn_freeTime=(ButtonSelect)root.findViewById(R.id.btn_freetime);
        btn_freeTime.setHint("");
        BaseHelper.applyAlpha(row1,0.8f);
        BaseHelper.applyAlpha(row2,0.8f);
        BaseHelper.applyAlpha(row3,0.8f);
//        BaseHelper.applyAlpha(row4,0.8f);
        BaseHelper.applyAlpha(row5,0.8f);
        BaseHelper.applyAlpha(row7,0.8f);
        BaseHelper.applyAlpha(row8,0.8f);
        BaseHelper.applyAlpha(row9,0.8f);

        BaseHelper.applyAlpha(btn_country,0.95f);
        BaseHelper.applyAlpha(btn_town,0.95f);
        BaseHelper.applyAlpha(btn_ageFrom,0.95f);
        BaseHelper.applyAlpha(btn_ageTo,0.95f);
        BaseHelper.applyAlpha(btn_intention,0.95f);
        BaseHelper.applyAlpha(btn_heightFrom,0.95f);
        BaseHelper.applyAlpha(btn_heightTo,0.95f);
        BaseHelper.applyAlpha(btn_Online,0.95f);
        BaseHelper.applyAlpha(btn_withPicture,0.95f);
        BaseHelper.applyAlpha(btn_freeTime, 0.95f);
        loadCountries();
        configure();

        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    @Override
    public void onResume(){
        loadHeader();
        super.onResume();
    }
     private void configure(){

         btn_ageFrom.setDataSource(dataValues.getEdad());
         btn_ageFrom.dataBind();
         btn_ageTo.setDataSource(dataValues.getEdad());
         btn_ageTo.dataBind();
         btn_Online.setDataSource(dataValues.getSiNo());
         btn_Online.dataBind();
         btn_withPicture.setDataSource(dataValues.getSiNo());
         btn_withPicture.dataBind();
         btn_freeTime.setDataSource(dataValues.getFreeTime());
         btn_freeTime.dataBind();
         btn_heightFrom.setDataSource(dataValues.getAltura());
         btn_heightFrom.dataBind();
         btn_heightTo.setDataSource(dataValues.getAltura());
         btn_heightTo.dataBind();
         btn_intention.setDataSource(dataValues.getIntecion());
         btn_intention.dataBind();
     }

      public void loadCountries() {
        final Dialog preloader1=Preloader.getInstance(SearchFilter.this).build();
        preloader1.show();
        Request.get("countries/allCountries", null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<KeyPairValue<Object,Object>> values_list=new ArrayList<KeyPairValue<Object, Object>>();
                for(int i=0;i<response.length();i++)
                {
                    try {
                        KeyPairValue<Object,Object> country=new KeyPairValue<Object, Object>();
                        JSONObject object=response.getJSONObject(i);
                        String code=object.getString("Code");
                        String name=object.getString("Name");
                        country.setKey(code);
                        country.setValue(name);
                        values_list.add(country);
                    }
                    catch (Exception e)
                    {
                        ;
                    }
                }
                values_list.add(new KeyPairValue<Object, Object>("", "Indiferente"));
                btn_country.setDataSource(values_list);
                btn_country.dataBind();
                btn_country.getLv().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        KeyPairValue<String, String> item = (KeyPairValue) parent.getItemAtPosition(position);
                        btn_country.setSelectItem(item);
                        btn_country.closeDialog();
                        loadPoblation(item.getKey(), "");

                    }
                });
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloader1.dismiss();

            }
        });
    }


    public void loadPoblation(String code_country, final String cityId) {
        if (!code_country.equals("")) {
            final Dialog preloader2=Preloader.getInstance(SearchFilter.this).build();
            preloader2.show();
            RequestParams params=new RequestParams();
            params.put("country_code", code_country);
            Request.get("cities/citiesCountry", params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    try {
                        ArrayList<KeyPairValue<Object,Object>> values_list=new ArrayList<KeyPairValue<Object, Object>>();
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                KeyPairValue<Object,Object> country=new KeyPairValue<Object, Object>();
                                JSONObject object=response.getJSONObject(i);
                                String code=object.getString("ID");
                                String name=object.getString("Name");
                                country.setKey(code);
                                country.setValue(name);
                                values_list.add(country);
                            }
                            catch (Exception e)
                            {
                                ;
                            }
                        }
                        values_list.add(new KeyPairValue<Object, Object>("", "Indiferente"));
                        btn_town.setDataSource(values_list);
                        btn_town.dataBind();
                        btn_town.setSelectItem(cityId);
                        btn_town.setHint("");
                    } catch (Exception ex) {

                    }
                    preloader2.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    preloader2.dismiss();
                }
            });
        }
        else
        {
            ArrayList<KeyPairValue<Object,Object>> values_list=new ArrayList<KeyPairValue<Object, Object>>();
            values_list.add(new KeyPairValue<Object, Object>("","Indiferente"));
            btn_town.setDataSource(values_list);
            btn_town.dataBind();
        }
    }
    public void search_onclick(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("YEARS_FROM", btn_ageFrom.getSelectItem()==null?"":btn_ageFrom.getSelectItem().getKey());
        bundle.putString("YEARS_TO", btn_ageTo.getSelectItem() == null ? "" : btn_ageTo.getSelectItem().getKey());
        bundle.putString("COUNTRY", btn_country.getSelectItem() == null ? "" : btn_country.getSelectItem().getKey());
        bundle.putString("STATE", btn_town.getSelectItem() == null ? "" : btn_town.getSelectItem().getKey());
//        bundle.putString("CITY", btn_city.getSelectItem() == null ? "" : btn_city.getSelectItem().getValue());
        bundle.putString("TALL_FROM", btn_heightFrom.getSelectItem() == null ? "" : btn_heightFrom.getSelectItem().getKey());
        bundle.putString("TALL_TO", btn_heightTo.getSelectItem() == null ? "" : btn_heightTo.getSelectItem().getKey());
        bundle.putString("INTENTION", btn_intention.getSelectItem() == null ? "" : btn_intention.getSelectItem().getKey());
        bundle.putString("FREE_TIME", btn_freeTime.getSelectItem()==null?"":btn_freeTime.getSelectItem().getKey());
        bundle.putString("WITH_PHOTO", btn_withPicture.getSelectItem() ==null?"":btn_withPicture.getSelectItem().getKey());
        bundle.putString("ONLINE", btn_Online.getSelectItem()==null?"":btn_Online.getSelectItem().getKey());
        Intent intent = new Intent(SearchFilter.this, SearchFilterResults.class);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

}
