package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.AdapterList;
import custom.WidgetResults;
import entities.Photo;
import utils.BaseHelper;
import utils.Preloader;
import utils.ScrollViewListener;
import custom.ObservableScrollView;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;


public class SearchFilterResults extends BaseBarActivity implements ScrollViewListener {

    Button btn_visible;
    Button btn_invisible;
    EditText txt_input;
    boolean IsInvisible;

    ObservableScrollView scrollView;
    TableLayout tableLayoutContent;
    Bitmap bitmap;
    Bitmap scaledBitmap;
    float dpHeightScreen = 0;
    float dpWidthScreen = 0;
    final float NUM_COLUMNS = 3;
    int threshold = 21;
    int idUser = 0;
    ObservableScrollView scrollOnDemand;
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=105;
    int TOTAL_ROWS=0;
    Bundle bundle;
    boolean isBusy;
    WidgetResults widgetResults;
    GridView grid;
    AdapterList adaper;
    ArrayList<Photo> photos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_search_filter_results, null);
        this.setContentView(root);
        session=new SessionManager(getApplicationContext());
        reloadActivity();
        bundle = this.getIntent().getExtras();

        btn_visible = (Button) findViewById(R.id.btn_visible);
        //btn_invisible = (Button) findViewById(R.id.btn_invisible);
        this.txt_input = (EditText) findViewById(R.id.txt_input);
        this.IsInvisible = false;
        this.idUser= BaseHelper.getUserId(getApplicationContext());
        this.loadUser();
        isBusy =false;
        txt_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchProfiles_onclick(v);
                    return true;
                }
                return false;
            }
        });
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        photos=new ArrayList<Photo>();
        adaper=new AdapterList(getApplicationContext(),photos);
        grid=(GridView)findViewById(R.id.grid);
        grid.setAdapter(adaper);
        grid.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
                if (firstVisibleItem == 0)
                    swipeContainer.setEnabled(true);
                else
                    swipeContainer.setEnabled(false);
                if (!isBusy && firstVisibleItem > 30 && (totalItemCount - visibleItemCount) <= (firstVisibleItem + (totalItemCount - 30))) {
                    // I load the next page of gigs using a background task,
                    // but you can call any function here.
                    isBusy = true;
                    getSearchResults(bundle);

                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub

            }
        });

        getSearchResults(bundle);

    }
    @Override
    public void onResume(){
        loadHeader();
        configure();
        loadUser();
        super.onResume();

    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                CURRENT_PAGE = 0;
                photos.clear();
                adaper.notifyDataSetChanged();
                getSearchResults(bundle);
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        CURRENT_PAGE = 0;
        bundle = intent.getExtras();
        photos.clear();
        adaper.notifyDataSetChanged();
        getSearchResults(bundle);
        super.onNewIntent(intent);
    }
    private void configure() {

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(btn_visible, 1.0f);
                //BaseHelper.applyAlpha(btn_invisible, 1.0f);
                break;
            }
            case "I":{
                BaseHelper.applyAlpha(btn_visible, 0.5f);
                //BaseHelper.applyAlpha(btn_invisible, 0.5f);

                break;
            }
            case "B":{
                BaseHelper.applyAlpha(btn_visible, 0.5f);
                //BaseHelper.applyAlpha(btn_invisible, 0.5f);
                break;
            }
        }
    }
    public void getSearchResults(Bundle bundle) {
            RequestParams params = new RequestParams();
            params.put("ID_USER", this.idUser);
            params.put("AgeFrom", bundle.getString("YEARS_FROM"));
            params.put("AgeTo", bundle.getString("YEARS_TO"));
            params.put("Country", bundle.getString("COUNTRY"));
            params.put("Province", bundle.getString("STATE"));
            params.put("TallFrom", bundle.getString("TALL_FROM"));
            params.put("TallTo", bundle.getString("TALL_TO"));
            params.put("Occupation", bundle.getString("OCUPATION"));
            params.put("Intention", bundle.getString("INTENTION"));
            params.put("FreeTime", bundle.getString("FREE_TIME"));
            params.put("WithPhotos", bundle.getString("WITH_PHOTO"));
            params.put("IsOnline", bundle.getString("ONLINE"));
            if(bundle.getString("nickname")!=null)
                params.put("nickname", bundle.getString("nickname"));
            params.put("page", CURRENT_PAGE);
            params.put("page_size", PAGE_SIZE);
            Request.post("SearchFilters/searchUsersSaveSearch", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {

                    try {
                        if(response.length()>0) {
                            SearchFilterResults.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (int index = 0; index < response.length(); index++) {
                                        try {
                                            JSONObject item = response.getJSONObject(index);
                                            Photo photo1 = new Photo();
                                            String path = item.getString("PathComplete") != "null" ? item.getString("PathComplete") : "";
                                            int photoId = item.getString("ID_PHOTO") != "null" ? Integer.valueOf(item.getInt("ID_PHOTO")) : 0;
                                            photo1.setID_PHOTO(photoId);
                                            photo1.setPathComplete(path);
                                            photo1.setID_USER(item.getInt("ID_USER"));
                                            photo1.setName(item.getString("Nickname"));
                                            TOTAL_ROWS = item.getInt("total_rows");
                                            TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                                            photos.add(photo1);
                                            adaper.notifyDataSetChanged();
                                        } catch (Exception e) {
                                        }
                                    }

                                }
                            });
                            CURRENT_PAGE++;
                        }
                        isBusy =false;
                    }
                    catch (Exception e)
                    {
                        isBusy =false;

                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    isBusy =false;
                    Preloader.getInstance(SearchFilterResults.this).dialogInfo("Error al cargar la lista de usuarias, inténtalo de nuevo en un rato.");

                }
            });
    }

    public void changeVisible_onclick(View v) {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                this.setVisibility();
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }

    public void searchProfiles_onclick(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        String value=txt_input.getText().toString();
        Bundle bundle=new Bundle();
        bundle.putString("nickname", value.trim());
        if(isBusy ==false && value.length()>0) {
            isBusy = true;
            CURRENT_PAGE = 0;
            photos.clear();
            getSearchResults(bundle);
        }
    }

    public void loadUser() {
        RequestParams params = new RequestParams();
        params.put("id", this.idUser);
        Request.get("users/user", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    IsInvisible = (response.getInt("IsInvisible") == 1) ? true : false;
                    setShowVisibility();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CURRENT_PROCESS++;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                Preloader.getInstance(SearchFilterResults.this).dialogInfo("Ha habido un error. Vuelve a internarlo de nuevo.");
            }
        });
    }

    public void setVisibility() {
        RequestParams params = new RequestParams();
        params.put("ID_USER", this.idUser);
        Request.post("users/changevisible", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    IsInvisible = response.getInt("IsInvisible")==1?true:false;
                    setShowVisibility();
                }catch (Exception e){}
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Preloader.getInstance(SearchFilterResults.this).dialogInfo("Ha habido un error. Vuelve a internarlo de nuevo.");
            }
        });
    }

    public void setShowVisibility() {
        btn_visible = (Button) root.findViewById(R.id.btn_visible);
        if ( IsInvisible) {
            btn_visible.setText(R.string.sf_btn_invisible);
            btn_visible.setBackgroundColor(Color.parseColor("#bf284a"));
            Drawable img = getApplicationContext().getResources().getDrawable( R.mipmap.icon_visible_not_24 );
            btn_visible.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
            session.setVisibility(false);

        }
        else {
            btn_visible.setText(R.string.sf_btn_visible);
            btn_visible.setBackgroundColor(Color.parseColor("#51C794"));
            Drawable img = getApplicationContext().getResources().getDrawable( R.mipmap.icon_visible_24 );
            btn_visible.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
            session.setVisibility(true);
        }

    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target= (LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);
        // if diff is zero, then the bottom has been reached

        if( diff == 0)
        {
            if(isBusy ==false) {
                isBusy =true;
                NUMBER_PROCESS=1;
                CURRENT_PROCESS=0;
                //runProcess(this);
                getSearchResults(bundle);
            }

        }
    }
}
