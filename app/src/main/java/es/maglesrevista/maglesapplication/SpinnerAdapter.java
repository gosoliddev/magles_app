package es.maglesrevista.maglesapplication;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import entities.SpinnerValue;
import entities.User;
import utils.TextFont;

/**
 * Created by Usuario on 10/07/2015.
 */
public class SpinnerAdapter extends ArrayAdapter<SpinnerValue> {

    private Context context;
    private SpinnerValue[] values;

    public SpinnerAdapter(Context context, int textViewResourceId, SpinnerValue[] values) {
        super(context, textViewResourceId, values);

        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.length;
    }

    public SpinnerValue getItem(int position){
        return values[position];
    }

    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return this.getViewStyle(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return this.getViewStyle(position, convertView, parent);
    }
    private View getViewStyle(int position, View convertView, ViewGroup parent){

        View vi=convertView;
        TextView nombre;
        ImageView image;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.custom_spinner, null);
        }
        SpinnerValue spinner_value=values[position];
        nombre= (TextView) vi.findViewById(R.id.text_value);
        //image=(ImageView)vi.findViewById(R.id.imagennegocio);
        nombre.setText(spinner_value.getText());
//
        return vi;
    }
}
