package es.maglesrevista.maglesapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import entities.User;

public class UserAdapter extends BaseAdapter
{
    protected Activity activity;
    protected ArrayList<User> lista_user;
    //int [] ciudades;
    public UserAdapter(Activity activity, ArrayList<User> users){
        this.activity = activity;
        this.lista_user = users;
    }


    @Override
    public int getCount() {
        return lista_user.size();
    }


    @Override
    public Object getItem(int position) {
        return lista_user.get(position);
    }


    @Override
    public long getItemId(int position) {
        return lista_user.get(position).getID_USER();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        TextView nombre;
        ImageView image;
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.right_nav_item, null);
        }
        User aux_user = lista_user.get(position);
        nombre = (TextView) vi.findViewById(R.id.nickname_userchat);
        nombre.setText(aux_user.getNickname());
        return vi;
    }
}