package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.ObservableScrollView;
import custom.WidgetBlocksUser;
import entities.Photo;
import entities.User;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;


public class UsersBlocked extends BaseBarActivity implements ScrollViewListener {

    WidgetBlocksUser widget_usersblocked;
    int userId;
    ObservableScrollView  scrollOnDemand;
    int CURRENT_PAGE=0;
    int TOTAL_PAGES=0;
    int PAGE_SIZE=15;
    int TOTAL_ROWS=0;
    Boolean isBusy =false;
    SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=1;
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_blocked_users, null);
        this.setContentView(root);

        session=new SessionManager(getApplicationContext());
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        swipeContainer=(SwipeRefreshLayout)root.findViewById(R.id.swipeContainer);
        reloadActivity();
        widget_usersblocked=(WidgetBlocksUser)root.findViewById(R.id.list_blocked);
        this.userId= BaseHelper.getUserId(getApplicationContext());
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
    }
    @Override
    protected void onResume() {
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=1;
        CURRENT_PAGE=0;
        isBusy =false;
        widget_usersblocked.clear();
        loadHeader();
        loadBlocked();
        super.onResume();
    }
    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);
        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);

        // if diff is zero, then the bottom has been reached
        if( diff == 0)
        {
            if(isBusy ==false) {
                isBusy =true;
                NUMBER_PROCESS=1;
                CURRENT_PROCESS=0;
                loadBlocked();
            }
        }
    }
    public void loadBlocked(){
        final Dialog preloader1= Preloader.getInstance(UsersBlocked.this).build();
        preloader1.show();
        RequestParams params=new RequestParams();
        params.put("user_id",this.userId);
        params.put("page_size",this.PAGE_SIZE);
        params.put("page",this.CURRENT_PAGE);
        Request.post("RelationShips/findAllLocked", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<Photo> photo_list = new ArrayList<Photo>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        Photo photo = new Photo();
                        photo.setID_USER(obj.getInt("ID_USER"));
                        photo.setPathComplete(obj.getString("PathComplete"));
                        photo.setName(obj.getString("Nickname"));
                        TOTAL_ROWS = obj.getInt("total_rows");
                        TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                        photo_list.add(photo);
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }
                }
                widget_usersblocked.setDataSource(photo_list);
                widget_usersblocked.setColumns(3);
                widget_usersblocked.dataBind();
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy = false;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy = false;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                isBusy = false;
                CURRENT_PAGE++;
                preloader1.dismiss();
            }
        });
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                NUMBER_PROCESS = 1;
                CURRENT_PROCESS = 0;
                CURRENT_PAGE = 0;
                widget_usersblocked.clear();
                loadBlocked();
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
}
