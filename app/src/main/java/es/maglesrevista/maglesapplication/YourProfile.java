package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import custom.ObservableScrollView;
import custom.UploadGallery;
import custom.WidgetGallery;
import entities.Photo;
import entities.User;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;


public class YourProfile extends BaseBarActivity implements ScrollViewListener, UploadGallery.IUploadGallery {

    SessionManager session;
    TextView txtname_profile,txv_visibility;
    ImageView photo_yourprofile,imv_visibility,image_file;
    boolean IsInvisible;
    int idUser = 0;
    WidgetGallery widget_gallery;
    TextView txt_maghug;
    ObservableScrollView scrollOnDemand;
    TextView txv_description,txt_basic_information1,txt_basic_information2,txt_basic_information3,txt_basic_information4,txt_basic_information5;
    LinearLayout icn_perfil1,icn_perfil2,icn_perfil3,icn_perfil4,icn_perfil5,icn_perfil6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=2;
        this.idUser=BaseHelper.getUserId(getApplicationContext());
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_your_profile, null);
        this.setContentView(root);
        icn_perfil1=(LinearLayout)root.findViewById(R.id.icn_perfil1);
        icn_perfil2=(LinearLayout)root.findViewById(R.id.icn_perfil2);
        icn_perfil3=(LinearLayout)root.findViewById(R.id.icn_perfil3);
        icn_perfil4=(LinearLayout)root.findViewById(R.id.icn_perfil4);
        icn_perfil5=(LinearLayout)root.findViewById(R.id.icn_perfil5);

        session=new SessionManager(getApplicationContext());
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        txtname_profile=(TextView)findViewById(R.id.txtname_profile);
        txv_description=(TextView)findViewById(R.id.txv_description);
        txt_basic_information1=(TextView)findViewById(R.id.txt_basic_information1);
        txt_basic_information2=(TextView)findViewById(R.id.txt_basic_information2);
        txt_basic_information3=(TextView)findViewById(R.id.txt_basic_information3);
        txt_basic_information4=(TextView)findViewById(R.id.txt_basic_information4);
        txt_basic_information5=(TextView)findViewById(R.id.txt_basic_information5);
        this.imv_visibility = (ImageView) findViewById(R.id.imv_visibility);
        this.txv_visibility=(TextView) findViewById(R.id.txv_visibility);
        this.photo_yourprofile=(ImageView)findViewById(R.id.photo_yourprofile);
        widget_gallery=(WidgetGallery)root.findViewById(R.id.list_gallery);

        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/Lobster 1.4.otf");
        txtname_profile.setTypeface(type);
        txt_maghug=(TextView)root.findViewById(R.id.txt_maghug);
        txt_maghug.setTypeface(Typeface.DEFAULT_BOLD);

    }
    private void configure() {

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(icn_perfil1, 1.0f);
                BaseHelper.applyAlpha(icn_perfil2, 1.0f);
                BaseHelper.applyAlpha(icn_perfil3, 1.0f);
                BaseHelper.applyAlpha(icn_perfil4, 1.0f);
                break;
            }
            case "I":{
                BaseHelper.applyAlpha(icn_perfil4, 0.5f);
                BaseHelper.applyAlpha(icn_perfil1, 0.5f);
                BaseHelper.applyAlpha(icn_perfil2, 1.0f);
                BaseHelper.applyAlpha(icn_perfil3, 1.0f);
                break;
            }
            case "B":{
                BaseHelper.applyAlpha(icn_perfil1, 0.5f);
                BaseHelper.applyAlpha(icn_perfil2, 0.5f);
                BaseHelper.applyAlpha(icn_perfil3, 0.5f);
                BaseHelper.applyAlpha(icn_perfil4, 0.5f);
                break;
            }
        }
    }
    public void  loadPhotoHeaderProfile(){
        Bitmap bmp= ImageUtils.loadFromCacheFile();
        if(bmp!=null) {

            photo_yourprofile.setImageBitmap(bmp);
            imghead_profile.setImageBitmap(bmp);

        }
        else {
            photo_yourprofile.setImageResource(R.mipmap.photo_default);
            imghead_profile.setImageResource(R.mipmap.photo_default);
        }
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached

        if( diff == 0)
        {


        }
    }
    public void loadUserProfile(int userId){
        final Dialog preloader1=Preloader.getInstance(YourProfile.this).build();
        preloader1.show();
        Request.get("users/userProfile/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                User user = new User();
                try {
                    txtname_profile.setText(response.getString("Nickname"));
                    if(!response.getString("Description").isEmpty())
                        txv_description.setText(response.getString("Description"));
                    else
                        txv_description.setVisibility(View.GONE);

                    IsInvisible=response.getString("IsInvisible").equals("1")?true:false;
                    setShowVisibility();
                    txt_basic_information5.setText(response.getString("NameCity").isEmpty()?"":response.getString("NameCity"));
                    txt_basic_information1.setText(response.getString("Edad"));
                    txt_basic_information3.setText(response.getString("Horoscope").isEmpty()?"":response.getString("Horoscope"));
                    txt_basic_information4.setText(response.getString("Value").isEmpty()?"":response.getString("Value"));
                    double stature = response.getDouble("Stature");
                    if(stature>0)
                        txt_basic_information2.setText(stature + " m.");
                    else
                        txt_basic_information2.setText("");


                    if(response.getString("Edad").isEmpty() || response.getString("Horoscope").isEmpty() || response.getString("Value").isEmpty() ||
                            response.getString("NameCity").isEmpty())
                    {
                        Preloader.getInstance(YourProfile.this).dialogInfo("Debes completar tu información personal desde\nConfiguración > Tu Perfil ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloader1.dismiss();
            }
        });
    }
    public void loadGallery(){

        RequestParams params=new RequestParams();
        params.put("ID_USER",this.idUser);
        Request.get("Galleries/findAllPhotos", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<Photo> photo_list = new ArrayList<Photo>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        Photo photo = new Photo();
                        photo.setID_PHOTO(obj.getInt("ID_PHOTO"));
                        photo.setID_USER(obj.getInt("ID_USER"));
                        photo.setPathComplete(obj.getString("PathComplete"));
                        photo_list.add(photo);
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }
                }
                widget_gallery.clear();
                widget_gallery.setDataSource(photo_list);
                widget_gallery.setColumns(3);
                widget_gallery.dataBind();
                CURRENT_PROCESS++;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
            }
        });
    }
    public void loadMugHug(int userId){
        final Dialog preloader2=Preloader.getInstance(YourProfile.this).build();
        preloader2.show();
        RequestParams params=new RequestParams();
        params.put("user_id", userId);
        Request.post("RelationShips/findAllMagHugs", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.length() > 0) {
                        txt_maghug.setText(response.length() + "");
                    } else
                        txt_maghug.setText("0");
                } catch (Exception e) {
                    txt_maghug.setText("0");
                }
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                txt_maghug.setText("0");
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }
        });
    }
    public void config_onclick(View view)
    {
        Intent intent=new Intent(YourProfile.this,conf_profile.class);
        intent.putExtra("ID_USER", BaseHelper.getUserId(getApplicationContext()));
        startActivity(intent);
    }
    public void mughugs_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), maghugs.class);
                startActivity(intent);
                break;
            }
            case "I":{
//                Intent intent=new Intent(getApplicationContext(), maghugs.class);
//                startActivity(intent);
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }
    public void changeVisible_onclick(View v) {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                this.setVisibility();
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                //Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }

    public void setVisibility() {
        final Dialog preloader3=Preloader.getInstance(YourProfile.this).build();
        preloader3.show();
        RequestParams params = new RequestParams();
        params.put("ID_USER", this.idUser);
        Request.post("users/changevisible", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if(IsInvisible )
                    IsInvisible = false;
                else
                    IsInvisible = true;
                setShowVisibility();
                preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Preloader.getInstance(YourProfile.this).dialogInfo("Hubo un problema en el cambio de visibilidad vuelva a intentarlo.");
                preloader3.dismiss();
            }
        });
    }
    public void setShowVisibility() {
        if (this.IsInvisible) {
            this.txv_visibility.setText(R.string.cp_invisible);
            this.imv_visibility.setImageResource(R.mipmap.icon_visible_not);
            session.setVisibility(false);
        }
        else {
            this.txv_visibility.setText(R.string.cp_visible);
            this.imv_visibility.setImageResource(R.mipmap.icon_visible);
            session.setVisibility(true);
        }
    }
    public void usersblocked_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(YourProfile.this,UsersBlocked.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(YourProfile.this,UsersBlocked.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void openGallery_onclick(View view){
        UploadGallery dialog = new UploadGallery();
        dialog.setUserId(BaseHelper.getUserId(getApplicationContext()));
        dialog.show(getFragmentManager(), "uploader");
    }
    @Override
    protected void onResume() {
        loadHeader();
        configure();
        loadPhotoHeaderProfile();
        loadUserProfile(this.idUser);
        loadGallery();
        loadMugHug(this.idUser);
        super.onResume();
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

    }

    @Override
    public void updateResult() {
        loadGallery();
    }
}

