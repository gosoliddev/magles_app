package es.maglesrevista.maglesapplication;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import custom.ObservableScrollView;
import custom.UploadProfile;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;

public class conf_profile extends BaseBarActivity implements ScrollViewListener, UploadProfile.IUploadProfile {

    int idUser = 0;
    boolean IsInvisible;

    ImageView imv_visibility;
    TextView txv_visibility;
    TextView txv_description;
    TextView txt_basic_information1,txt_basic_information2,txt_basic_information3,txt_basic_information4,txv_address;
    static ImageView photo_yourprofile;
    ObservableScrollView scrollOnDemand;
    Dialog dialog;
    TextView texto;
    EditText txt_input;
    String codeConutry="";
    LinearLayout icn_conf1,icn_conf2,icn_conf3,icn_conf4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_conf_profile, null);
        this.setContentView(root);
        NUMBER_PROCESS=1;
        CURRENT_PROCESS=0;
        Bundle bundle=getIntent().getExtras();
        if(bundle.getInt("ID_USER")!=0) {
            this.idUser = bundle.getInt("ID_USER");
        }
        session=new SessionManager(getApplicationContext());
        icn_conf1=(LinearLayout)root.findViewById(R.id.icn_conf1);
        icn_conf2=(LinearLayout)root.findViewById(R.id.icn_conf2);
        icn_conf3=(LinearLayout)root.findViewById(R.id.icn_conf3);
        icn_conf4=(LinearLayout)root.findViewById(R.id.icn_conf4);
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        scrollOnDemand.setScrollViewListener(this);
        this.imv_visibility = (ImageView) findViewById(R.id.imv_visibility);
        this.txv_visibility = (TextView) findViewById(R.id.txv_visibility);
        this.txv_description = (TextView) findViewById(R.id.txv_description);
        this.txt_basic_information1 = (TextView) findViewById(R.id.txt_basic_information1);
        this.txt_basic_information2 = (TextView) findViewById(R.id.txt_basic_information2);
        this.txt_basic_information3 = (TextView) findViewById(R.id.txt_basic_information3);
        this.txt_basic_information4 =(TextView) findViewById(R.id.txt_basic_information4);
        this.txv_address =(TextView) findViewById(R.id.txv_address);

        this.IsInvisible = false;

        photo_yourprofile=(ImageView)root.findViewById(R.id.photoProfile);

        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        TextView txtEditar2 = (TextView)findViewById(R.id.editar2);
        TextView txtEditar3 = (TextView)findViewById(R.id.editar3);
        TextView txtEditar4 = (TextView)findViewById(R.id.editar4);
        TextView txtEditar5 = (TextView)findViewById(R.id.editar5);
        SpannableString content = new SpannableString(txtEditar2.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtEditar2.setText(content);
        content = new SpannableString(txtEditar3.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtEditar3.setText(content);
        content =new SpannableString(txtEditar4.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtEditar4.setText(content);
        content =new SpannableString(txtEditar5.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtEditar5.setText(content);
    }


    private void configure() {

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(icn_conf1, 1.0f);
                BaseHelper.applyAlpha(icn_conf2, 1.0f);
                BaseHelper.applyAlpha(icn_conf3, 1.0f);
                BaseHelper.applyAlpha(icn_conf4, 1.0f);
                break;
            }
            case "I":{
                BaseHelper.applyAlpha(icn_conf4, 0.5f);
                BaseHelper.applyAlpha(icn_conf1, 1.0f);
                BaseHelper.applyAlpha(icn_conf2, 1.0f);
                BaseHelper.applyAlpha(icn_conf3, 1.0f);
                break;
            }
            case "B":{
                BaseHelper.applyAlpha(icn_conf2, 0.5f);
                BaseHelper.applyAlpha(icn_conf3, 0.5f);
                BaseHelper.applyAlpha(icn_conf4, 0.5f);
                break;
            }
        }
    }
    @Override
    protected void onResume() {
        try {

            Bundle bundle = getIntent().getExtras();
            if (bundle.getInt("ID_USER") != 0) {
                this.idUser = bundle.getInt("ID_USER");
            }
            loadHeader();
            configure();
            loadPhotoHeaderProfile();
            this.loadUser();
            super.onResume();
        }
        catch (Exception e)
        {}
    }


    public void loadUser() {
        final Dialog preloader1=Preloader.getInstance(conf_profile.this).build();
        preloader1.show();
        RequestParams params = new RequestParams();
        params.put("id", this.idUser);
        Request.get("users/userProfile", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getString("Description").isEmpty() || response.getString("Description").equals("\n")) {
                        txv_description.setText(R.string.cp_descripcion_default);
                    }
                    else
                    {
                        txv_description.setText(response.getString("Description"));
                    }
                    txt_basic_information1.setText(response.getString("Edad"));
                    double stature=response.getDouble("Stature");
                    txt_basic_information2.setText(stature+" m.");
                    txt_basic_information3.setText(response.getString("Horoscope").isEmpty()?"":response.getString("Horoscope"));
                    txt_basic_information4.setText(response.getString("Value").isEmpty()?"":response.getString("Value"));
                    txv_address.setText(response.getString("NameCity").isEmpty()?"":response.getString("NameCity"));
                    codeConutry = response.getString("CODE_COUNTRY");
                    IsInvisible = (response.getInt("IsInvisible") == 1) ? true : false;
                    setShowVisibility();
                } catch (JSONException e) {
                    e.printStackTrace();

                }
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }
        });
    }

    public void changeBasicInformation_onclick(View v) {}

    public void changeDescription_onclick(View v) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_prompt);
        TextView txtTitle=(TextView)dialog.findViewById(R.id.title_dialog);
        texto=(TextView)dialog.findViewById(R.id.text_dialog);
        txt_input=(EditText)dialog.findViewById(R.id.txt_input);
        if(txv_description.getText().toString().equals(getString(R.string.cp_descripcion_default))) {
            txt_input.setHint(R.string.cp_descripcion_default);
            txt_input.setText("");
        }
        else
        {
            txt_input.setHint(R.string.cp_descripcion_default);
            txt_input.setText(txv_description.getText().toString());
        }
        txt_input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
        //txt_input.setText(txv_description.getText().toString());
        TextView txt_ok= (TextView)dialog.findViewById(R.id.txt_si);
        TextView txt_no = (TextView)dialog.findViewById(R.id.txt_no);
        txtTitle.setText("");
        txtTitle.setHeight(0);
        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                txv_description.requestFocus();
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txv_description.requestFocus();
                saveDescription(txt_input.getText().toString(), BaseHelper.getUserId(getApplicationContext()));
                if(txt_input.getText().toString().equals("") || txt_input.getText().toString().equals("\n") ){
                    txv_description.setText(R.string.cp_descripcion_default);
                }
                else
                {
                    txv_description.setText(txt_input.getText().toString());
                }
                dialog.cancel();
            }
        });
        txt_input.addTextChangedListener(tw);
        dialog.show();
    }

    private void saveDescription(final String description,int userId) {
        RequestParams params = new RequestParams();
        params.put("user_id", userId);
        params.put("description", description);
        Request.post("userdetails/saveDescription", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (!description.equals("")) {
                    txv_description.setText(description);
                } else {
                    txv_description.setText(R.string.cp_descripcion_default);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    public void accountProfile_onclick(View v) {
        Runtime.getRuntime().freeMemory();
        try {
            Intent intent = new Intent(conf_profile.this, CreateAccountProfile.class);
            intent.putExtra("new_user_id", BaseHelper.getUserId(getApplicationContext()));
            intent.putExtra("view", "config");
            startActivity(intent);
        }
        catch (Exception e)
        {
            Log.d("Magles",e.getMessage());
        }
    }

    public void accountPreferences_onclick(View v) {
        Intent intent = new Intent(conf_profile.this, CreateAccountPreferences.class);
        intent.putExtra("new_user_id", BaseHelper.getUserId(getApplicationContext()));
        intent.putExtra("codeConutry",codeConutry);
        intent.putExtra("view", "config");
        startActivity(intent);
    }
    public void accountEdit_onclick(View view){
        Intent intent = new Intent(getApplicationContext(), AccountEdit.class);
        startActivity(intent);
    }
    public void changeVisible_onclick(View v) {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                this.setVisibility();
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }

    public void setVisibility() {
        RequestParams params = new RequestParams();
        params.put("ID_USER", this.idUser);
        Request.post("users/changevisible", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                IsInvisible = !IsInvisible;
                setShowVisibility();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(), "Erro al cargaer lista de usuarios", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setShowVisibility() {
        if (this.IsInvisible) {
            this.txv_visibility.setText(R.string.cp_invisible);
            this.imv_visibility.setImageResource(R.mipmap.icon_visible_not);
            session.setVisibility(false);
        }
        else {
            this.txv_visibility.setText(R.string.cp_visible);
            this.imv_visibility.setImageResource(R.mipmap.icon_visible);
            session.setVisibility(true);
        }
        icn_conf4.requestLayout();
    }
    public void favorites_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(this,Favorite.class);
                intent.putExtra("ID_USER", BaseHelper.getUserId(getApplicationContext()));
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(this,Favorite.class);
                intent.putExtra("ID_USER", BaseHelper.getUserId(getApplicationContext()));
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void usersblocked_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(conf_profile.this,UsersBlocked.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(conf_profile.this,UsersBlocked.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void gallery_onclick(View view)
    {
        Intent intent=new Intent(conf_profile.this,Gallery.class);
        startActivity(intent);
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void changePhoto_onclick(View view){
        UploadProfile dialog = new UploadProfile();
        dialog.setUserId(this.idUser);
        dialog.setType_upload(dialog.UPLOAD_CONFIG);
        dialog.show(getFragmentManager(), "uploader");
    }
    public static void setPhoto(Bitmap bmp)
    {
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached

        if( diff == 0)
        {


        }
    }
    private TextWatcher tw = new TextWatcher() {
        public void afterTextChanged(Editable s){
            int number=s.length();
            texto.setText((100-number) +"/"+100);

        }
        public void  beforeTextChanged(CharSequence s, int start, int count, int after){
            // you can check for enter key here

        }
        public void  onTextChanged (CharSequence s, int start, int before,int count) {

        }
    };
    public void  loadPhotoHeaderProfile(){
        Bitmap bmp= ImageUtils.loadFromCacheFile();
        if(bmp!=null) {

            photo_yourprofile.setImageBitmap(bmp);
            imghead_profile.setImageBitmap(bmp);

        }
        else
            photo_yourprofile.setImageResource(R.mipmap.photo_default);
    }
    public void loadPhotoProfile(int userId){
        final Dialog preloader2=Preloader.getInstance(conf_profile.this).build();
        preloader2.show();
        Request.get("photos/getPhotoProfile/userId/" + userId, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    if (!cadena.equals("")) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = ImageUtils.decodeFile(bytes, 150);
                        ImageUtils.saveToCacheFile(bitmap);
                    }
                } catch (Exception ex) {
                    imghead_profile.setImageResource(R.mipmap.photo_default);
                }
                loadPhotoHeaderProfile();
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

                CURRENT_PROCESS++;
                preloader2.dismiss();
            }
        });
    }
    @Override
    public void updateResult() {
        loadPhotoProfile(idUser);
    }
}
