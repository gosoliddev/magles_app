package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import utils.BaseHelper;
import utils.DateUtils;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;
import utils.TextFont;


public  class home extends BaseBarActivity {

    SessionManager session;
    ImageView  photoProfile;
    TextView txt_messageNotReaded;
    int id_user;
    TextView txtVisibility;
    LinearLayout pnlaction_1,pnlaction_2,pnlaction_3,pnlaction_4,pnlaction_5,pnlaction_6;
    LinearLayout panel_premium,panel_nopremium;
    TextView  txt_invitation1;
    TextView  txt_premium01,txt_premium02,txt_premium03,txt_premium2,txt_premium3;
    Dialog preloader=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
         root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_home, null);
        this.setContentView(root);
        photoProfile=(ImageView) root.findViewById(R.id.imgProfile);
        txt_messageNotReaded=(TextView)root.findViewById(R.id.txt_messageNotReaded);
        session=new SessionManager(getApplicationContext());
        this.id_user=Integer.valueOf(session.getUserDetails().get("ID_USER"));
        txtVisibility=(TextView)root.findViewById(R.id.txtVisibility);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);

        pnlaction_1=(LinearLayout)root.findViewById(R.id.pnlaction_1);
        pnlaction_2=(LinearLayout)root.findViewById(R.id.pnlaction_2);
        pnlaction_3=(LinearLayout)root.findViewById(R.id.pnlaction_3);
        pnlaction_4=(LinearLayout)root.findViewById(R.id.pnlaction_4);
        pnlaction_5=(LinearLayout)root.findViewById(R.id.pnlaction_5);
        pnlaction_6=(LinearLayout)root.findViewById(R.id.pnlaction_6);
        pnlaction_6=(LinearLayout)root.findViewById(R.id.pnlaction_6);
        panel_premium=(LinearLayout)root.findViewById(R.id.panel_premium);
        panel_nopremium=(LinearLayout)root.findViewById(R.id.panel_nopremium);

        TextView  txt_perfil=(TextView)root.findViewById(R.id.txt_perfil);
        TextView  txt_message1=(TextView)root.findViewById(R.id.txt_message1);
        TextView  txt_search=(TextView)root.findViewById(R.id.txt_search);
        TextView  txt_chat=(TextView)root.findViewById(R.id.txt_chat);
        TextView  txt_match=(TextView)root.findViewById(R.id.txt_match);
        TextView  txt_noticies=(TextView)root.findViewById(R.id.txt_noticies);
        txt_invitation1=(TextView)root.findViewById(R.id.txt_invitation1);
        TextView  txt_invitation2=(TextView)root.findViewById(R.id.txt_invitation2);
        TextView  txt_maghug=(TextView)root.findViewById(R.id.txt_maghug);
        txt_premium01=(TextView)root.findViewById(R.id.txt_premium01);
        txt_premium02=(TextView)root.findViewById(R.id.txt_premium02);
        //txt_premium03=(TextView)root.findViewById(R.id.txt_premium03);
        txt_premium2=(TextView)root.findViewById(R.id.txt_premium2);
        txt_premium3=(TextView)root.findViewById(R.id.txt_premium3);


        txt_perfil.setTypeface(Typeface.DEFAULT_BOLD);
        txt_message1.setTypeface(Typeface.DEFAULT_BOLD);
        txt_search.setTypeface(Typeface.DEFAULT_BOLD);
        txt_chat.setTypeface(Typeface.DEFAULT_BOLD);
        txt_match.setTypeface(Typeface.DEFAULT_BOLD);
        txt_noticies.setTypeface(Typeface.DEFAULT_BOLD);
        txt_invitation1.setTypeface(Typeface.DEFAULT_BOLD);
        txt_invitation2.setTypeface(Typeface.DEFAULT_BOLD);
        txt_maghug.setTypeface(Typeface.DEFAULT_BOLD);
        txt_premium01.setTypeface(Typeface.DEFAULT_BOLD);
        txt_premium02.setTypeface(Typeface.DEFAULT_BOLD);
        //txt_premium03.setTypeface(Typeface.DEFAULT_BOLD);
        txt_premium2.setTypeface(Typeface.DEFAULT_BOLD);
        txt_premium3.setTypeface(Typeface.DEFAULT_BOLD);
        txt_messageNotReaded.setTypeface(Typeface.DEFAULT_BOLD);

    }
    public void configure()
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(pnlaction_1, 1.0f);
                BaseHelper.applyAlpha(pnlaction_2, 1.0f);
                BaseHelper.applyAlpha(pnlaction_3, 1.0f);
                BaseHelper.applyAlpha(pnlaction_4, 1.0f);
                BaseHelper.applyAlpha(pnlaction_5, 1.0f);
                BaseHelper.applyAlpha(pnlaction_6, 1.0f);
                break;
            }
            case "I":{
                BaseHelper.applyAlpha(pnlaction_1, 1.0f);
                BaseHelper.applyAlpha(pnlaction_2, 1.0f);
                BaseHelper.applyAlpha(pnlaction_3, 1.0f);
                BaseHelper.applyAlpha(pnlaction_4, 1.0f);
                BaseHelper.applyAlpha(pnlaction_5, 0.5f);
                BaseHelper.applyAlpha(pnlaction_6, 0.5f);
                break;
            }
            case "B":{
                BaseHelper.applyAlpha(pnlaction_1, 0.5f);
                BaseHelper.applyAlpha(pnlaction_2, 0.5f);
                BaseHelper.applyAlpha(pnlaction_3, 0.5f);
                BaseHelper.applyAlpha(pnlaction_4, 0.5f);
                BaseHelper.applyAlpha(pnlaction_5, 0.5f);
                BaseHelper.applyAlpha(pnlaction_6, 0.5f);
                break;
            }
        }
    }
    @Override
    protected void onResume() {
        load();
        configure();
        configureMenu();
        super.onResume();

    }
    @Override
    protected void onNewIntent(Intent intent) {
        load();
        setIntent(intent);
        super.onNewIntent(intent);
    }
    public void load(){
        loadPhotoProfile(id_user);
        loadMessagesNotReaded();
        loadUserProfile(id_user);
    }
    public void loadUserProfile(int userId){
       final Dialog preloader1=Preloader.getInstance(home.this).build();
        preloader1.show();
        Request.get("users/userProfile/id/" + userId, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    String visible = response.getString("IsInvisible");
                    String number_invitations = String.valueOf(response.getInt("Invitations"));
                    session.setStatus(response.getString("Status"));
                    if (visible.equals("0")) {
                        txtVisibility.setText(R.string.cp_visible);
                        session.setVisibility(true);

                    } else {
                        txtVisibility.setText(R.string.cp_invisible);
                        session.setVisibility(false);
                    }
                    txt_invitation1.setText(number_invitations);
                    switch (response.getString("Status"))
                    {
                        case "P":{
                            panel_nopremium.setVisibility(View.GONE);
                            panel_premium.setVisibility(View.VISIBLE);
                            SimpleDateFormat formatter2 = new SimpleDateFormat("dd|MM|yyyy");
                            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            try {
                                Date date =formatter1.parse(response.getString("dateTo"));
                                String date_string=formatter2.format(date);
                                txt_premium3.setText(date_string);
                                if(DateUtils.compareDates(Calendar.getInstance().getTime(),date)>0)
                                {
                                    final Dialog dialog1 = new Dialog(home.this);
                                    dialog1.setCancelable(false);
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.setContentView(R.layout.dialog_premium_success);
                                    TextView texto1=(TextView)dialog1.findViewById(R.id.text_dialog1);
                                    texto1.setTypeface(TextFont.getInstance().getFont(home.this,TextFont.PATH_FONT_BOLD));
                                    texto1.setTextSize(18);
                                    TextView texto2=(TextView)dialog1.findViewById(R.id.text_dialog2);
                                    texto2.setVisibility(View.GONE);
                                    String text1="",text2="";
                                    text1=getString(R.string.premium_expiration);
                                    texto1.setText(text1);
                                    dialog1.show();

                                    final Timer t = new Timer();
                                    t.schedule(new TimerTask() {
                                        public void run() {
                                            try {
                                                dialog1.dismiss();
                                                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                                                Intent intent = new Intent(home.this, splashscreen.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 4000);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                txt_premium3.setText("00|00|0000");
                            }

                            break;
                        }
                        case "I":{
                            panel_nopremium.setVisibility(View.VISIBLE);
                            panel_premium.setVisibility(View.GONE);
                            break;
                        }
                        case "B":{
                            panel_nopremium.setVisibility(View.VISIBLE);
                            panel_premium.setVisibility(View.GONE);
                            break;
                        }
                    }
                    configure();
                    configureMenu();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                CURRENT_PROCESS++;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                CURRENT_PROCESS++;
                preloader1.dismiss();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }
    public void loadPhotoProfile(int userId){
        final Dialog preloader2=Preloader.getInstance(home.this).build();
        preloader2.show();
        Request.get("photos/getPhotoProfile/userId/" + userId, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    if (!cadena.equals("")) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = ImageUtils.decodeFile(bytes, 150);
                        ImageUtils.saveToCacheFile(bitmap);
                        photoProfile.setImageBitmap(bitmap);
                        photoProfile.setAdjustViewBounds(false);
                        photoProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else {
                        photoProfile.setImageResource(R.mipmap.photo_default);
                    }
                } catch (Exception ex) {
                    photoProfile.setImageResource(R.mipmap.photo_default);
                } catch (OutOfMemoryError ex) {
                    photoProfile.setImageResource(R.mipmap.photo_default);
                }
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    photoProfile.setImageResource(R.mipmap.photo_default);
                } catch (Exception ex) {
                    photoProfile.setImageResource(R.mipmap.photo_default);
                }
                CURRENT_PROCESS++;
                preloader2.dismiss();
            }
        });

    }
    public void loadMessagesNotReaded(){
        final Dialog preloader3=Preloader.getInstance(home.this).build();
        preloader3.show();
        RequestParams params=new RequestParams();
        params.put("nickname",BaseHelper.getNickname(getApplicationContext()));
        Request.get("chats/messagesnotreaded/", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    txt_messageNotReaded.setText(response.length() + "");
                } catch (Exception e) {
                    txt_messageNotReaded.setText("0");
                }

                CURRENT_PROCESS++;
                preloader3.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                preloader3.dismiss();
            }
        });
    }

    public  void profile_onclick(View v)
    {
        Intent intent=new Intent(getApplicationContext(),YourProfile.class);
        startActivity(intent);
    }
    public  void chat_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(),ListChat.class);
                intent.putExtra("WITH_PHOTO", "");
                intent.putExtra("ID_USER", this.id_user);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(getApplicationContext(),ListChat.class);
                intent.putExtra("WITH_PHOTO", "");
                intent.putExtra("ID_USER", this.id_user);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }

    }
    public void search_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(),SearchFilter.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(getApplicationContext(),SearchFilter.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void config_onclick(View v)
    {
        Intent intent=new Intent(getApplicationContext(),conf_profile.class);
        intent.putExtra("ID_USER", BaseHelper.getUserId(getApplicationContext()));
        startActivity(intent);

    }
    public void chating_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void mughugs_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), maghugs.class);
                startActivity(intent);
                break;
            }
            case "I":{
//                Intent intent=new Intent(getApplicationContext(), maghugs.class);
//                startActivity(intent);
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }

    }
    public void match_onclick(View v)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), Match.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                //Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }
    public void premium_onclick(View v)
    {
        Intent intent=new Intent(getApplicationContext(), GettingPremium.class);
        startActivity(intent);
    }
    public void openInvitation_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), Invitations.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(getApplicationContext(), Invitations.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(home.this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void toggle(View v){
        this.root.toggleMenu();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean band=true;
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(root.menuCurrentState== FlyOutContainer.MenuState.OPEN)
            {
                root.toggleMenu();
                band=false;
            }
            else
            {
                band=true;
            }
        }
        if(band)
        {
            this.finish();
        }
        return band;
    }


}
