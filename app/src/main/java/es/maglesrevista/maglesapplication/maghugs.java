package es.maglesrevista.maglesapplication;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import custom.ObservableScrollView;
import custom.WidgetMaghugs;
import entities.MagHug;
import utils.BaseHelper;
import utils.Preloader;
import utils.Request;
import utils.ScrollViewListener;
import utils.SessionManager;
import utils.TextFont;


public class maghugs extends BaseBarActivity implements ScrollViewListener{

    WidgetMaghugs widget_maghug;
    int userId;
    ObservableScrollView scrollOnDemand;
    int PAGE_SIZE=20;
    int CURRENT_PAGE=0;
    int TOTAL_ROWS=0;
    SwipeRefreshLayout swipeContainer;
    boolean isBusy =false;
    int TOTAL_PAGES=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NUMBER_PROCESS=1;
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_maghugs, null);
        this.setContentView(root);

        session=new SessionManager(getApplicationContext());
        scrollOnDemand=(ObservableScrollView)root.findViewById(R.id.scrollOnDemand);
        TextView txttitleMagHug=(TextView)root.findViewById(R.id.titleMagHug);
        scrollOnDemand.setScrollViewListener(this);
        swipeContainer=(SwipeRefreshLayout)root.findViewById(R.id.swipeContainer);
        reloadActivity();
        widget_maghug=(WidgetMaghugs)root.findViewById(R.id.list_maghug);

        this.userId= BaseHelper.getUserId(getApplicationContext());
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        txttitleMagHug.setTypeface(Typeface.DEFAULT_BOLD);


    }
    @Override
    public void onResume()
    {
        CURRENT_PROCESS=0;
        NUMBER_PROCESS=0;
        CURRENT_PAGE=0;
        widget_maghug.clear();
        loadHeader();
        loadMugHug();
        super.onResume();
    }
    public void reloadActivity(){
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setEnabled(true);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                NUMBER_PROCESS = 1;
                CURRENT_PROCESS = 0;
                CURRENT_PAGE = 0;
                widget_maghug.clear();
                loadMugHug();
                swipeContainer.setRefreshing(false);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(R.color.color_btn_magles,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_red_dark);
    }
    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        LinearLayout target=(LinearLayout)scrollView.getChildAt(0);
        View view = (View) target.getChildAt( target.getChildCount()-1);

        // Calculate the scrolldiff
        int diff = (view.getBottom()- (scrollView.getHeight()+scrollView.getScrollY()));
        if(y!=0)
            swipeContainer.setEnabled(false);
        else
            swipeContainer.setEnabled(true);

        // if diff is zero, then the bottom has been reached
        if( diff == 0)
        {
            if(isBusy ==false) {
                isBusy =true;
                NUMBER_PROCESS=1;
                CURRENT_PROCESS=0;
                loadMugHug();
            }
        }
    }
    public void loadMugHug(){
        final Dialog preloader1= Preloader.getInstance(maghugs.this).build();
        preloader1.show();
        RequestParams params=new RequestParams();
        params.put("user_id",this.userId);
        params.put("page_size",PAGE_SIZE);
        params.put("page",CURRENT_PAGE);
        Request.post("RelationShips/findAllMagHugs", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                ArrayList<MagHug> maghug_list = new ArrayList<MagHug>();
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        MagHug maghug = new MagHug();
                        maghug.setID_USER(obj.getInt("ID_USER"));
                        maghug.setName_contact(obj.getString("Nickname"));
                        maghug.setPathComplete(obj.getString("PathComplete"));
                        maghug.setReturned(obj.getInt("returned"));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            String date_string = obj.getString("entry_date");
                            java.util.Date date = formatter.parse(date_string);
                            maghug.setEntry_date(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        TOTAL_ROWS = obj.getInt("total_rows");
                        TOTAL_PAGES = (int) Math.ceil(((double) TOTAL_ROWS) / PAGE_SIZE);
                        maghug_list.add(maghug);
                    } catch (Exception e) {
                        Log.w("magles", e.getMessage());
                    }

                }
                widget_maghug.setDataSource(maghug_list);
                widget_maghug.dataBind();
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy =false;
                preloader1.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                CURRENT_PROCESS++;
                CURRENT_PAGE++;
                isBusy =false;
                preloader1.dismiss();
            }
        });
    }

}
