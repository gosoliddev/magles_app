package es.maglesrevista.maglesapplication;

import android.content.Intent;

import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import custom.ObservableScrollView;
import utils.BaseHelper;
import utils.Preloader;
import utils.SessionManager;
import utils.TextFont;


public class menuchat extends BaseBarActivity{
    SwipeRefreshLayout swipeContainer;
    ObservableScrollView scrollOnDemand;
    TextView text_op1,text_op2,text_op3,text_op4;
    RelativeLayout pnlaction_1,pnlaction_2,pnlaction_0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_menuchat, null);
        this.setContentView(root);
        session=new SessionManager(getApplicationContext());
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        text_op1=(TextView)root.findViewById(R.id.text_op1);
        text_op2=(TextView)root.findViewById(R.id.text_op2);
        text_op3=(TextView)root.findViewById(R.id.text_op3);
        text_op4=(TextView)root.findViewById(R.id.text_op4);
        text_op1.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));
        text_op2.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));
        text_op3.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));
        text_op4.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));
        pnlaction_1=(RelativeLayout)root.findViewById(R.id.pnlaction_1);
        pnlaction_2=(RelativeLayout)root.findViewById(R.id.pnlaction_2);
        pnlaction_0=(RelativeLayout)root.findViewById(R.id.pnlaction_0);

    }
    @Override
    public void onResume(){
        loadHeader();
        configure();
        super.onResume();

    }
    public void configure()
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                BaseHelper.applyAlpha(pnlaction_1, 1.0f);
                BaseHelper.applyAlpha(pnlaction_2, 1.0f);
                BaseHelper.applyAlpha(pnlaction_0, 1.0f);

                break;
            }
            case "I":{
                BaseHelper.applyAlpha(pnlaction_1, 0.5f);
                BaseHelper.applyAlpha(pnlaction_2, 0.5f);
//                BaseHelper.applyAlpha(pnlaction_0, 0.5f);

                break;
            }
            case "B":{

                BaseHelper.applyAlpha(pnlaction_1, 0.5f);
                BaseHelper.applyAlpha(pnlaction_2, 0.5f);
                BaseHelper.applyAlpha(pnlaction_0, 0.5f);

                break;
            }
        }
    }
    public void micuenta_onclick(View view)
    {
        Intent intent=new Intent(menuchat.this,YourProfile.class);
        startActivity(intent);
    }
    public void search_onclick(View view)
    {

        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(menuchat.this,SearchFilter.class);
                startActivity(intent);
                break;
            }
            case "I":{
                Intent intent=new Intent(menuchat.this,SearchFilter.class);
                startActivity(intent);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_INVITED);
                break;
            }
        }
    }
    public void matchinterest_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), Match.class);
                intent.putExtra("type",2);
                startActivity(intent);
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }
    public void matchwith_onclick(View view)
    {
        switch(BaseHelper.getUserStatus(getApplicationContext())) {
            case "P": {
                Intent intent=new Intent(getApplicationContext(), Match.class);
                intent.putExtra("type",1);
                startActivity(intent);
                break;
            }
            case "I":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
            case "B":{
                Preloader.getInstance(this).dialogBasic(Preloader.TAG_PERMISION_PREMIUM);
                break;
            }
        }
    }





}
