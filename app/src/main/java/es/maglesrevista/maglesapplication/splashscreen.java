package es.maglesrevista.maglesapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.IOException;
import java.util.TimeZone;

import entities.User;
import pushnotifications.Globals;
import utils.BaseHelper;
import utils.ImageUtils;
import utils.Preloader;
import utils.Request;
import utils.SessionManager;


public class splashscreen extends Activity {
    private static int SPLASH_TIME_OUT = 3000;
    SessionManager sessionManager;
    GoogleCloudMessaging gcm;
    String regid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        sessionManager=new SessionManager(getApplicationContext());
        if(BaseHelper.getKeepLogin(getApplicationContext()))
        {
            autoLogin();
        }
        else
        {
            sessionManager.setKeepLogin(false);
            Intent i = new Intent(splashscreen.this, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }
    public void autoLogin(){
        RequestParams params = new RequestParams();
        params.put("username", sessionManager.getUserDetails().get(SessionManager.KEY_NICKNAME));
        params.put("password",sessionManager.getUserDetails().get(SessionManager.KEY_PASSWORD));
        params.put("timezone", TimeZone.getDefault().getID());
        Request.post("users/login", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, JSONObject object) {
                try {

                    User user = new User();
                    user.setID_USER(object.getInt("ID_USER"));
                    user.setName(object.getString("Name"));
                    user.setLastName(object.getString("LastName"));
                    user.setNickname(object.getString("Nickname"));
                    user.setEmail(object.getString("Email"));
                    user.setStatus(object.getString("Status"));
                    sessionManager.setLogin(true);
                    loadPhotoProfile(user.getID_USER());
                    registerInBackground();
                } catch (Exception ex) {
                    sessionManager.setLogin(false);
                    Intent intent = new Intent().setClass(splashscreen.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                sessionManager.setLogin(false);
                Intent intent = new Intent().setClass(splashscreen.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }
    public void loadPhotoProfile(int userId){
        Request.get("photos/getPhotoProfile/userId/" + userId, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                try {
                    String cadena = new String(bytes, "UTF-8");
                    if (!cadena.equals("")) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = ImageUtils.decodeFile(bytes, 150);
                        ImageUtils.saveToCacheFile(bitmap);

                    }
                } catch (Exception ex) {
                } catch (OutOfMemoryError ex) {
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                try {

                } catch (Exception ex) {
                }
            }
        });
    }
    private void registerInBackground()
    {
        final Dialog preloader=Preloader.getInstance(splashscreen.this).build();
        preloader.show();
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (gcm == null)
                    {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(Globals.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg)
            {
                preloader.dismiss();
                sendRegistrationIdToServer(regid);
            }
        }.execute(null, null, null);
    }
    public void sendRegistrationIdToServer(String registrationId){
        final Dialog preloder=Preloader.getInstance(splashscreen.this).build();
        preloder.show();
        RequestParams params=new RequestParams();
        params.put("user_id",BaseHelper.getUserId(getApplicationContext()));
        params.put("registrationId", registrationId);
        Request.post("users/saveregistration", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    sessionManager.setRegistrationId(response.getString("registrationId"));
                    sessionManager.setLogin(true);
                    Intent intent = new Intent().setClass(splashscreen.this, home.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                } catch (Exception ex) {
                }
                preloder.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                preloder.dismiss();
                Preloader.getInstance(splashscreen.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                preloder.dismiss();
                Preloader.getInstance(splashscreen.this).dialogInfo("Hubo un problema en el envío de tu identificador para el chat vuelve intentarlo.");
            }
        });
    }

}
