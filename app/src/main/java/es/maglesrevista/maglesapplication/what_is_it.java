package es.maglesrevista.maglesapplication;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import utils.TextFont;

public class what_is_it extends BaseBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_is_it);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        TextView title_whatisit=(TextView)findViewById(R.id.title_whatisit);
        TextFont.getInstance().overrideFonts(getApplicationContext(), getWindow().getDecorView().findViewById(android.R.id.content), TextFont.PATH_FONT);
        title_whatisit.setTextColor(Color.parseColor("#7b7677"));
        title_whatisit.setTypeface(TextFont.getInstance().getFont(getApplicationContext(),TextFont.PATH_FONT_BOLD));
    }

}
