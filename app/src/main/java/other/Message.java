package other;

public class Message {
	private String fromName, message;
	private boolean isSelf;
	private boolean readed=false;

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	private String receiver;
	public String getType_message() {
		return type_message;
	}

	public void setType_message(String type_message) {
		this.type_message = type_message;
	}

	private String type_message;
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	private String date;
	public Message() {
	}

	public Message(String fromName, String message, boolean isSelf) {
		this.fromName = fromName;
		this.message = message;
		this.isSelf = isSelf;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSelf() {
		return isSelf;
	}

	public void setSelf(boolean isSelf) {
		this.isSelf = isSelf;
	}
	public void setReaded(boolean readed){this.readed=readed;}
	public boolean getReaded(){return readed;}


}
