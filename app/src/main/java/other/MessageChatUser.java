package other;

import java.util.Date;

import entities.Data;

/**
 * Created by Usuario on 11/08/2015.
 */
public class MessageChatUser {
    private String fromName, message;
    private Date fromDate;
    private boolean isView;
    private String receiver;
    private int ID_USER;

    public String getType_message() {
        return type_message;
    }

    public void setType_message(String type_message) {
        this.type_message = type_message;
    }

    private String type_message;

    public int getID_USER_CONTACT() {
        return ID_USER_CONTACT;
    }

    public void setID_USER_CONTACT(int ID_USER_CONTACT) {
        this.ID_USER_CONTACT = ID_USER_CONTACT;
    }

    public int getID_USER() {
        return ID_USER;
    }

    public void setID_USER(int ID_USER) {
        this.ID_USER = ID_USER;
    }

    private int ID_USER_CONTACT;
    public MessageChatUser() {
    }

    public MessageChatUser(String fromName, String message, Date fromDate, boolean isView) {
        this.fromName = fromName;
        this.message = message;
        this.fromDate = fromDate;
        this.isView = isView;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getFromDate()
    {
        return this.fromDate;
    }

    public void setFromDate(Date fromDate)
    {
        this.fromDate = fromDate;
    }

    public boolean getIsView() {
        return isView;
    }

    public void setIsView(boolean isView) {
        this.isView = isView;
    }
}
