package other;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.json.JSONException;
import org.json.JSONObject;

public class Utils {

	private Context context;
	private SharedPreferences sharedPref;

	private static final String KEY_SHARED_PREF = "ANDROID_WEB_CHAT";
	private static final int KEY_MODE_PRIVATE = 0;
	private static final String KEY_SESSION_ID = "sessionId",
			FLAG_MESSAGE = "message";

	public Utils(Context context) {
		this.context = context;
		sharedPref = this.context.getSharedPreferences(KEY_SHARED_PREF,
				KEY_MODE_PRIVATE);
	}

	public void storeSessionId(String sessionId) {
		Editor editor = sharedPref.edit();
		editor.putString(KEY_SESSION_ID, sessionId);
		editor.commit();
	}

	public String getSessionId() {
		return sharedPref.getString(KEY_SESSION_ID, null);
	}

	public String getSendMessageJSON(String message) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put("flag", FLAG_MESSAGE);
			jObj.put("sessionId", getSessionId());
			jObj.put("message", message);

				json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	public String getSendMessageJSONChat(String message,String nicknameFriend) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put("flag", FLAG_MESSAGE);
			jObj.put("sessionId", getSessionId());
			jObj.put("message", message);
			jObj.put("nickFriend",nicknameFriend);

			//create for chat custom

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	public String getSendMessageAceptedJSONChat(String message,String nicknameFriend) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put("flag", "accepted");
			jObj.put("sessionId", getSessionId());
			jObj.put("message", message);
			jObj.put("nickFriend",nicknameFriend);

			//create for chat custom

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	public String getSendMessageImageJSONChat(String message,String nicknameFriend) {
		String json = null;

		try {
			JSONObject jObj = new JSONObject();
			jObj.put("flag", "image");
			jObj.put("sessionId", getSessionId());
			jObj.put("message", message);
			jObj.put("nickFriend",nicknameFriend);

			//create for chat custom

			json = jObj.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

}
