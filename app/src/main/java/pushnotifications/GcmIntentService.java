package pushnotifications;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.SimpleFormatter;

import es.maglesrevista.maglesapplication.Login;
import es.maglesrevista.maglesapplication.MainActivity;
import es.maglesrevista.maglesapplication.R;
import utils.DateUtils;
import utils.ImageUtils;
import utils.Request;

public class GcmIntentService extends IntentService
{
    public static final int NOTIFICATION_ID = 1;
	public static final String GROUP_KEY="chat_magles";
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService()
    {
	super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
	Bundle extras = intent.getExtras();
	GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
	// The getMessageType() intent parameter must be the intent you received
	// in your BroadcastReceiver.
	String messageType = gcm.getMessageType(intent);

	if (!extras.isEmpty()) // has effect of unparcelling Bundle
	{
	    /*
	     * Filter messages based on message type. Since it is likely that
	     * GCM will be extended in the future with new message types, just
	     * ignore any message types you're not interested in, or that you
	     * don't recognize.
	     */
	    if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType))
	    {
		sendNotification("Send error: " + extras.toString(), null);
	    }
	    else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType))
	    {
		sendNotification("Deleted messages on server: " + extras.toString(), null);
	    }
	    else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
	    {
		// Post notification of received message.
		String message = ((intent.getExtras() == null) ? "Empty Bundle" : intent.getExtras().getString("message"));
		if (intent.getExtras() != null
			&& "es.magelesrevista.maglesapplication.CLEAR_NOTIFICATION".equals(intent.getExtras()
				.getString("action")))
		{
		    clearNotification();
		}
		else
		{
		    sendNotification(message, extras);
		}
		Log.i(Globals.TAG, "Received: " + message);
	    }
	}
	// Release the wake lock provided by the WakefulBroadcastReceiver.
	GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
     * Put the message into a notification and post it.
     * This is just one simple example of what you might choose to do with 
     * a GCM message.
     * @param msg
     * @param extras
     */
    private void sendNotification(String msg, Bundle extras)
    {
		buildNotification2(msg, extras);
    }

    /**
     * Remove the app's notification
     */
    private void clearNotification()
    {
	mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
	mNotificationManager.cancel(NOTIFICATION_ID);
    }
	public Bitmap getBitmapFromURL(String strURL) {
		try {

			//URL url = new URL(strURL));

			URL url = new URL(strURL);
			URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			url = uri.toURL();
			Bitmap myBitmap=null;
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(0);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-agent", "Mozilla/5.0");
			connection.setDoInput(true);
			connection.connect();
			int responseCode = connection.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_OK)
			{
				//download
				InputStream input = connection.getInputStream();
				myBitmap = BitmapFactory.decodeStream(input);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				myBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				myBitmap=ImageUtils.decodeFile(byteArray,200);
			}

			return myBitmap;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void buildNotification2(String message,Bundle extras){
		Bitmap bitmap=getBitmapFromURL(Request.BASE_URL+"Photos/getPhotoByNickname?nickname="+URLEncoder.encode(extras.getString("name")));
		int id=Integer.valueOf(extras.getString("id"));
		Intent intent = new Intent(this, MainActivity.class);
		Date date= DateUtils.currentDate();
		String date_sent="";
		try
		{
			SimpleDateFormat formatter =new SimpleDateFormat("HH:mm");
			date_sent=formatter.format(date);
		}
		catch (Exception e){
			;
		}
		if (extras != null)
		{
			extras.putString("type", "1");
			extras.putInt("id",id);
			intent.putExtras(extras);
		}
		PendingIntent pIntent = PendingIntent.getActivity(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);
		builder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_stat_logo)
				.setContent(remoteViews)
				.setContentIntent(pIntent)
				.setAutoCancel(true)
				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
		if(bitmap!=null)
			remoteViews.setImageViewBitmap(R.id.notifiation_image, bitmap);

		remoteViews.setTextViewText(R.id.notification_title, "MagLes Match");
		remoteViews.setTextViewText(R.id.notification_text, extras.getString("name") + "\n" + message);
		remoteViews.setTextViewText(R.id.notification_time ,date_sent);


		// Create Notification Manager
		NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Build Notification with Notification Manager
		notificationmanager.notify(id, builder.build());
	}
}
