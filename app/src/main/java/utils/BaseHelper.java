package utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;

import java.util.Calendar;
import java.util.Date;

import es.maglesrevista.maglesapplication.R;

/**
 * Created by Romano on 31/07/2015.
 */
public class BaseHelper {
    static SessionManager session;



    public static  int getUserId(Context context){
        session=new SessionManager(context);
        return Integer.valueOf(session.getUserDetails().get(session.KEY_ID_USER));
    }
    public static  String getUserStatus(Context context){
        session=new SessionManager(context);
        return String.valueOf(session.getUserDetails().get(session.KEY_STATUS));
    }
    public static String getNickname(Context context){
        session=new SessionManager(context);
        return session.getUserDetails().get(session.KEY_NICKNAME);
    }
    public static boolean isVisible(Context context){
        session=new SessionManager(context);
        return Boolean.valueOf(session.getUserDetails().get(session.KEY_VISIBLE));
    }
    public static String getRegistrationId(Context context){
        session=new SessionManager(context);
        return  session.getUserDetails().get(session.KEY_REGISTRATIONID);
    }
    public static boolean getKeepLogin(Context context){
        session=new SessionManager(context);
        return  session.getUserDetails().get(session.KEY_KEEPLOGIN).equals("true")?true:false;
    }
    public static void applyAlpha(View view,float alpha_value){
        AlphaAnimation alpha = new AlphaAnimation(alpha_value, alpha_value);
        alpha.setDuration(50); // Make animation instant
        alpha.setFillAfter(true); // Tell it to persist after the animation ends
        view.startAnimation(alpha);
    }
    public static void logout(Context context){
        session=new SessionManager(context);
        session.logoutUser();
    }
    public static void onDestroy(Activity activity) {
        View rootView = null;

        try {
            rootView = ((ViewGroup) activity.findViewById(android.R.id.content))
                    .getChildAt(0);
        } catch (Exception e) {
           // Log.w("Cannot find root view to call unbindDrawables on",activity);
        }

        if (rootView != null) {
            //Log.d("unbindDrawables", activity, rootView);
            unbindDrawables(rootView);
        }
    }

    /**
     * Utility method to unbind drawables when an activity is destroyed.  This
     * ensures the drawables can be garbage collected.
     */
    public static void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }

            try {
                // AdapterView objects do not support the removeAllViews method
                if (!(view instanceof AdapterView)) {
                    ((ViewGroup) view).removeAllViews();
                }
            } catch (Exception e) {
                Log.w("Ignore Exception in unbindDrawables", e);
            }
        }
    }

    public static String DiferenciaFechas(Date vinicio, Date vfinal){

        Date dinicio = null, dfinal = null;
        long milis1, milis2, diff;

        dinicio = vinicio;
        dfinal = vfinal;

        //INSTANCIA DEL CALENDARIO GREGORIANO
        Calendar cinicio = Calendar.getInstance();
        Calendar cfinal = Calendar.getInstance();

        //ESTABLECEMOS LA FECHA DEL CALENDARIO CON EL DATE GENERADO ANTERIORMENTE
        cinicio.setTime(dinicio);
        cfinal.setTime(dfinal);

        milis1 = cinicio.getTimeInMillis();
        milis2 = cfinal.getTimeInMillis();
        diff = milis2-milis1;

        // calcular la diferencia en segundos
        long diffSegundos =  Math.abs (diff / 1000);

        // calcular la diferencia en minutos
        long diffMinutos =  Math.abs (diff / (60 * 1000));
        long restominutos = diffMinutos%60;

        // calcular la diferencia en horas
        long diffHoras =   (diff / (60 * 60 * 1000));

        // calcular la diferencia en dias
        long diffdias = Math.abs ( diff / (24 * 60 * 60 * 1000) );

        String devolver = "";
        if(diffSegundos > 0 && restominutos <= 0 && diffHoras <= 0 && diffdias <= 0)
            devolver = "Hace unos instantes";
        else{
            if(diffSegundos <= 0 || restominutos > 0 && diffHoras <= 0 && diffdias <= 0)
            {
                String minuto = restominutos >1 ? " minutos": " minuto";
                devolver = "Hace "+restominutos + minuto;
            }else
            {
                if(diffSegundos <= 0 && restominutos <= 0 || diffHoras > 0 && diffdias <= 0)
                {
                    String hora = diffHoras >1 ? " horas": " hora";
                    devolver = "Hace "+diffHoras + hora;
                }else
                {
                    if(diffSegundos <= 0 && restominutos <= 0 && diffHoras <= 0 || diffdias > 0)
                    {
                        String dia = diffdias >1 ? " días": " día";
                        devolver = "Hace "+diffdias + dia;
                    }
                }
            }
        }
        return devolver;
    }
    public static String PrettifyDateDiff(long dateDiff, boolean showDisclaimer) {
        /* Constantes utilizadas para facilitar
         * la lectura de la aplicación
         */

        long second = 1000;
        long minute = second * 60;
        long hour = minute * 60;
        long day = hour * 24;
        long week = day * 7;
        long month = day * 30;
        long year = month * 12;

        // Dividimos los milisegundos entre su equivalente de
        // las constantes de arriba para obtener el valor en la
        // escala de tiempo correspondiente.
        long minutes = dateDiff / minute;
        long hours = dateDiff / hour;
        long days = dateDiff / day;
        long weeks = dateDiff / week;
        long months = dateDiff / month;
        long years = dateDiff / year;

        String prettyDateString = "";

        if (minutes > 60) {
            prettyDateString = minutes - (hours * 60) + " minutos.";

            if (hours > 24) {
                prettyDateString = hours - (days * 24) + " horas " + prettyDateString;

                if (days > 7) {
                    prettyDateString = days - (weeks * 7) + " dias " + prettyDateString;

                    if(weeks > 4){
                        prettyDateString = weeks - (months * 4)  + " semanas " + prettyDateString;

                        if(months > 12){

                            prettyDateString = months - (years * 12) + " meses " + prettyDateString;

                            if(years > 0){
                                prettyDateString = years + " años " + prettyDateString;
                            }
                        }else{
                            prettyDateString = months  + " meses " + prettyDateString;
                        }

                    }else{
                        prettyDateString = weeks + " semanas " + prettyDateString;
                    }
                } else {
                    prettyDateString = days + " dias " + prettyDateString;
                }
            } else {
                prettyDateString = hours + " horas " + prettyDateString;
            }

        } else {
            prettyDateString = minutes + " minutos.";
        }

        if(showDisclaimer && (weeks > 0 || months > 0)){
            prettyDateString += " (Semanas de 7 dias, Meses de 30 dias).";
        }

        return prettyDateString;
    }
}
