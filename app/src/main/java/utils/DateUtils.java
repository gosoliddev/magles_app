package utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.SimpleFormatter;

import es.maglesrevista.maglesapplication.R;

/**
 * Created by Romano on 26/10/2015.
 */
public class DateUtils {
    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static String getTimeAgo(Date date, Context ctx) {

        if(date == null) {
            return null;
        }

        long time = date.getTime();

        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int dim = getTimeDistanceInMinutes(time);

        String timeAgo = null;
        if(dim <= 1)
        {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_ego) + " "+ ctx.getResources().getString(R.string.date_util_term_a)+" " +  ctx.getResources().getString(R.string.date_util_term_moment);
        }
        else if (dim>=2 && dim <=59){
            timeAgo = ctx.getResources().getString(R.string.date_util_term_ego) + " "+ dim+" " +  ctx.getResources().getString(R.string.date_util_unit_minutes);
        }
        else if(dim>59 && Math.round(dim / 60)<=1)
        {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_ego) + " "+(Math.round(dim / 60))+ " " + ctx.getResources().getString(R.string.date_util_unit_hour);
        }
        else if(Math.round(dim / 60)>=2 &&  Math.round(dim / 60)<24 )
        {
            timeAgo = ctx.getResources().getString(R.string.date_util_term_ego) + " "+(Math.round(dim / 60))+ " " + ctx.getResources().getString(R.string.date_util_unit_hours);
        }
        else if(Math.round(dim / 60)>=24 &&  Math.round(dim / 60)<=48 )
        {
            try {
                SimpleDateFormat formatter_date = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat formatter_hours = new SimpleDateFormat("HH:mm");
                timeAgo = ctx.getResources().getString(R.string.date_util_unit_term_yesterday)+" "+formatter_hours.format(date);;
            }
            catch (Exception e){}

        }
        else if(Math.round(dim / 60)>=48)
        {
            try {
                SimpleDateFormat formatter_date = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat formatter_hours = new SimpleDateFormat("HH:mm");
                timeAgo = formatter_date.format(date)+" "+formatter_hours.format(date);
            }
            catch (Exception e){}
        }



        return timeAgo;
    }
    public static int compareDates(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            int retVal = date1.compareTo(date2);

            if (retVal > 0)
                return 1;// date1 is greatet than date2
            else if (retVal == 0)
                return 0; // both dates r equal

        }
        return -1;// date1 is less than date2
    }
    public static   Date convertFromOneTimeZoneToOhter(Date dt,String from,String to ) {

        TimeZone fromTimezone =TimeZone.getTimeZone(from);//get Timezone object
        TimeZone toTimezone=TimeZone.getTimeZone(to);

        long fromOffset = fromTimezone.getOffset(dt.getTime());//get offset
        long toOffset = toTimezone.getOffset(dt.getTime());

        //calculate offset difference and calculate the actual time
        long convertedTime = dt.getTime() - (fromOffset - toOffset);
        Date d2 = new Date(convertedTime);

        return d2;
    }
    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }
}
