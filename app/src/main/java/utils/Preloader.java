package utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import es.maglesrevista.maglesapplication.GettingPremium;
import es.maglesrevista.maglesapplication.R;
import es.maglesrevista.maglesapplication.what_is_it;

/**
 * Created by Romano on 12/08/2015.
 */
public class Preloader {
    public static final String TAG_PERMISION_PREMIUM ="premium";
    public static final String TAG_PERMISION_INVITED="invited";



    private   boolean isLogin=false;
    private static Activity activity;


    private int noti_local_id=-1;

    private String PACKAGE_ID="";
    Dialog dialog;
    Dialog dialog_info;
    Dialog preloader;
    private static Preloader INSTANCE = null;
    public static Preloader getInstance(Activity activity_parent) {
        if (INSTANCE == null) {
            INSTANCE = new Preloader();

        }
        activity=activity_parent;
        INSTANCE.createDialog(activity);
        return INSTANCE;
    }
    public Dialog build(){
        Dialog preloader = new Dialog(activity);
        preloader.setCancelable(true);
        preloader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        preloader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preloader.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        preloader.setContentView(R.layout.preloader);
        return preloader;
    }
    public void createDialog(Activity activity){
        try {
            if (dialog == null || !dialog.isShowing()) {
                dialog = new Dialog(activity);
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
                dialog.setContentView(R.layout.preloader);
                ProgressBar mPregressBar=(ProgressBar)dialog.findViewById(R.id.preloader);
                mPregressBar.getIndeterminateDrawable().setColorFilter(
                        activity.getResources().getColor(R.color.color_btn_magles),
                        android.graphics.PorterDuff.Mode.SRC_IN);
            }
            else if (dialog!=null &&  dialog.isShowing())
            {
                dialog().dismiss();
            }
        }
        catch (Exception e)
        {

        }
    }
    public void dialogInfo(String text){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        TextView texto=(TextView)dialog.findViewById(R.id.text_dialog);
        texto.setText(text);
        dialog.show();
    }
    public Dialog dialog(){

        return dialog;
    }
    public void dialogInvited(){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info);
        TextView texto=(TextView)dialog.findViewById(R.id.text_dialog);
        dialog.show();
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                dialog.cancel(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 4000);
    }
    public void dialogBasic(String tag){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        switch (tag)
        {
            case TAG_PERMISION_PREMIUM:{
                dialog.setContentView(R.layout.dialog_permision_premium);
                TextView message_premision=(TextView)dialog.findViewById(R.id.message_premision);
                Button btn_premium=(Button)dialog.findViewById(R.id.btn_premium);
                message_premision.setText(activity.getString(R.string.permision_text2));
                btn_premium.setText(activity.getString(R.string.premium_btn));
                btn_premium.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, GettingPremium.class);
                        activity.startActivity(intent);
                    }
                });
               break;

            }
            case TAG_PERMISION_INVITED:{
                dialog.setContentView(R.layout.dialog_permision_invitation);
                TextView message_premision=(TextView)dialog.findViewById(R.id.message_premision);
                TextView link_permision=(TextView)dialog.findViewById(R.id.link_permision);
                link_permision.setClickable(true);
                message_premision.setText(activity.getString(R.string.permision_text1));
                //link_permision.setText(Html.fromHtml(activity.getString(R.string.permision_info)));
                link_permision.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(activity,what_is_it.class);
                        activity.startActivity(intent);
                    }
                });
                break;
            }
        }
        dialog.show();
//        final Timer t = new Timer();
//        t.schedule(new TimerTask() {
//            public void run() {
//                dialog.cancel(); // when the task active then close the dialog
//                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
//            }
//        }, 4000);
    }
    public String getPACKAGE_ID() {
        return PACKAGE_ID;
    }

    public void setPACKAGE_ID(String PACKAGE_ID) {
        this.PACKAGE_ID = PACKAGE_ID;
    }
    public int getNoti_local_id() {
        return noti_local_id;
    }

    public void setNoti_local_id(int noti_local_id) {
        this.noti_local_id = noti_local_id;
    }
    public   boolean isLogin() {
        return isLogin;
    }
    public   void setIsLogin(Boolean status) {
         isLogin=status;
    }
}
