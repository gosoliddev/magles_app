package utils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.client.HttpResponseException;

import es.maglesrevista.maglesapplication.BaseBarActivity;

/**
 * Created by Romano on 06/07/2015.
 */
public class Request {

    public static final String BASE_URL = "http://magles.develoop.net/api/";//"http://192.168.0.200:90/api/" //http://192.168.0.59:8080/ServerChat/

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        try {
            client.setTimeout(20 * 1000);
            client.setConnectTimeout(20*1000);
            client.setResponseTimeout(20*1000);
            client.get(getAbsoluteUrl(url), params, responseHandler);
        }
        catch (Exception e)
        {
            Thread.currentThread().interrupt();
        }

    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        try {
            client.setTimeout(20 * 1000);
            client.setConnectTimeout(20*1000);
            client.setResponseTimeout(20*1000);
            client.post(getAbsoluteUrl(url), params, responseHandler);
        }
        catch (Exception ex){
            Thread.currentThread().interrupt();
        }
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
    private static String getBaseUrl() {

        return BASE_URL;

    }
}
