package utils;

import custom.ObservableScrollView;

/**
 * Created by Usuario on 15/07/2015.
 */
public interface ScrollViewListener {

    void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy);
}
