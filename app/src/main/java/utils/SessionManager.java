package utils;
import java.util.HashMap;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import es.maglesrevista.maglesapplication.Login;

/**
 * Created by Romano on 16/07/2015.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    public static final String KEY_KEEPLOGIN = "KEEPLOGIN";
    // Sharedpref file name
    private static final String PREF_NAME = "Magles";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "NAME";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "EMAIL";

    public static final String KEY_ID_USER = "ID_USER";

    public static final String KEY_NICKNAME = "NICKNAME";

    public static final String KEY_PASSWORD = "PASSWORD";
    public static final String KEY_STATUS = "STATUS";
    public static final String KEY_VISIBLE = "VISIBLE";
    public static final String KEY_REGISTRATIONID="REGISTRATIONID";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name, String email,String ID_USER,String nickname,String password,String status,boolean visible){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putBoolean(KEY_KEEPLOGIN,true);
        editor.putBoolean(KEY_VISIBLE, true);
        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        editor.putString(KEY_ID_USER,ID_USER);

        editor.putString(KEY_NICKNAME,nickname);

        editor.putString(KEY_PASSWORD,password);

        editor.putString(KEY_STATUS, status);

        editor.putBoolean(KEY_VISIBLE, visible);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, Login.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        //user id
        user.put(KEY_ID_USER, pref.getString(KEY_ID_USER, null));

        user.put(KEY_NICKNAME, pref.getString(KEY_NICKNAME, null));

        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));

        user.put(KEY_STATUS, pref.getString(KEY_STATUS, null));

        user.put(KEY_VISIBLE, String.valueOf(pref.getBoolean(KEY_VISIBLE, false)));

        user.put(KEY_REGISTRATIONID, pref.getString(KEY_REGISTRATIONID, null));

        user.put(KEY_KEEPLOGIN, pref.getBoolean(KEY_KEEPLOGIN, false)?"true":"false");
        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, Login.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
    public void setKeepLogin(boolean state){
        editor.putBoolean(KEY_KEEPLOGIN, state);
        editor.commit();
    }
    public void setLogin(boolean state){
        editor.putBoolean(IS_LOGIN, state);
        editor.commit();
    }
    public void setVisibility(boolean visible){
        editor.putBoolean(KEY_VISIBLE, visible);
        editor.commit();
    }
    public void setStatus(String status){
        editor.putString(KEY_STATUS, status);
        editor.commit();
    }
    public void setRegistrationId(String registrationId){
        editor.putString(KEY_REGISTRATIONID, registrationId);
        editor.commit();
    }
}