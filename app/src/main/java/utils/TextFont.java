package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Usuario on 20/07/2015.
 */
public class TextFont {

    private static TextFont INSTANCE = null;
    private Typeface font;
    public static String PATH_FONT="fonts/Lato-Light.ttf";//"fonts/ITCBLKAD.TTF"
    public static String PATH_FONT_BOLD="fonts/Lato-Bold.ttf";//"fonts/ITCBLKAD.TTF"


//    private TextFont(Context context) {
//        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
//    }

    public static TextFont getInstance( ) {
        if (INSTANCE == null) {
            INSTANCE = new TextFont();
        }

        return INSTANCE;
    }

    public Typeface getFont(Context context){
       this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
        return this.font;
    }
    public Typeface getFont(Context context,String path){
        this.font = Typeface.createFromAsset(context.getAssets(), path);
        return this.font;
    }
    /*
    @param path path relative assets
     */
    public void overrideFonts(final Context context, final View v,String path) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child,path);
                }
            } else if (v instanceof EditText) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), path));
            }
            else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), path));
            }
            else if (v instanceof CheckBox) {
                ((CheckBox) v).setTypeface(Typeface.createFromAsset(context.getAssets(), path));
            }
            else if (v instanceof Button) {
                ((Button) v).setTypeface(Typeface.createFromAsset(context.getAssets(), path));
            }
        } catch (Exception e) {
            Log.v("TextFont", "overrideFonst() -- "+e.fillInStackTrace() );
        }
    }
}
